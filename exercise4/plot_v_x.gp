# # Set the output format (e.g., PNG)
# set terminal pngcairo enhanced font "Helvetica,12"
# set output "results/v_x_Re_100_mesh_16.png"

# # Set the plot title and labels for x:property
# set title "V vs x"
# set xlabel "X"
# set ylabel "V"

# # Specify the input data file and format
# set datafile separator ","
# set datafile missing "NaN"
# # set yrange [-0.25:0.25]

# # Plot x against property with a blue line and no markers
# plot "results/plotV.csv" using 1:3 with lines linecolor "blue" notitle, \
#      "Guia-v-x.csv" using 1:2 with points pointtype 7 pointsize 1 linecolor "red" notitle

# # Save the plot to a file and close Gnuplot
# set output
# quit


# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output "results/v_x_Re_100_meshes.png"

# Set the plot title and labels for x:property
# set title "y vs U"
set xlabel "x"
set ylabel "V"

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"
# set yrange [-0.25:0.25]

# Plot x against property with different line types and no markers
plot "results/plotV_Re_100_mesh_16.csv" using 1:3 with lines title "h=L/16" linetype 1, \
     "results/plotV_Re_100_mesh_32.csv" using 1:3 with lines title "h=L/32" linetype 2, \
     "results/plotV_Re_100_mesh_64.csv" using 1:3 with lines title "h=L/64" linetype 3, \
     "results/plotV_Re_100_mesh_96.csv" using 1:3 with lines title "h=L/96" linetype 4, \
     "results/plotV_Re_100_mesh_128.csv" using 1:3 with lines title "h=L/128" linetype 5, \
     "results/plotV_Re_100_mesh_160.csv" using 1:3 with lines title "h=L/160" linetype 6, \
     "Guia-v-x.csv" using 1:2 with points pointtype 7 pointsize 1 linecolor "red" title "Ref"

# Save the plot to a file and close Gnuplot
set output
quit
