#include "navier_stoks_solver.h"
#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"
#include "gauss_seidel.h"
#include "conjugate_gradient.h"
#include <algorithm> // for std::max_element

// #include <boost/filesystem.hpp>
// namespace fs = boost::filesystem;

NavierStocksSolver::NavierStocksSolver(const std::string &filename)
{
    readConfigFromYAML(filename);
    initialise_xy_vectors();
    initializePropertyDistribution();

    mu_ = u_ref_ * (rho_ * GeometryWidth_) / Re_;

    if (timeIntegrationScheme_ == "Euler")
    {
        coef_R = 1.0;
        coef_R_old = 0.0;
    }
    else
    {
        coef_R = 1.5;
        coef_R_old = -0.5;
    }
}

void NavierStocksSolver::initialise_xy_vectors()
{
    x_.resize(numGridPoints_x_, 0.0);
    y_.resize(numGridPoints_y_, 0.0);

    // Calculate the spacing between points in each dimension
    dx_ = GeometryWidth_ / numGridPoints_x_;
    dy_ = GeometryHeight_ / numGridPoints_y_;

    x_[0] = dx_ / 2.0;
    y_[0] = dy_ / 2.0;

    for (int i = 1; i < numGridPoints_x_; i++)
    {
        x_[i] = i * dx_ + x_[0];
    }

    for (int j = 1; j < numGridPoints_y_; j++)
    {
        y_[j] = j * dy_ + y_[0];
    }
}

// Method to read configuration parameters from a YAML file
bool NavierStocksSolver::readConfigFromYAML(const std::string &filename)
{
    try
    {
        YAML::Node config = YAML::LoadFile(filename);

        // Read configuration parameters from the YAML file
        GeometryWidth_ = config["GeometryWidth"].as<double>();
        GeometryHeight_ = config["GeometryHeight"].as<double>();
        initialValue_ = config["initialValue"].as<double>();

        rho_ = config["rho"].as<double>();
        scheme_ = config["scheme"].as<std::string>();

        timeStep_ = config["timeStep"].as<double>();
        timestepCoeficient_ = config["timestepCoeficient"].as<double>();
        numGridPoints_x_ = config["numGridPoints_x"].as<int>();
        numGridPoints_y_ = config["numGridPoints_y"].as<int>();
        convergence_criteria_ = config["convergence_criteria"].as<double>();
        u_ref_ = config["u_ref"].as<double>();
        Re_ = config["Re"].as<double>();

        solverMaxIteration_ = config["solverMaxIteration"].as<int>();
        solverTolerance_ = config["solverTolerance"].as<double>();
        solverRelaxation_ = config["solverRelaxation"].as<double>();
        pressureSolver_ = config["pressureSolver"].as<std::string>();

        finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();
        plotUFileName_ = config["plotUFileName"].as<std::string>();
        plotVFileName_ = config["plotVFileName"].as<std::string>();
        timeIntegrationScheme_ = config["timeIntegrationScheme"].as<std::string>();

        outputInterval_ = config["outputInterval"].as<double>();

        return true; // Configuration read successfully
    }
    catch (const YAML::Exception &e)
    {
        std::cerr << "Error reading configuration: " << e.what() << std::endl;
        return false; // Configuration read failed
    }
}

void NavierStocksSolver::initializePropertyDistribution()
{
    u_.resize(numGridPoints_x_ + 1, std::vector<double>(numGridPoints_y_, initialValue_));
    up_.resize(numGridPoints_x_ + 1, std::vector<double>(numGridPoints_y_, initialValue_));
    u_old_.resize(numGridPoints_x_ + 1, std::vector<double>(numGridPoints_y_, initialValue_));

    v_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_ + 1, initialValue_));
    vp_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_ + 1, initialValue_));
    v_old_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_ + 1, initialValue_));

    P_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, initialValue_));
    P_old_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, initialValue_));
}

void NavierStocksSolver::writeAveragePropertyDistributionToCSV(std::string &filename)
{
    std::ofstream file(filename);
    if (file.is_open())
    {
        // Write column headers
        file << "x, y, u, v, U_tot, p" << std::endl;

        // Write data for each point
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                const double u_cell = average(u_[i][j], u_[i + 1][j]);
                const double v_cell = average(v_[i][j], v_[i][j + 1]);
                const double U_tot = sqrt(u_cell * u_cell + v_cell * v_cell);
                const double P_cell = P_[i][j];

                file << x_[i] << ", " << y_[j] << ", " << u_cell << ", " << v_cell << ", " << U_tot << ", " << P_cell << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << filename << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::writePropertyToCSV(std::string &filename, const std::vector<std::vector<double>> &property)
{
    std::ofstream file(filename);
    if (file.is_open())
    {
        // Write column headers
        file << "i, j, x, y, " << filename << std::endl;

        // Write data for each point
        for (int i = 0; i < property.size(); i++)
        {
            for (int j = 0; j < property[i].size(); j++)
            {
                file << i << ", " << j << ", " << x_[i] << ", " << y_[j] << ", " << property[i][j] << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << filename << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::saveTheSimulationResults(double currentTime)
{
    static double lastOutputTime = 0.0;

    // Check if it's time to write output
    if (currentTime - lastOutputTime >= outputInterval_)
    {
        // Format the current time to include in the filename
        std::ostringstream oss_u, oss_v, oss_P;
        oss_u << "results/u_" << std::fixed << std::setprecision(2) << currentTime << ".csv";
        oss_v << "results/v_" << std::fixed << std::setprecision(2) << currentTime << ".csv";
        oss_P << "results/P_" << std::fixed << std::setprecision(2) << currentTime << ".csv";

        std::string filename_u = oss_u.str();
        std::string filename_v = oss_v.str();
        std::string filename_P = oss_P.str();

        writePropertyToCSV(filename_u, u_);
        writePropertyToCSV(filename_v, v_);
        writePropertyToCSV(filename_P, P_);

        // Update the last output time
        lastOutputTime = currentTime;
    }
}

void NavierStocksSolver::readPropertyFromCSV(const std::string &filename, std::vector<std::vector<double>> &property)
{
    std::ifstream file(filename);
    if (!file.is_open())
    {
        std::cerr << "Error: Unable to open file " << filename << std::endl;
        return;
    }

    // Skip the header line
    std::string header;
    std::getline(file, header);

    // Read data for each point
    int i, j;
    double x, y, value;
    while (file >> i >> j >> x >> y >> value)
    {

        // Make sure i and j are within bounds
        if (i >= 0 && i < numGridPoints_x_ + 1 && j >= 0 && j < numGridPoints_y_ + 1)
        {
            property[i][j] = value;
        }
        else
        {
            std::cerr << "Warning: Indices out of bounds (" << i << ", " << j << ") in file " << filename << std::endl;
        }
    }

    file.close();
}

double NavierStocksSolver::loadLastSimulationResults()
{
    // // Specify the pattern for the filenames
    // std::string pattern = "results/u_*.csv";

    // // Find files that match the pattern
    // std::vector<std::string> filenames_u;
    // for (const auto& entry : fs::directory_iterator(pattern)) {
    //     filenames_u.push_back(entry.path().string());
    // }

    // // Find the filename with the highest time
    // auto maxTimeFilename_u = std::max_element(filenames_u.begin(), filenames_u.end());
    // std::string filename_u = *maxTimeFilename_u;

    // // Repeat the process for v and P
    // pattern = "results/v_*.csv";
    // std::vector<std::string> filenames_v;
    // for (const auto& entry : fs::directory_iterator(pattern)) {
    //     filenames_v.push_back(entry.path().string());
    // }

    // auto maxTimeFilename_v = std::max_element(filenames_v.begin(), filenames_v.end());
    // std::string filename_v = *maxTimeFilename_v;

    // pattern = "results/P_*.csv";
    // std::vector<std::string> filenames_P;
    // for (const auto& entry : fs::directory_iterator(pattern)) {
    //     filenames_P.push_back(entry.path().string());
    // }

    // auto maxTimeFilename_P = std::max_element(filenames_P.begin(), filenames_P.end());
    // std::string filename_P = *maxTimeFilename_P;

    // // Read the properties from the files with the highest time
    // readPropertyFromCSV(filename_u, u_);
    // readPropertyFromCSV(filename_v, v_);
    // readPropertyFromCSV(filename_P, P_);

    // double currentTime = extractCurrentTime(filename_P);

    // return currentTime;
}

double NavierStocksSolver::extractCurrentTime(const std::string &filename)
{
    // Find the position of the underscore before the timestamp
    size_t underscorePos = filename.find('_');
    if (underscorePos == std::string::npos)
    {
        // Handle the case where the filename format is not as expected
        return 0.0; // or any suitable default value
    }

    // Extract the substring after the underscore (timestamp)
    std::string timestampStr = filename.substr(underscorePos + 1);

    // Convert the timestamp string to a double
    try
    {
        return std::stod(timestampStr);
    }
    catch (const std::exception &e)
    {
        // Handle the case where the conversion fails
        std::cerr << "Error extracting current time from filename: " << e.what() << std::endl;
        return 0.0; // or any suitable default value
    }
}

void NavierStocksSolver::writeUAtCenterVerticalLineToCSV(std::string &filename)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(filename);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,y,U" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(filename, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // Calculate the average value of u_cell along the vertical centerline
        double average_u_cell = 0.0;
        const int i_centerline = int(numGridPoints_x_ / 2.);

        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double u_cell = u_[i_centerline][j];
            average_u_cell += u_cell;

            // Print the values
            file << average(x_[i_centerline], x_[i_centerline - 1]) << "," << y_[j] << "," << u_cell << std::endl;
        }

        // Calculate and print the average value
        average_u_cell /= numGridPoints_y_;
        std::cout << "Average value of u_cell along the vertical centerline: " << average_u_cell << std::endl;

        file.close();
        std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::writeVAtCenterHorizontalLineToCSV(std::string &filename)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(filename);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,y,V" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(filename, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // meaning the horizontal centerline
        // Calculate the average value of v_cell along the horizontal centerline
        double average_v_cell = 0.0;
        const int j_centerline = int(numGridPoints_y_ / 2.);

        for (int i = 0; i < numGridPoints_x_; i++)
        {
            const double v_cell = v_[i][j_centerline];
            average_v_cell += v_cell;

            // Print the values
            file << x_[i] << "," << average(y_[j_centerline], y_[j_centerline - 1]) << "," << v_cell << std::endl;
        }

        // Calculate and print the average value
        average_v_cell /= numGridPoints_x_;
        std::cout << "Average value of v_cell along the horizontal centerline: " << average_v_cell << std::endl;

        file.close();
        std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

const double NavierStocksSolver::get_average_of_changes(
    std::vector<std::vector<double>> &old_field,
    std::vector<std::vector<double>> &new_field)
{
    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    double sumDifferences = 0.0;

    // Copy values from propertyDistribution_ to propertyDistribution_new
    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            double difference = std::abs(old_field[i][j] - new_field[i][j]);
            sumDifferences += difference;
        }
    }
    // Calculate the average
    double averageDifference = sumDifferences / (x_size * y_size);

    return averageDifference;
}

const double NavierStocksSolver::get_maximum_of_changes(
    const std::vector<std::vector<double>> &old_field,
    const std::vector<std::vector<double>> &new_field)
{

    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    // Calculate the change in the scalar field
    double maxChange = 0.0;

    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            double delta = std::abs(old_field[i][j] - new_field[i][j]);
            maxChange = std::max(maxChange, delta);
        }
    }

    return maxChange;
}

void NavierStocksSolver::store_property_in_temp_array(
    std::vector<std::vector<double>> &old_field,
    const std::vector<std::vector<double>> &new_field)
{
    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    // Copy values from new_field to old_field
    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            old_field[i][j] = new_field[i][j];
        }
    }
}

double NavierStocksSolver::linearInterpolation(double x0, double y0, double x1, double y1, double x)
{
    // Ensure x0 is not equal to x1 to avoid division by zero
    if (x0 == x1)
    {
        return y0;
    }

    // Calculate the slope (m) and y-intercept (b) of the line between (x0, y0) and (x1, y1)
    double m = (y1 - y0) / (x1 - x0);
    double b = y0 - m * x0;

    // Use the equation of the line to interpolate the value at x
    return m * x + b;
}

double NavierStocksSolver::evaluate_convergence_rates()
{
    const double convergence_rate_u = get_maximum_of_changes(u_old_, u_);
    const double convergence_rate_v = get_maximum_of_changes(v_old_, v_);
    const double convergence_rate_P = get_maximum_of_changes(P_old_, P_);
    std::cout << "Convergence Rate u is: " << convergence_rate_u << std::endl;
    std::cout << "Convergence Rate v is: " << convergence_rate_v << std::endl;
    std::cout << "Convergence Rate p is: " << convergence_rate_P << std::endl;

    // return std::max(convergence_rate_u, std::max(convergence_rate_v, convergence_rate_P));
    return std::max(convergence_rate_u, convergence_rate_v);
}

double NavierStocksSolver::calculateTimeStep_conv()
{
    double max_dt = std::numeric_limits<double>::infinity();

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double u_cell = average(u_[i][j], u_[i + 1][j]);
            const double dt_u = timestepCoeficient_ * dx_ / std::fabs(u_cell);

            const double v_cell = average(v_[i][j], v_[i][j + 1]);
            const double dt_v = timestepCoeficient_ * dy_ / std::fabs(v_cell);

            max_dt = std::min(std::min(dt_u, dt_v), max_dt);
        }
    }
    return max_dt;
}

double NavierStocksSolver::calculateTimeStep_diff()
{
    double max_dt = std::numeric_limits<double>::infinity();

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double dt_u = timestepCoeficient_ * std::pow(dx_, 2.0) * rho_ / mu_;
            const double dt_v = timestepCoeficient_ * std::pow(dy_, 2.0) * rho_ / mu_;

            max_dt = std::min(std::min(dt_u, dt_v), max_dt);
        }
    }

    return max_dt;
}

double NavierStocksSolver::calculateTimeStep()
{
    const double dt_conv = calculateTimeStep_conv();
    static const double dt_diff = calculateTimeStep_diff();
    return std::min(dt_conv, dt_diff);
}

void NavierStocksSolver::solve()
{
    double convergence_rate = 1.e10;

    // currentTime_ = loadLastSimulationResults();

    int itr = 0;
    while (convergence_rate > convergence_criteria_)
    // while(currentTime_<30)
    {
        timeStep_ = calculateTimeStep();
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "Iteration: " << itr << ", dt: " << timeStep_ << ", solving for time: " << currentTime_ << std::endl;

        // Update the property distribution based on the solution
        solveExplicit();

        // Advance the current time
        currentTime_ += timeStep_;

        convergence_rate = evaluate_convergence_rates();

        store_property_in_temp_array(u_old_, u_);
        store_property_in_temp_array(v_old_, v_);
        store_property_in_temp_array(P_old_, P_);

        saveTheSimulationResults(currentTime_);
        itr++;
    }

    writeAveragePropertyDistributionToCSV(finalResultOutputFileName_);
    writeVAtCenterHorizontalLineToCSV(plotVFileName_);
    writeUAtCenterVerticalLineToCSV(plotUFileName_);
    std::cout << "solution finished" << std::endl;
}

void NavierStocksSolver::solveExplicit()
{
    applyBoundaryValues_u();
    applyBoundaryValues_v();

    evaluate_u_p();
    evaluate_v_p();

    evaluate_P();

    evaluate_u_new();
    evaluate_v_new();
}

void NavierStocksSolver::applyBoundaryValues_u()
{
    for (int i = 0; i < numGridPoints_x_ + 1; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            if (i == 0 | i == numGridPoints_x_) // left, or right boundaries
            {
                u_[i][j] = 0.0;
            }

            else if (j == 0) // bottom boundary
            {
                u_[i][j] = linearInterpolation(0.0, 0.0, y_[j + 1], u_[i][j + 1], dy_ / 2.);
            }

            else if (j == (numGridPoints_y_ - 1)) // top boundary
            {
                u_[i][j] = linearInterpolation(GeometryHeight_, u_ref_, y_[j - 1], u_[i][j - 1], y_[j]);
            }
        }
    }
}

void NavierStocksSolver::applyBoundaryValues_v()
{
    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_ + 1; j++)
        {
            if (j == 0 | j == numGridPoints_y_) // bottom, or top boundaries
            {
                v_[i][j] = 0.0;
            }
            else if (i == 0) // left boundary
            {
                v_[i][j] = linearInterpolation(0.0, 0.0, x_[i + 1], v_[i + 1][j], dx_ / 2.);
            }

            else if (i == (numGridPoints_x_ - 1)) // right boundary
            {
                v_[i][j] = linearInterpolation(GeometryWidth_, 0.0, x_[i - 1], v_[i - 1][j], x_[i]);
            }
        }
    }
}

double NavierStocksSolver::computeRu(int i, int j, const std::vector<std::vector<double>> &u, const std::vector<std::vector<double>> &v)
{
    const double u_E = u[i + 1][j];
    const double u_W = u[i - 1][j];
    const double u_N = u[i][j + 1];
    const double u_S = u[i][j - 1];
    const double u_P = u[i][j];

    const double u_e = average(u[i + 1][j], u[i][j]);
    const double u_w = average(u[i - 1][j], u[i][j]);
    const double u_n = average(u[i][j + 1], u[i][j]);
    const double u_s = average(u[i][j - 1], u[i][j]);

    const double v_n = average(v[i - 1][j + 1], v[i][j + 1]);
    const double v_s = average(v[i - 1][j], v[i][j]);

    return mu_ * (u_N + u_S + u_E + u_W - 4. * u_P) -
           rho_ * dx_ * (v_n * u_n - v_s * u_s + u_e * u_e - u_w * u_w);
}

double NavierStocksSolver::average(double a, double b)
{
    return 0.5 * (a + b);
}

void NavierStocksSolver::evaluate_u_p()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            const double Ru = computeRu(i, j, u_, v_);
            const double Ru_old = computeRu(i, j, u_old_, v_old_);

            up_[i][j] = u_[i][j] + timeStep_ / (rho_ * dx_ * dy_) * (coef_R * Ru + coef_R_old * Ru_old);
        }
    }
}

double NavierStocksSolver::compute_Rv(int i, int j, const std::vector<std::vector<double>> &u, const std::vector<std::vector<double>> &v)
{
    const double v_E = v[i + 1][j];
    const double v_W = v[i - 1][j];
    const double v_N = v[i][j + 1];
    const double v_S = v[i][j - 1];
    const double v_P = v[i][j];

    const double v_e = average(v[i + 1][j], v[i][j]);
    const double v_w = average(v[i - 1][j], v[i][j]);
    const double v_n = average(v[i][j + 1], v[i][j]);
    const double v_s = average(v[i][j - 1], v[i][j]);

    const double u_w = average(u[i][j], u[i][j - 1]);
    const double u_e = average(u[i + 1][j], u[i + 1][j - 1]);

    return mu_ * (v_N + v_S + v_E + v_W - 4. * v_P) -
           rho_ * dx_ * (v_n * v_n - v_s * v_s + u_e * v_e - u_w * v_w);
}

void NavierStocksSolver::evaluate_v_p()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_; j++)
        {
            const double Rv_ = compute_Rv(i, j, u_, v_);
            const double Rv_old = compute_Rv(i, j, u_old_, v_old_);

            vp_[i][j] = v_[i][j] + timeStep_ / (rho_ * dx_ * dy_) * (coef_R * Rv_ + coef_R_old * Rv_old);
        }
    }
}

void NavierStocksSolver::evaluate_u_new()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            u_[i][j] = up_[i][j] - (timeStep_ / rho_) * (P_[i][j] - P_[i - 1][j]) / dx_;
        }
    }
}

void NavierStocksSolver::evaluate_v_new()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_; j++)
        {
            v_[i][j] = vp_[i][j] - (timeStep_ / rho_) * (P_[i][j] - P_[i][j - 1]) / dy_;
        }
    }
}

void NavierStocksSolver::evaluate_P()
{
    // Fill the matrix A and vector b for the current time step
    auto [A, b] = fillMatrixAAndVectorB();

    // Solve the system of equations using Gauss-Seidel
    if (pressureSolver_ == "Gauss_Sidel")
    {
        gaussSeidel(A, b, solverMaxIteration_, solverTolerance_, P_, solverRelaxation_);
    }
    else
    {
        conjugateGradient(A, b, solverMaxIteration_, solverTolerance_, P_);
    }
}

std::pair<std::vector<std::vector<std::vector<double>>>, std::vector<std::vector<double>>> NavierStocksSolver::fillMatrixAAndVectorB()
{
    static const int diagonals = 5;

    static std::vector<std::vector<std::vector<double>>> A(numGridPoints_x_, std::vector<std::vector<double>>(numGridPoints_y_, std::vector<double>(diagonals, 0.0)));
    static std::vector<std::vector<double>> b(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double aW = get_aW_pressure_eq(i, j);
            const double aE = get_aE_pressure_eq(i, j);
            const double aN = get_aN_pressure_eq(i, j);
            const double aS = get_aS_pressure_eq(i, j);

            const double aP = aE + aW + aN + aS;

            A[i][j][0] = -1. * aE;
            A[i][j][1] = -1. * aW;
            A[i][j][2] = aP;
            A[i][j][3] = -1. * aS;
            A[i][j][4] = -1. * aN;

            const double vp_n = vp_[i][j + 1];
            const double vp_s = vp_[i][j];
            const double up_e = up_[i + 1][j];
            const double up_w = up_[i][j];

            b[i][j] = -1. * (rho_ / timeStep_) * ((vp_n - vp_s) * dx_ + (up_e - up_w) * dy_);
        }
    }

    return std::make_pair(A, b);
}

inline const double NavierStocksSolver::get_aW_pressure_eq(const int i, const int j)
{
    if (i == 0)
    {
        return 0;
    }
    return 1;
}

inline const double NavierStocksSolver::get_aE_pressure_eq(const int i, const int j)
{
    if (i == (numGridPoints_x_ - 1))
    {
        return 0;
    }
    return 1;
}

inline const double NavierStocksSolver::get_aS_pressure_eq(const int i, const int j)
{
    if (j == 0)
    {
        return 0;
    }
    return 1;
}

inline const double NavierStocksSolver::get_aN_pressure_eq(const int i, const int j)
{
    if (j == (numGridPoints_y_ - 1))
    {
        return 0;
    }
    return 1;
}