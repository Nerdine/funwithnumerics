#ifndef CONJUGATE_GRADIENT_H
#define CONJUGATE_GRADIENT_H

#include <vector>

void conjugateGradient(
    const std::vector<std::vector<std::vector<double>>> &A,
    const std::vector<std::vector<double>> &b,
    int maxIterations,
    double tolerance,
    std::vector<std::vector<double>> &x
    );

#endif // CONJUGATE_GRADIENT_H
