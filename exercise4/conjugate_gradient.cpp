#include "conjugate_gradient.h"
#include <iostream>
#include <vector>

void conjugateGradient(
    const std::vector<std::vector<std::vector<double>>> &A,
    const std::vector<std::vector<double>> &b,
    int maxIterations,
    double tolerance,
    std::vector<std::vector<double>> &x)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    // Initialize solution vector and residual
    std::vector<std::vector<double>> r(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));
    // std::vector<std::vector<double>> p(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));
    std::vector<std::vector<double>> p = x;     // Initial guess for the solution similar to what i did in gauss_sidel

    // Initial guess for the solution
    // x = p;

    // Calculate the initial residual r = b - Ax
    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            double Ax = 0.0;

            if (i > 0)
                Ax += A[i][j][1] * x[i - 1][j]; // aW * x[i - 1][j]

            if (i < numGridPoints_x_ - 1)
                Ax += A[i][j][0] * x[i + 1][j]; // aE * x[i + 1][j]

            if (j > 0)
                Ax += A[i][j][3] * x[i][j - 1]; // aS * x[i][j - 1]

            if (j < numGridPoints_y_ - 1)
                Ax += A[i][j][4] * x[i][j + 1]; // aN * x[i][j + 1]

            Ax += A[i][j][2] * x[i][j]; // aP * x[i][j]

            r[i][j] = b[i][j] - Ax;
            p[i][j] = r[i][j];
        }
    }

    double rsold = 0.0;

    // Calculate initial squared norm of the residual
    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            rsold += r[i][j] * r[i][j];
        }
    }

    // Main loop
    for (int iter = 0; iter < maxIterations; iter++)
    {
        double rsnew = 0.0;
        double alpha = 0.0;
        double beta = 0.0;

        // Calculate Ap
        std::vector<std::vector<double>> Ap(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));

        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                Ap[i][j] = A[i][j][2] * p[i][j]; // aP * p[i][j]

                if (i > 0)
                    Ap[i][j] += A[i][j][1] * p[i - 1][j]; // aW * p[i - 1][j]

                if (i < numGridPoints_x_ - 1)
                    Ap[i][j] += A[i][j][0] * p[i + 1][j]; // aE * p[i + 1][j]

                if (j > 0)
                    Ap[i][j] += A[i][j][3] * p[i][j - 1]; // aS * p[i][j - 1]

                if (j < numGridPoints_y_ - 1)
                    Ap[i][j] += A[i][j][4] * p[i][j + 1]; // aN * p[i][j + 1]
            }
        }

        // Calculate alpha
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                alpha += p[i][j] * Ap[i][j];
            }
        }

        alpha = rsold / alpha;

        // Update solution and residual
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                x[i][j] += alpha * p[i][j];
                r[i][j] -= alpha * Ap[i][j];
            }
        }

        // Calculate new squared norm of the residual
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                rsnew += r[i][j] * r[i][j];
            }
        }

        // Check for convergence
        if (rsnew < tolerance)
        {
            std::cout << "Conjugate Gradient solver returning after " << iter + 1 << " iterations." << std::endl;
            return;
        }

        // Calculate beta
        beta = rsnew / rsold;

        // Update search direction
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                p[i][j] = r[i][j] + beta * p[i][j];
            }
        }

        rsold = rsnew;
    }

    std::cout << "Conjugate Gradient did not converge within " << maxIterations << " iterations." << std::endl;
}
