#ifndef navier_stoks_solver_H
#define navier_stoks_solver_H

#include <vector>
#include <fstream>

class NavierStocksSolver
{
public:
    // Constructor to initialize the solver with parameters
    NavierStocksSolver(const std::string &filename);

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string &filename);

    // Method to initialize the location vectors of x and y
    void initialise_xy_vectors();

    // Initialize the property distribution on the geometry
    void initializePropertyDistribution();

    // Functions to write and read the property values of the entire geometry
    void writePropertyToCSV(std::string &filename, const std::vector<std::vector<double>> &property);
    void writeAveragePropertyDistributionToCSV(std::string &filename);
    void readPropertyFromCSV(const std::string &filename, std::vector<std::vector<double>> &property);
    double loadLastSimulationResults();
    double extractCurrentTime(const std::string &filename);

    // Function to write the property value at a location in each timestep
    void writeUAtCenterVerticalLineToCSV(std::string &filename);
    void writeVAtCenterHorizontalLineToCSV(std::string &filename);

    void applyBoundaryValues_u();
    void applyBoundaryValues_v();
    double computeRu(int i, int j, const std::vector<std::vector<double>> &u, const std::vector<std::vector<double>> &v);
    double compute_Rv(int i, int j, const std::vector<std::vector<double>> &u, const std::vector<std::vector<double>> &v);
    void evaluate_u_p();
    void evaluate_v_p();
    void evaluate_P();
    void evaluate_u_new();
    void evaluate_v_new();

    // Function to fill and return the coefficient matrix A and vector b for Ax = b
    std::pair<std::vector<std::vector<std::vector<double>>>, std::vector<std::vector<double>>> fillMatrixAAndVectorB();
    const double get_aW_pressure_eq(const int i, const int j);
    const double get_aE_pressure_eq(const int i, const int j);
    const double get_aS_pressure_eq(const int i, const int j);
    const double get_aN_pressure_eq(const int i, const int j);

    void solveExplicit();

    // Solve the convection-diffusion equation over a specified time period
    void solve();

    // Get the property distribution at the current time if needed from outside of the class
    const std::vector<std::vector<double>> &getCurrentPropertyDistribution() const;

    const double get_average_of_changes(
        std::vector<std::vector<double>> &old_field,
        std::vector<std::vector<double>> &new_field);
    const double get_maximum_of_changes(
        const std::vector<std::vector<double>> &old_field,
        const std::vector<std::vector<double>> &new_field);

    void store_property_in_temp_array(std::vector<std::vector<double>> &old_field, const std::vector<std::vector<double>> &new_field);
    void saveTheSimulationResults(double currentTime);
    double evaluate_convergence_rates();
    double average(double a, double b);
    double linearInterpolation(double x0, double y0, double x1, double y1, double x);
    double calculateTimeStep_conv();
    double calculateTimeStep_diff();
    double calculateTimeStep();

private:
    // private member functions:
    double timeStep_;
    double timestepCoeficient_;
    int numGridPoints_x_;
    int numGridPoints_y_;

    int solverMaxIteration_;
    double solverTolerance_;
    double solverRelaxation_;
    std::string pressureSolver_;

    double initialValue_;

    double convergence_criteria_;
    double dx_;
    double dy_;

    std::vector<std::vector<double>> u_;
    std::vector<std::vector<double>> up_;
    std::vector<std::vector<double>> u_old_;

    std::vector<std::vector<double>> v_;
    std::vector<std::vector<double>> vp_;
    std::vector<std::vector<double>> v_old_;

    std::vector<std::vector<double>> P_;
    std::vector<std::vector<double>> P_old_;

    std::vector<double> x_;
    std::vector<double> y_;

    double GeometryWidth_;
    double GeometryHeight_;

    double rho_;
    double mu_;
    double Re_;
    double u_ref_;
    double coef_R;
    double coef_R_old;

    std::string scheme_;
    std::string timeIntegrationScheme_;
    std::string finalResultOutputFileName_;
    std::string plotUFileName_;
    std::string plotVFileName_;
    double outputInterval_;

    double currentTime_;
};

#endif // navier_stoks_solver_H
