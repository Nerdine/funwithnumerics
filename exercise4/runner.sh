#!/bin/bash

# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <cpp_file>"
    exit 1
fi

cpp_file="$1"
output_file="${cpp_file%.cpp}"
log_file="${output_file}_log.txt"

# Check if the input C++ file exists
if [ ! -f "$cpp_file" ]; then
    echo "Error: The input file '$cpp_file' does not exist."
    exit 1
fi

# Compile the C++ program using g++ with the C++17 standard
g++ -std=c++17 -o "$output_file" "$cpp_file" conjugate_gradient.cpp gauss_seidel.cpp navier_stoks_solver.cpp -lyaml-cpp 2>&1 | tee "$log_file"


# Check if compilation was successful
if [ ${PIPESTATUS[0]} -eq 0 ]; then
    echo "Compilation successful. Running the program..."
    "./$output_file"
else
    echo "Compilation failed."
fi
