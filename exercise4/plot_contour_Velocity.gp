num_isolines = 5  # Adjust the number of isolines as needed
min_value = 0
max_value = 1
my_data = "results/finalResultsEntireDomain.csv"

# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output "results/vel_contour_Re_100_mesh_16.png"

# Set the plot title and labels
set title "U 2D Grid"
set xlabel "X"
set ylabel "Y"

# Specify the color range based on your data
set cbrange [min_value:max_value]

# Customize the color map
set pm3d map
set palette defined (0 "blue", 0.4 "green", 0.6 "yellow", 1 "red")

# Set the size ratio to ensure x-axis length is exactly twice the y-axis length
set size ratio -1

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"

# Create the 2D grid plot from the CSV data file
splot my_data using 1:2:5 with image

# Save the plot to a file and close Gnuplot
set output
quit
