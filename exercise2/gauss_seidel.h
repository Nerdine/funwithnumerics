#ifndef GAUSS_SEIDEL_H
#define GAUSS_SEIDEL_H

#include <vector>

void gaussSeidel(
    const std::vector<std::vector<std::vector<double>>> &A,
    const std::vector<std::vector<double>> &b,
    int maxIterations,
    double tolerance,
    std::vector<std::vector<double>> &x,
    double omega = 1.0
    );

// Function to apply the coefficients on the left boundary
void solve_internal_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the left boundary
void solve_left_boundary_points(const std::vector<std::vector<std::vector<double>>> &A,const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the right boundary
void solve_right_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the top boundary
void solve_top_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the bottom boundary
void solve_bottom_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the left bottom cell
void solve_left_bottom_corner_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the right bottom cell
void solve_right_bottom_corner_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the left top cell
void solve_left_top_corner_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

// Function to apply the coefficients on the right top cell
void solve_right_top_corner_boundary_points(const std::vector<std::vector<std::vector<double>>> &A, const std::vector<std::vector<double>> &b, std::vector<std::vector<double>> &x, double& omega, double& maxError);

#endif // GAUSS_SEIDEL_H