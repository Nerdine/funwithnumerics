#ifndef HEAT_EQUATION_SOLVER_H
#define HEAT_EQUATION_SOLVER_H

#include <vector>
#include <fstream>

class HeatEquationSolver
{
public:
    // Constructor to initialize the solver with parameters
    HeatEquationSolver(const std::string &filename);

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string &filename);

    // Method to initialize the location vectors of x and y
    void initialise_xy_vectors();

    // Initialize the temperature distribution on the rod
    void initializeTemperatureDistribution();

    // Method to initialse the physical properties in geometry
    void initialise_properties_in_geometry();

    double getTemperatureValueAtLocation(double x, double y);

    // Function to write the temperature values of the entire rod at the end of solution
    void writeTemperatureDistributionToCSV(int iterator_);

    // Function to write the temperature value at a location in each timestep
    void writeTemperatureAtLocationToCSV(double t);

    // Function to fill and return the coefficient matrix A and vector b for Ax = b
    std::pair<std::vector<std::vector<std::vector<double>>>, std::vector<std::vector<double>>> fillMatrixAAndVectorB();

    void solveImplicit();

    // Solve the 1D heat equation over a specified time period
    void solve();

    // Get the temperature distribution at the current time if needed from outside of the class
    const std::vector<std::vector<double>> &getCurrentTemperatureDistribution() const;

    // Helper function to use for the interpolation of a Temperature at a arbitrary location
    int findIndexOfLargestValueSmallerThanX(const std::vector<double> &x_, double x);

    // Function to get the geometric average of two values
    inline double geometric_average_x(const double x1, const double x2);

    // Function to apply the coefficients on the left boundary
    void apply_internal_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the left boundary
    void apply_left_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the right boundary
    void apply_right_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the top boundary
    void apply_top_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the bottom boundary
    void apply_bottom_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the left bottom cell
    void apply_left_bottom_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the right bottom cell
    void apply_right_bottom_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the left top cell
    void apply_left_top_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    // Function to apply the coefficients on the right top cell
    void apply_right_top_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b);

    inline const double get_aW(const int i, const int j);
    inline const double get_aE(const int i, const int j);
    inline const double get_aS(const int i, const int j);
    inline const double get_aN(const int i, const int j);


private:
    // private member functions:
    double timeStep_;
    int numGridPoints_x_;
    int numGridPoints_y_;

    double initialTemperature_;

    double endTime_;
    double dx_;
    double dy_;
    double beta_; // Additional parameter for temporal approach
    int solverMaxIteration_;
    double solverTolerance_;
    double solverRelaxation_;
    double gamma_; // internal parameter used to simplify the coefficients
    double probLocation_x_;
    double probLocation_y_;
    double probLocation_x2_;
    double probLocation_y2_;
    std::vector<std::vector<double>> temperatureDistribution_;
    std::vector<std::vector<double>> rho_;
    std::vector<std::vector<double>> cp_;
    std::vector<std::vector<double>> lambda_;
    std::vector<double> x_;
    std::vector<double> y_;

    double rodWidth_;
    double rodHeight_;

    double M1_x0_;
    double M1_x1_;
    double M1_y0_;
    double M1_y1_;
    double M1_rho_;
    double M1_cp_;
    double M1_lambda_;

    double M2_x0_;
    double M2_x1_;
    double M2_y0_;
    double M2_y1_;
    double M2_rho_;
    double M2_cp_;
    double M2_lambda_;

    double M3_x0_;
    double M3_x1_;
    double M3_y0_;
    double M3_y1_;
    double M3_rho_;
    double M3_cp_;
    double M3_lambda_;

    double M4_x0_;
    double M4_x1_;
    double M4_y0_;
    double M4_y1_;
    double M4_rho_;
    double M4_cp_;
    double M4_lambda_;

    std::string finalResultOutputFileName_;
    std::string finalResultOutputFileNameIterator_;

    std::string evolutionaryResultOutputFileName_;

    double T_bottom_;
    double T_inf_;
    double alpha_inf_; 
    double top_flux_;

    double currentTime_;

};

#endif // HEAT_EQUATION_SOLVER_H
