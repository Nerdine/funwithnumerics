#include "heat_equation_solver.h"
#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"
#include "gauss_seidel.h"

HeatEquationSolver::HeatEquationSolver(const std::string &filename)
{
    readConfigFromYAML(filename);
    initialise_xy_vectors();
    initializeTemperatureDistribution();
    initialise_properties_in_geometry();
}

void HeatEquationSolver::initialise_properties_in_geometry()
{
    rho_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));
    cp_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));
    lambda_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            if (x_[i] > M1_x0_ && x_[i] < M1_x1_ && y_[j] > M1_y0_ && y_[j] < M1_y1_)
            {
                // Set rho, cp, and lambda based on the condition for M1 region
                rho_[i][j] = M1_rho_;
                cp_[i][j] = M1_cp_;
                lambda_[i][j] = M1_lambda_;
            }

            if (x_[i] > M2_x0_ && x_[i] < M2_x1_ && y_[j] > M2_y0_ && y_[j] < M2_y1_)
            {
                // Set rho, cp, and lambda based on the condition for M2 region
                rho_[i][j] = M2_rho_;
                cp_[i][j] = M2_cp_;
                lambda_[i][j] = M2_lambda_;
            }

            if (x_[i] > M3_x0_ && x_[i] < M3_x1_ && y_[j] > M3_y0_ && y_[j] < M3_y1_)
            {
                // Set rho, cp, and lambda based on the condition for M3 region
                rho_[i][j] = M3_rho_;
                cp_[i][j] = M3_cp_;
                lambda_[i][j] = M3_lambda_;
            }

            if (x_[i] > M4_x0_ && x_[i] < M4_x1_ && y_[j] > M4_y0_ && y_[j] < M4_y1_)
            {
                // Set rho, cp, and lambda based on the condition for M4 region
                rho_[i][j] = M4_rho_;
                cp_[i][j] = M4_cp_;
                lambda_[i][j] = M4_lambda_;
            }
        }
    }
}

void HeatEquationSolver::initialise_xy_vectors()
{
    x_.resize(numGridPoints_x_, 0.0);
    y_.resize(numGridPoints_y_, 0.0);

    // Calculate the spacing between points in each dimension
    dx_ = rodWidth_ / numGridPoints_x_;
    dy_ = rodHeight_ / numGridPoints_y_;

    x_[0] = dx_ / 2.0;
    y_[0] = dy_ / 2.0;

    for (int i = 1; i < numGridPoints_x_; i++)
    {
        x_[i] = i * dx_ + x_[0];
    }

    for (int j = 1; j < numGridPoints_y_; j++)
    {
        y_[j] = j * dy_ + y_[0];
    }
}

// Method to read configuration parameters from a YAML file
bool HeatEquationSolver::readConfigFromYAML(const std::string &filename)
{
    try
    {
        YAML::Node config = YAML::LoadFile(filename);

        // Read configuration parameters from the YAML file
        rodWidth_ = config["rodWidth"].as<double>();
        rodHeight_ = config["rodHeight"].as<double>();

        initialTemperature_ = config["initialTemperature"].as<double>();

        M1_x0_ = config["M1_x0"].as<double>();
        M1_x1_ = config["M1_x1"].as<double>();
        M1_y0_ = config["M1_y0"].as<double>();
        M1_y1_ = config["M1_y1"].as<double>();
        M1_rho_ = config["M1_rho"].as<double>();
        M1_cp_ = config["M1_cp"].as<double>();
        M1_lambda_ = config["M1_lambda"].as<double>();

        M2_x0_ = config["M2_x0"].as<double>();
        M2_x1_ = config["M2_x1"].as<double>();
        M2_y0_ = config["M2_y0"].as<double>();
        M2_y1_ = config["M2_y1"].as<double>();
        M2_rho_ = config["M2_rho"].as<double>();
        M2_cp_ = config["M2_cp"].as<double>();
        M2_lambda_ = config["M2_lambda"].as<double>();

        M3_x0_ = config["M3_x0"].as<double>();
        M3_x1_ = config["M3_x1"].as<double>();
        M3_y0_ = config["M3_y0"].as<double>();
        M3_y1_ = config["M3_y1"].as<double>();
        M3_rho_ = config["M3_rho"].as<double>();
        M3_cp_ = config["M3_cp"].as<double>();
        M3_lambda_ = config["M3_lambda"].as<double>();

        M4_x0_ = config["M4_x0"].as<double>();
        M4_x1_ = config["M4_x1"].as<double>();
        M4_y0_ = config["M4_y0"].as<double>();
        M4_y1_ = config["M4_y1"].as<double>();
        M4_rho_ = config["M4_rho"].as<double>();
        M4_cp_ = config["M4_cp"].as<double>();
        M4_lambda_ = config["M4_lambda"].as<double>();

        timeStep_ = config["timeStep"].as<double>();
        numGridPoints_x_ = config["numGridPoints_x"].as<int>();
        numGridPoints_y_ = config["numGridPoints_y"].as<int>();
        endTime_ = config["endTime"].as<double>();

        solverMaxIteration_ = config["solverMaxIteration"].as<int>();
        solverTolerance_ = config["solverTolerance"].as<double>();
        solverRelaxation_ = config["solverRelaxation"].as<double>();
        beta_ = config["beta"].as<double>();

        finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();
        evolutionaryResultOutputFileName_ = config["evolutionaryResultOutputFileName"].as<std::string>();
        probLocation_x_ = config["probLocation_x"].as<double>();
        probLocation_y_ = config["probLocation_y"].as<double>();
        
        probLocation_x2_ = config["probLocation_x2"].as<double>();
        probLocation_y2_ = config["probLocation_y2"].as<double>();

        T_bottom_ = config["T_bottom"].as<double>();
        T_inf_ = config["T_inf"].as<double>();
        alpha_inf_ = config["alpha_inf"].as<double>();
        top_flux_ = config["top_flux"].as<double>();

        return true; // Configuration read successfully
    }
    catch (const YAML::Exception &e)
    {
        std::cerr << "Error reading configuration: " << e.what() << std::endl;
        return false; // Configuration read failed
    }
}

void HeatEquationSolver::initializeTemperatureDistribution()
{
    temperatureDistribution_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, initialTemperature_));
}

void HeatEquationSolver::writeTemperatureDistributionToCSV(int iterator_)
{
    finalResultOutputFileNameIterator_ = finalResultOutputFileName_ + "_" + std::to_string(iterator_) + ".csv";

    std::ofstream file(finalResultOutputFileNameIterator_);
    if (file.is_open())
    {
        // Write column headers
        file << "x,y,temperatureDistribution" << std::endl;

        // Write data for each point
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                file << x_[i] << "," << y_[j] << "," << temperatureDistribution_[i][j] - 273.15 << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << finalResultOutputFileNameIterator_ << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

int HeatEquationSolver::findIndexOfLargestValueSmallerThanX(const std::vector<double> &x_, double x)
{
    double maxSmallerValue = -std::numeric_limits<double>::infinity(); // Initialize with negative infinity
    int indexOfMaxSmallerValue = -1;                                   // Initialize with an invalid index

    for (int i = 0; i < x_.size(); i++)
    {
        if (x_[i] < x && x_[i] > maxSmallerValue)
        {
            maxSmallerValue = x_[i];
            indexOfMaxSmallerValue = i;
        }
    }
    return indexOfMaxSmallerValue;
}

double HeatEquationSolver::getTemperatureValueAtLocation(double x, double y)
{
    // Calculate the indices and weights for bilinear interpolation in 2D
    int index_left_x = findIndexOfLargestValueSmallerThanX(x_, x);
    int index_right_x = index_left_x + 1;
    int index_bottom_y = findIndexOfLargestValueSmallerThanX(y_, y);
    int index_top_y = index_bottom_y + 1;

    // Retrieve temperatures from adjacent cells
    double temp_bottom_left = temperatureDistribution_[index_left_x][index_bottom_y];
    double temp_bottom_right = temperatureDistribution_[index_right_x][index_bottom_y];
    double temp_top_left = temperatureDistribution_[index_left_x][index_top_y];
    double temp_top_right = temperatureDistribution_[index_right_x][index_top_y];

    double x_left = x_[index_left_x];
    double x_right = x_[index_right_x];
    double y_top = y_[index_top_y];
    double y_bottom = y_[index_bottom_y];

    // Calculate the weights for interpolation
    double weight_x = (x - x_left) / (x_right - x_left);
    double weight_y = (y - y_bottom) / (y_top - y_bottom);

    // Bilinear interpolation
    double temp_at_location = (1 - weight_x) * ((1 - weight_y) * temp_bottom_left + weight_y * temp_top_left) +
                              weight_x * ((1 - weight_y) * temp_bottom_right + weight_y * temp_top_right);

    return temp_at_location;
}

void HeatEquationSolver::writeTemperatureAtLocationToCSV(double t)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(evolutionaryResultOutputFileName_);
        if (file.is_open())
        {
            // Write column headers (only once)
            // file << "x,y,t,temperature" << std::endl;
            file << "t,temperature_point_1,temerature_point_2" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(evolutionaryResultOutputFileName_, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // Write the data for the specified x and t
        // file << probLocation_x_ << "," << probLocation_y_ << "," << t << "," << getTemperatureValueAtLocation(probLocation_x_, probLocation_y_) << std::endl;
        file << t << "," << getTemperatureValueAtLocation(probLocation_x_, probLocation_y_) << "," << getTemperatureValueAtLocation(probLocation_x2_, probLocation_y2_) << std::endl;

        file.close();
        // std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void HeatEquationSolver::solve()
{
    currentTime_ = timeStep_;
    int iterator_ = 1;
    while (currentTime_ <= endTime_)
    {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "solving for the time: " << currentTime_ << std::endl;

        // Update the temperature distribution based on the solution
        solveImplicit();

        // Advance the current time
        currentTime_ += timeStep_;

        writeTemperatureAtLocationToCSV(currentTime_);
        writeTemperatureDistributionToCSV(iterator_);
        iterator_ = iterator_ +1;
    }

    std::cout << "temperature at (0.65, 0.56) is: " << getTemperatureValueAtLocation(0.65, 0.56) - 273.15 << std::endl;
    std::cout << "temperature at (0.74, 0.72) is: " << getTemperatureValueAtLocation(0.74, 0.72) - 273.15 << std::endl;

    //writeTemperatureDistributionToCSV();
    std::cout << "solution finished" << std::endl;
}

const std::vector<std::vector<double>> &HeatEquationSolver::getCurrentTemperatureDistribution() const
{
    return temperatureDistribution_;
}

inline double HeatEquationSolver::geometric_average_x(const double x1, const double x2)
{
    return 2.0 / (1.0 / x1 + 1.0 / x2);
}

void HeatEquationSolver::apply_bottom_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = 0;
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        const double aW = get_aW(i, j);
        const double aE = get_aE(i, j);
        const double aN = get_aN(i, j);
        const double aS = (lambda_[i][j] * dx_ / (dy_ / 2.0)) *
                          (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
        const double aP = (1.0 + aE + aW + aN + aS);

        A[i][j][0] = -1. * aE;
        A[i][j][1] = -1. * aW;
        A[i][j][2] = aP;
        // A[i][j][3] = -1. * aS;
        A[i][j][4] = -1. * aN;

        b[i][j] = temperatureDistribution_[i][j] + aS * T_bottom_;
    }
}

void HeatEquationSolver::apply_right_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int i = numGridPoints_x_ - 1;
    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        const double aW = get_aW(i, j);
        const double aE = (lambda_[i][j] * dy_ / (dx_ / 2.0)) *
                          (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
        const double aN = get_aN(i, j);
        const double aS = get_aS(i, j);
        const double aP = (1.0 + aE + aW + aN + aS);

        // A[i][j][0] = -1. * aE;
        A[i][j][1] = -1. * aW;
        A[i][j][2] = aP;
        A[i][j][3] = -1. * aS;
        A[i][j][4] = -1. * aN;

        b[i][j] = temperatureDistribution_[i][j] + aE * (initialTemperature_ + 0.005 * currentTime_);
    }
}

void HeatEquationSolver::apply_top_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = numGridPoints_y_ - 1;
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        const double aW = get_aW(i, j);
        const double aE = get_aE(i, j);
        const double aN = 0.0;
        const double aS = get_aS(i, j);
        const double aP = (1.0 + aE + aW + aN + aS);

        A[i][j][0] = -1. * aE;
        A[i][j][1] = -1. * aW;
        A[i][j][2] = aP;
        A[i][j][3] = -1. * aS;
        // A[i][j][4] = -1. * aN;

        b[i][j] = temperatureDistribution_[i][j] + (top_flux_ * dx_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    }
}

void HeatEquationSolver::apply_left_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int i = 0;
    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        const double aW = (alpha_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
        const double aE = get_aE(i, j);
        const double aN = get_aN(i, j);
        const double aS = get_aS(i, j);
        const double aP = (1.0 + aE + aW + aN + aS);

        A[i][j][0] = -1. * aE;
        // A[i][j][1] = -1. * aW;
        A[i][j][2] = aP;
        A[i][j][3] = -1. * aS;
        A[i][j][4] = -1. * aN;

        b[i][j] = temperatureDistribution_[i][j] + (alpha_inf_ * T_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    }
}

void HeatEquationSolver::apply_left_bottom_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = 0;
    static const int i = 0;

    const double aW = (alpha_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    const double aE = get_aE(i, j);
    const double aN = get_aN(i, j);
    const double aS = (lambda_[i][j] * dx_ / (dy_ / 2.0)) *
                      (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    const double aP = (1.0 + aE + aW + aN + aS);

    A[i][j][0] = -1. * aE;
    // A[i][j][1] = -1. * aW;
    A[i][j][2] = aP;
    // A[i][j][3] = -1. * aS;
    A[i][j][4] = -1. * aN;

    b[i][j] = temperatureDistribution_[i][j] + aS * T_bottom_ + (alpha_inf_ * T_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

void HeatEquationSolver::apply_right_bottom_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = 0;
    static const int i = numGridPoints_x_ - 1;

    const double aW = get_aW(i, j);
    const double aE = (lambda_[i][j] * dy_ / (dx_ / 2.0)) *
                      (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    const double aN = get_aN(i, j);
    const double aS = (lambda_[i][j] * dx_ / (dy_ / 2.0)) *
                      (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));

    const double aP = (1.0 + aE + aW + aN + aS);

    // A[i][j][0] = -1. * aE;
    A[i][j][1] = -1. * aW;
    A[i][j][2] = aP;
    // A[i][j][3] = -1. * aS;
    A[i][j][4] = -1. * aN;

    b[i][j] = temperatureDistribution_[i][j] + aS * T_bottom_ + aW * (initialTemperature_ + 0.005 * currentTime_);
}

void HeatEquationSolver::apply_left_top_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = numGridPoints_y_ - 1;
    static const int i = 0;

    const double aW = (alpha_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    const double aE = get_aE(i, j);
    const double aN = 0.0;
    const double aS = get_aS(i, j);
    const double aP = (1.0 + aE + aW + aN + aS);

    A[i][j][0] = -1. * aE;
    // A[i][j][1] = -1. * aW;
    A[i][j][2] = aP;
    A[i][j][3] = -1. * aS;
    // A[i][j][4] = -1. * aN;

    b[i][j] = temperatureDistribution_[i][j] +
              top_flux_ * dx_ * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_))) +
              (alpha_inf_ * T_inf_ * dy_) * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

void HeatEquationSolver::apply_right_top_corner_boundary_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    static const int j = numGridPoints_y_ - 1;
    static const int i = numGridPoints_x_ - 1;

    const double aW = get_aW(i, j);
    const double aE = (lambda_[i][j] * dy_ / (dx_ / 2.0)) *
                      (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
    const double aN = 0.0;
    const double aS = get_aS(i, j);
    const double aP = (1.0 + aE + aW + aN + aS);

    // A[i][j][0] = -1. * aE;
    A[i][j][1] = -1. * aW;
    A[i][j][2] = aP;
    A[i][j][3] = -1. * aS;
    // A[i][j][4] = -1. * aN;

    b[i][j] = temperatureDistribution_[i][j] + aE * (initialTemperature_ + 0.005 * currentTime_) + top_flux_ * dx_ * (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

inline const double HeatEquationSolver::get_aW(const int i, const int j)
{
    return (geometric_average_x(lambda_[i - 1][j], lambda_[i][j]) * dy_ / (x_[i] - x_[i - 1])) *
           (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

inline const double HeatEquationSolver::get_aE(const int i, const int j)
{
    return (geometric_average_x(lambda_[i + 1][j], lambda_[i][j]) * dy_ / (x_[i + 1] - x_[i])) *
           (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

inline const double HeatEquationSolver::get_aS(const int i, const int j)
{
    return (geometric_average_x(lambda_[i][j - 1], lambda_[i][j]) * dx_ / (y_[j] - y_[j - 1])) *
           (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

inline const double HeatEquationSolver::get_aN(const int i, const int j)
{
    return (geometric_average_x(lambda_[i][j + 1], lambda_[i][j]) * dx_ / (y_[j + 1] - y_[j])) *
           (timeStep_ / (rho_[i][j] * cp_[i][j] * (dx_ * dy_)));
}

void HeatEquationSolver::apply_internal_coefficients(std::vector<std::vector<std::vector<double>>> &A, std::vector<std::vector<double>> &b)
{
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            const double aW = get_aW(i, j);
            const double aE = get_aE(i, j);
            const double aN = get_aN(i, j);
            const double aS = get_aS(i, j);
            const double aP = (1.0 + aE + aW + aN + aS);

            A[i][j][0] = -1. * aE;
            A[i][j][1] = -1. * aW;
            A[i][j][2] = aP;
            A[i][j][3] = -1. * aS;
            A[i][j][4] = -1. * aN;

            b[i][j] = temperatureDistribution_[i][j];
        }
    }
}

std::pair<std::vector<std::vector<std::vector<double>>>, std::vector<std::vector<double>>> HeatEquationSolver::fillMatrixAAndVectorB()
{
    static const int diagonals = 5;

    std::vector<std::vector<std::vector<double>>> A(numGridPoints_x_, std::vector<std::vector<double>>(numGridPoints_y_, std::vector<double>(diagonals, 0.0)));
    std::vector<std::vector<double>> b(numGridPoints_x_, std::vector<double>(numGridPoints_y_, 0.0));

    apply_left_boundary_coefficients(A, b);
    apply_right_boundary_coefficients(A, b);
    apply_top_boundary_coefficients(A, b);
    apply_bottom_boundary_coefficients(A, b);

    apply_right_top_corner_boundary_coefficients(A, b);
    apply_left_top_corner_boundary_coefficients(A, b);
    apply_right_bottom_corner_boundary_coefficients(A, b);
    apply_left_bottom_corner_boundary_coefficients(A, b);

    apply_internal_coefficients(A, b);

    return std::make_pair(A, b);
}

void HeatEquationSolver::solveImplicit()
{
    // Fill the matrix A and vector b for the current time step
    auto [A, b] = fillMatrixAAndVectorB();

    // Solve the system of equations using Gauss-Seidel
    gaussSeidel(A, b, solverMaxIteration_, solverTolerance_, temperatureDistribution_, solverRelaxation_);
}
