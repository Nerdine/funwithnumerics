num_isolines = 5  # Adjust the number of isolines as needed
min_value = 23
max_value = 33
my_data = "finalResults.csv"


# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output "results/plots/temperature_2D_grid.png"

# Set the plot title and labels
set title "Temperature 2D Grid"
set xlabel "X"
set ylabel "Y"

# Specify the color range based on your data
set cbrange [min_value:max_value]  # Replace min_value and max_value

# Customize the color map
set pm3d map
set palette defined (0 "blue", 1 "green", 2 "yellow", 3 "red")

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"

# Create the 2D grid plot from the CSV data file
splot my_data using 1:2:3 with image

# Save the plot to a file and close Gnuplot
set output
quit
