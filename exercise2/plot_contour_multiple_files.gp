min_value = 8
max_value = 33

# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"

# Set the plot title and labels
set title "Temperature 2D Grid"
set xlabel "X"
set ylabel "Y"

# Specify the color range based on your data
set cbrange [min_value:max_value]

# Customize the color map
set pm3d map
set palette defined (0 "blue", 1 "green", 2 "yellow", 3 "red")

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"

do for [i=1:500] {
    # Construct the filename for each iteration
    my_data = sprintf("results/video_resources/finalResults_%d.csv", i)

    # Set the output filename for each iteration
    set output sprintf("results/plots/temperature_2D_grid_%d.png", i)

    # Create the 2D grid plot from the CSV data file
    splot my_data using 1:2:3 with image
}

# Close Gnuplot
quit
