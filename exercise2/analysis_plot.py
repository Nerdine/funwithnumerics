import matplotlib.pyplot as plt
import numpy as np

# Data for Error%-d analysis

'''

e1 = [4.7855, 0.2086, 0.0690, 0.0349]
e2 = [3.4291, 0.1363, 0.0442, 0.0221]
x = [0.1, 0.02, 0.01, 0.005]
'''


# Data for Error%-dt analysis
'''
e1 = [0.6683, 0.2086, 0.1613, 0.1596]
e2 = [0.0442, 0.1363, 0.1400,0.1446]
x = [100,10,1,0.1]

'''


# Data for Error%-epsilon analysis

'''
e1 = [15.4356,0.3192,0.2086,0.1596,0.2086]
e2 = [13.3350,0.2081,0.1372,0.1446,0.1363]
x = [10e-2,10e-4,10e-6,10e-8, 10e-10]
'''
'''
# Data for Error%-d analysis comparing towards the finest numerical result

e1 = [4.7643, 0.0416, 0.0083 , 0.0000]
e2 = [3.4076, 0.0276, 0.0000, 0.0000]
x = [0.1, 0.01, 0.005, 0.0025]
'''

# Data for Error%-dt analysis comparing towards the finest numerical result

e1 = [0.5079, 0.0500, 0.0000, 0.0000, 0.0000]
e2 = [0.0922, 0.0000, 0.0000, 0.0000, 0.0000]
x = [100,10,1,0.1,0.01] 


# Convert lists to numpy arrays
e1 = np.array(e1)
e2 = np.array(e2)
x = np.array(x)

# Create the plot
plt.figure(figsize=(8, 6))

# Plot e1 and e2 against x with logarithmic axes
plt.plot(x, e1, label='Error T1%', marker='o')
plt.plot(x, e2, label='Error T2%', marker='o')

# Set logarithmic scale for x-axis and y-axis
plt.xscale('log')
plt.yscale('log')

# Label axes and add legend
#xlab = 'ε'
xlab = 'dt'
#xlab = 'd'
plt.xlabel(xlab)
plt.ylabel('Error%')

plt.legend()

# Show plot
plt.grid(True)
#plt.show()
plt.savefig('results/plots/Error_'+xlab+'.pdf', format='pdf')
