#include "heat_equation_solver.h"
#include <iostream>
#include <vector>

int main() {
    const std::string& configurationFile = "heat_equation_config.yaml";

    // Create an instance of the HeatEquationSolver
    HeatEquationSolver heatSolver(configurationFile);

    // Solve the heat equation
    heatSolver.solve();

    return 0;
}
