import numpy as np
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import plotly.graph_objects as go


# Define the neural network model
class DNN(tf.keras.Model):
    def __init__(self, layers):
        super(DNN, self).__init__()
        self.hidden_layers = [tf.keras.layers.Dense(neurons, activation='sigmoid') for neurons in layers]
        self.output_layer = tf.keras.layers.Dense(1, activation=None)

    def call(self, x):
        for layer in self.hidden_layers:
            x = layer(x)
        return self.output_layer(x)


# Define the PDE and boundary conditions for a 2D problem
def advection_pde(x, model):
    with tf.GradientTape(persistent=True) as tape:
        tape.watch(x)
        u = model(x)
        u_x = tape.gradient(u, x)
    return u_x


def boundary_condition(x):
    return np.sin(2 * np.pi * x[:, 0]) * np.cos(4 * np.pi * x[:, 1]) + 1


# Define the loss function
def loss_function(model, x_collocation, x_boundary):
    x_collocation = tf.convert_to_tensor(x_collocation)
    x_boundary = tf.convert_to_tensor(x_boundary)

    u_pred = model(x_collocation)
    pde_residual = advection_pde(x_collocation, model)
    boundary_residual = model(x_boundary) - boundary_condition(x_boundary)

    return tf.reduce_mean(tf.square(pde_residual)) + tf.reduce_mean(tf.square(boundary_residual))


# Training function
def train(model, x_collocation, x_boundary, epochs, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate)
    for epoch in range(epochs):
        with tf.GradientTape() as tape:
            loss = loss_function(model, x_collocation, x_boundary)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        if epoch % 100 == 0:
            print(f"Epoch {epoch}, Loss: {loss.numpy()}")


# Generate collocation points
x_collocation = np.random.rand(100, 2).astype(np.float32)  # 2D collocation points
x_boundary = np.random.rand(20, 2).astype(np.float32)  # Boundary points

# Define and train the model
layers = [10, 10, 10, 10, 10]  # Five hidden layers with 10 neurons each
model = DNN(layers)
train(model, x_collocation, x_boundary, epochs=5000, learning_rate=0.001)

# Test the model
x_test = np.random.rand(100, 2).astype(np.float32)  # 2D test points
u_test = model(x_test).numpy()
x_test = x_test.reshape(-1, 2)

# Ensure boundary condition is evaluated correctly
boundary_test = boundary_condition(x_test)

# Plot the results using Plotly
fig = go.Figure()

fig.add_trace(go.Scatter3d(
    x=x_test[:, 0], y=x_test[:, 1], z=u_test.flatten(),
    mode='markers',
    marker=dict(size=5, color='blue'),
    name='Predicted'
))

fig.add_trace(go.Scatter3d(
    x=x_test[:, 0], y=x_test[:, 1], z=boundary_test,
    mode='markers',
    marker=dict(size=5, color='red'),
    name='Analytical'
))

fig.update_layout(
    title='2D Advection Equation Solution',
    scene=dict(
        xaxis_title='x',
        yaxis_title='y',
        zaxis_title='u'
    ),
    legend=dict(x=0, y=1)
)
fig.write_image("Case2D.svg")
fig.write_image("Case2D.png")
fig.show()
