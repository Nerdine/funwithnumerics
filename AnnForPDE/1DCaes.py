import numpy as np
import os
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import tensorflow as tf
import plotly.graph_objects as go

# Define the neural network model
class DNN(tf.keras.Model):
    def __init__(self, layers):
        super(DNN, self).__init__()
        self.hidden_layers = [tf.keras.layers.Dense(neurons, activation='sigmoid') for neurons in layers]
        self.output_layer = tf.keras.layers.Dense(1, activation=None)

    def call(self, x):
        for layer in self.hidden_layers:
            x = layer(x)
        return self.output_layer(x)

# Define the PDE and boundary conditions
def advection_pde(x, model):
    with tf.GradientTape() as tape:
        tape.watch(x)
        u = model(x)
    u_x = tape.gradient(u, x)
    return u_x

def boundary_condition(x):
    return np.sin(2 * np.pi * x) * np.cos(4 * np.pi * x) + 1

# Define the loss function
def loss_function(model, x_collocation, x_boundary):
    x_collocation = tf.convert_to_tensor(x_collocation)
    x_boundary = tf.convert_to_tensor(x_boundary)

    u_pred = model(x_collocation)
    pde_residual = advection_pde(x_collocation, model)
    boundary_residual = model(x_boundary) - boundary_condition(x_boundary)

    return tf.reduce_mean(tf.square(pde_residual)) + tf.reduce_mean(tf.square(boundary_residual))

# Training function
def train(model, x_collocation, x_boundary, epochs, learning_rate):
    optimizer = tf.keras.optimizers.Adam(learning_rate)
    for epoch in range(epochs):
        with tf.GradientTape() as tape:
            loss = loss_function(model, x_collocation, x_boundary)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))
        if epoch % 100 == 0:
            print(f"Epoch {epoch}, Loss: {loss.numpy()}")

# Generate collocation points for 1D problem
x_collocation = np.linspace(0, 1, 100).reshape(-1, 1).astype(np.float32)
x_boundary = np.array([[0]], dtype=np.float32)

# Define and train the model for 1D problem
layers = [10, 10]  # Two hidden layers with 10 neurons each
model = DNN(layers)
train(model, x_collocation, x_boundary, epochs=5000, learning_rate=0.001)

# Test the model for 1D problem
x_test = np.linspace(0, 1, 100).reshape(-1, 1).astype(np.float32)
u_test = model(x_test).numpy()

# Plot the 1D results using Plotly
fig1d = go.Figure()
fig1d.add_trace(go.Scatter(
    x=x_test.flatten(), y=u_test.flatten(),
    mode='lines', name='Predicted', line=dict(color='blue')
))
fig1d.add_trace(go.Scatter(
    x=x_test.flatten(), y=boundary_condition(x_test).flatten(),
    mode='lines', name='Analytical', line=dict(color='red')
))
fig1d.update_layout(
    title='1D Advection Equation Solution',
    xaxis_title='x',
    yaxis_title='u'
)
fig1d.show()