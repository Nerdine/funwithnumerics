#include "src/burger_equation_solver.h"
#include <iostream>
#include <vector>

int main()
{
    const std::string& configurationFile = "burger_equation_config.yaml";

    // Create an instance of the NavierStocksSolver
    BurgerEquationSolver BESolver(configurationFile);

    // Solve the heat equation
    BESolver.solve();

    return 0;
}
