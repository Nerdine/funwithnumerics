# my_data = "results/grid.csv"  # Default value
# output_file = "results/grid.png"  # Default value

# Check if parameters are provided from the command line
if (ARGC > 1) { my_data = ARGV[1] }
if (ARGC > 2) { output_file = ARGV[2] }

# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output output_file

# Set the plot title and labels for x:property
set title "E_k vs k"
set xlabel "k"
set ylabel "E_k"

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"
# set yrange [0:1]
set logscale xy
set format y "%.0e"

# Plot x against property with different line types and no markers

plot my_data using 1:2 with lines linecolor "blue" notitle

# Save the plot to a file and close Gnuplot
set output
quit
