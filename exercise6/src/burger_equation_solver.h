#ifndef BURGER_EQUATION_SOLVER_H
#define BURGER_EQUATION_SOLVER_H

#include <vector>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <complex>
#include <functional>

using namespace std;

typedef std::vector<double> vec;
typedef std::vector<std::complex<double>> vec_complex;

class BurgerEquationSolver
{
public:
    // Constructor to initialize the solver with parameters
    BurgerEquationSolver(const std::string &filename);
    void solve();

private:
    // private methods
    void initialize_variables();

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string &filename);

    void solveExplicit();

    void evaluate_Ru();
    void evaluate_u();
    void evaluate_nu_eff_LES();
    void evaluate_nu_eff_DNS();
    
    const double get_maximum_of_changes(const vec_complex &old_field, const vec_complex &new_field);
    void store_property_in_temp_array(const vec_complex &source, vec_complex &destination);
    void WriteEnergySpectrum();

    // private variables
    std::function<void()> evaluate_nu_eff;
    vec_complex u_new, u, R_u, R_uold;
    vec Nu_eff;

    int Re_;
    int N_;
    double Nu_;
    double C1_;
    double Ck_;
    double timeStep_;
    double initialValue_;
    double convergence_criteria_;
    double currentTime_;
    double coef_R;
    double coef_R_old;

    std::string finalResultOutputFileName_;
    std::string timeIntegrationScheme_;
    std::string approach_;
};

#endif // BURGER_EQUATION_SOLVER_H
