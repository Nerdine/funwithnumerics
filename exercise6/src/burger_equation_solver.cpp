#include "burger_equation_solver.h"

#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"

//-------------------------------------------------------------

BurgerEquationSolver::BurgerEquationSolver(const std::string &filename)
{
	readConfigFromYAML(filename);

	std::string case_name = "_Re_" + std::to_string(int(Re_)) + "_N_" + std::to_string(N_) + "_" + approach_;
	finalResultOutputFileName_ = finalResultOutputFileName_ + case_name + ".csv";

	initialize_variables();

	if (approach_ != "DNS" && approach_ != "LES")
	{
		throw std::runtime_error("Invalid approach specified");
	}

	if (approach_ == "DNS")
	{
		evaluate_nu_eff = std::bind(&BurgerEquationSolver::evaluate_nu_eff_DNS, this);
	}
	else if (approach_ == "LES")
	{
		evaluate_nu_eff = std::bind(&BurgerEquationSolver::evaluate_nu_eff_LES, this);
	}

	if (timeIntegrationScheme_ == "Euler")
	{
		coef_R = 1.0;
		coef_R_old = 0.0;
	}
	else
	{
		coef_R = 1.5;
		coef_R_old = -0.5;
	}
}

// Method to read configuration parameters from a YAML file
bool BurgerEquationSolver::readConfigFromYAML(const std::string &filename)
{
	try
	{
		YAML::Node config = YAML::LoadFile(filename);

		// Read configuration parameters from the YAML file
		initialValue_ = config["initialValue"].as<double>();

		convergence_criteria_ = config["convergence_criteria"].as<double>();

		Re_ = config["Reynolds_number"].as<int>();
		N_ = config["N_number"].as<int>();
		C1_ = config["C1"].as<double>();
		Ck_ = config["Ck"].as<double>();
		timeIntegrationScheme_ = config["timeIntegrationScheme"].as<std::string>();
		approach_ = config["approach"].as<std::string>();

		finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();

		return true; // Configuration read successfully
	}
	catch (const YAML::Exception &e)
	{
		std::cerr << "Error reading configuration: " << e.what() << std::endl;
		return false; // Configuration read failed
	}
}

void BurgerEquationSolver::initialize_variables()
{
	Nu_ = 1.0 / Re_;
	timeStep_ = C1_ * Re_ / (N_ * N_);

	u_new = vec_complex(N_ + 1, {0.0, 0.0});
	u = vec_complex(N_ + 1, {0.0, 0.0});
	R_u = vec_complex(N_ + 1, {0.0, 0.0});
	R_uold = vec_complex(N_ + 1, {0.0, 0.0});
	// F = vec_complex(N_ + 1, {0.0, 0.0});

	Nu_eff = vec(N_ + 1, 0.0);

	for (int k = 1; k <= N_; ++k)
	{
		u[k] = 1.0 / k;
		cout << "u_init[" << k << "]= " << u[k] << endl;
	}
};

void BurgerEquationSolver::evaluate_Ru() {
	static vec_complex conv(N_ + 1, {0.0, 0.0});

    for (int k = 2; k <= N_; ++k) {
        conv[k] = {0.0, 0.0};

        // Compute tmp[k] based on the convolution
        for (int q = -N_; q <= N_; ++q) {
            const int p = k - q;
            if (p >= -N_ && p <= N_) {
                const complex<double> a = (q >= 0) ? u[q] : conj(u[-q]);
                const complex<double> b = (p >= 0) ? u[p] : conj(u[-p]);
                conv[k] += (b * a) * complex<double>(0.0, q);
            }
        }

        // Compute R_u[k] based on the convective and dissipative terms
        R_u[k] = (-double(k * k * Nu_eff[k])) * u[k] - conv[k]; // + F[k] 
    }
}

void BurgerEquationSolver::evaluate_u()
{
	for (int k = 2; k <= N_; ++k)
	{
		u_new[k] = u[k] + timeStep_ * (coef_R * R_u[k] + coef_R_old * R_uold[k]);
	}
}

void BurgerEquationSolver::evaluate_nu_eff_DNS()
{
	for (int k = 2; k <= N_; ++k)
	{
		Nu_eff[k] = Nu_;
	}
}

void BurgerEquationSolver::evaluate_nu_eff_LES()
{
	static double m = 2.0;
	static double Nu_tinf = 0.31 * (5 - m) / (m + 1) * sqrt(3 - m) * pow(Ck_, -1.5);

	double E_kN = abs(u[N_] * conj(u[N_]));

	for (int k = 2; k <= N_; ++k)
	{
		Nu_eff[k] = Nu_ + Nu_tinf * sqrt(E_kN / N_) * (1.0 + 34.5 * exp(-3.03 * N_ / k));
	}
}

void BurgerEquationSolver::solveExplicit()
{
	evaluate_nu_eff();

	evaluate_Ru();
	evaluate_u();
};

const double BurgerEquationSolver::get_maximum_of_changes(const vec_complex &old_field, const vec_complex &new_field)
{
	// Determine the size of the fields
	static const int size_ = old_field.size();

	// Calculate the maximum change in the scalar field
	double maxChange = 0.0;
	for (int i = 2; i < old_field.size(); ++i)
	{
		double delta = std::abs(old_field[i] - new_field[i]);
		maxChange = std::max(maxChange, delta) / timeStep_;
	}

	std::cout << "Convergence Rate u is: " << maxChange << std::endl;

	return maxChange;
}

void BurgerEquationSolver::store_property_in_temp_array(const vec_complex &source, vec_complex &destination)
{
	// Copy values from source to destination
	for (size_t i = 2; i < source.size(); ++i)
	{
		destination[i] = source[i];
	}
}

void BurgerEquationSolver::solve()
{
	double convergence_rate = 1.e10;
	int itr = 0;

	// Main loop
	while (convergence_rate > convergence_criteria_)
	{
		std::cout << "-------------------------------------" << std::endl;
		std::cout << "Iteration: " << itr << ", dt: " << timeStep_ << ", solving for time: " << currentTime_ << std::endl;

		// Solve the equations explicitly
		solveExplicit();

		// Advance the current time
		currentTime_ += timeStep_;

		convergence_rate = get_maximum_of_changes(u, u_new);
		store_property_in_temp_array(u_new, u);
		store_property_in_temp_array(R_u, R_uold);		

		itr++;
	}

	// Write energy spectrum
	WriteEnergySpectrum();
}


void BurgerEquationSolver::WriteEnergySpectrum()
{
	// Open the output file
	std::ofstream output(finalResultOutputFileName_);

	// Check if the output file is opened successfully
	if (!output.is_open())
	{
		throw std::runtime_error("Unable to open output file");
	}

	// Write header to output file
	output << "k, E(k)" << std::endl;

	// Compute and write energy spectrum for each wave number k
	for (int k = 1; k <= N_; ++k)
	{
		// Compute energy E(k) for wave number k
		std::complex<double> E = std::norm(u[k]);

		// Write wave number and energy to output file
		output << k << ", " << real(E) << std::endl;
	}

	// Close the output file
	output.close();
}