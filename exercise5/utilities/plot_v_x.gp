# my_data = "results/grid.csv"  # Default value
# output_file = "results/grid.png"  # Default value

# Check if parameters are provided from the command line
if (ARGC > 1) { my_data = ARGV[1] }
if (ARGC > 2) { output_file = ARGV[2] }

# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output output_file

# Set the plot title and labels for x:property
set title "V vs x"
set xlabel "X"
set ylabel "V"

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"
set xrange [0:1]

# Plot x against property with a blue line and no markers
plot my_data using 1:3 with lines linecolor "blue" notitle

# plot "results/v_x_Re_3200_mesh_32.csv" using 1:3 with linespoints title "h=L/32" linetype 2 pi 5, \
#      "results/v_x_Re_3200_mesh_64.csv" using 1:3 with linespoints title "h=L/64" linetype 3 pi 10, \
#      "results/v_x_Re_3200_mesh_128.csv" using 1:3 with linespoints title "h=L/128" linetype 4 pi 20, \
#      "results/v_x_Re_3200_mesh_320.csv" using 1:3 with linespoints title "h=L/320" linetype 5 pi 60, \
#      "results/v_x_Re_3200_mesh_480.csv" using 1:3 with linespoints title "h=L/480" linetype 6 pi 80, \
#      "Guia-v-x.csv" using 1:2 with points pointtype 7 pointsize 1 linecolor "red" title "Ref"


# Save the plot to a file and close Gnuplot
set output
quit

