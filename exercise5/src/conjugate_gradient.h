#ifndef CONJUGATE_GRADIENT_H
#define CONJUGATE_GRADIENT_H

#include <vector>

typedef std::vector<double> vec;
typedef std::vector<std::vector<double>> vec2d;

void conjugateGradient(
    const std::vector<vec2d> &A,
    const vec2d &b,
    int maxIterations,
    double tolerance,
    vec2d &x
    );

#endif // CONJUGATE_GRADIENT_H
