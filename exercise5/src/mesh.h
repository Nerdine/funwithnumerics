#ifndef mesh_H
#define mesh_H

#include <vector>
#include <fstream>
#include <tuple>

typedef std::vector<double> vec;
typedef std::vector<std::vector<double>> vec2d;

class Mesh
{
public:
    Mesh(const int &numGridPoints_x_,
         const int &numGridPoints_y_,
         const double &GeometryWidth_,
         const double &GeometryHeight_,
         const double &refinedGridStretchingFactor_x_,
         const double &refinedGridStretchingFactor_y_,
         const std::string mesh_type);
    Mesh();

    vec x, y, xfe, xfw, yfn, yfs;
    vec2d P, Pold, T, Told, u, uold, up, v, vold, vp;

    void create_uniform_mesh();
    void create_refined_mesh();
    inline double non_uniform_mesh_alpha(double i, int numGridPoints, double& refinedGridStretchingFactor_);
    double evaluate_dx_non_uniform_mesh(double i, int numGridPoints, double& refinedGridStretchingFactor_);

    // Helper functions to return, center (c), east (e), west (w), north (n), and south (s)
    // locations of pressure mesh
    inline double x_p_c(const double &i, const double &j) { return x[i]; };
    inline double y_p_c(const double &i, const double &j) { return y[j]; };
    inline double x_p_e(const double &i, const double &j) { return xfe[i]; };
    inline double x_p_w(const double &i, const double &j) { return xfw[i]; };
    inline double y_p_n(const double &i, const double &j) { return yfn[j]; };
    inline double y_p_s(const double &i, const double &j) { return yfs[j]; };
    inline double dx_p(const double &i, const double &j) { return (xfe[i] - xfw[i]); };    
    inline double dy_p(const double &i, const double &j) { return (yfn[j] - yfs[j]); };        
    inline double dv_p(const double &i, const double &j) { return (dx_p(i,j)* dy_p(i,j)); };        

    // Helper functions to return, center (c), east (e), west (w), north (n), and south (s)
    // locations of U velocity mesh
    inline double x_u_c(const double &i, const double &j) { return xfw[i]; };
    inline double y_u_c(const double &i, const double &j) { return y_p_c(i,j); };
    inline double x_u_e(const double &i, const double &j) { return x[i]; };
    inline double x_u_w(const double &i, const double &j) { return x[i - 1]; };
    inline double y_u_n(const double &i, const double &j) { return yfn[j]; };
    inline double y_u_s(const double &i, const double &j) { return yfs[j]; };
    inline double dx_u(const double &i, const double &j) { return (x[i] - x[i-1]); };    
    inline double dy_u(const double &i, const double &j) { return (yfn[j] - yfs[j]); };        
    inline double dv_u(const double &i, const double &j) { return (dx_u(i,j)* dy_u(i,j)); };

    // Helper functions to return, center (c), east (e), west (w), north (n), and south (s)
    // locations of V velocity mesh
    inline double x_v_c(const double &i, const double &j) { return x_p_c(i,j); };
    inline double y_v_c(const double &i, const double &j) { return yfs[j]; };
    inline double x_v_e(const double &i, const double &j) { return xfe[i]; };
    inline double x_v_w(const double &i, const double &j) { return xfw[i]; };
    inline double y_v_n(const double &i, const double &j) { return y[j]; };
    inline double y_v_s(const double &i, const double &j) { return y[j - 1]; };
    inline double dx_v(const double &i, const double &j) { return dx_p(i,j); };    
    inline double dy_v(const double &i, const double &j) { return (y[j] - y[j-1]); };        
    inline double dv_v(const double &i, const double &j) { return (dx_v(i,j)* dy_v(i,j)); };

private:
    int size_x_;
    int size_y_;
    double L_;
    double H_;
    double refinedGridStretchingFactor_x_;
    double refinedGridStretchingFactor_y_;
};

#endif // mesh_H
