#include "gauss_seidel.h"
#include <iostream>
#include <cmath>

void gaussSeidel(
    const std::vector<vec2d> &A,
    const vec2d &b,
    int maxIterations,
    double tolerance,
    vec2d &x,
    double omega)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    for (int iter = 0; iter < maxIterations; iter++)
    {
        double maxError = 0.0;

        // for (int i = 0; i < numGridPoints_x_; i++)
        // {
        //     for (int j = 0; j < numGridPoints_y_; j++)
        //     {
        //         const double aE = A[i][j][0];
        //         const double aW = A[i][j][1];
        //         const double aP = A[i][j][2];
        //         const double aS = A[i][j][3];
        //         const double aN = A[i][j][4];

        //         double sigma = 0.0;
        //         if(i==0)
        //         {
        //             if(j==0)
        //             {
        //                 sigma = aE * x[i + 1][j] +
        //                         // aW * x[i - 1][j] +
        //                         aN * x[i][j + 1];
        //                         // aS * x[i][j - 1];
        //             }
        //             else if(j==numGridPoints_y_-1)
        //             {
        //                 sigma = aE * x[i + 1][j] +
        //                         // aW * x[i - 1][j] +
        //                         // aN * x[i][j + 1] +
        //                         aS * x[i][j - 1];
        //             }
        //             else
        //             {
        //                 sigma = aE * x[i + 1][j] +
        //                         // aW * x[i - 1][j] +
        //                         aN * x[i][j + 1] +
        //                         aS * x[i][j - 1];
        //             }
        //         }
        //         else if(i==numGridPoints_x_-1)
        //         {
        //             if(j==0)
        //             {
        //                 sigma = aW * x[i - 1][j] +
        //                         // aE * x[i + 1][j] +
        //                         aN * x[i][j + 1];
        //                         // aS * x[i][j - 1];
        //             }
        //             else if(j==numGridPoints_y_-1)
        //             {
        //                 sigma = aW * x[i - 1][j] +
        //                         // aE * x[i + 1][j] +
        //                         // aN * x[i][j + 1] +
        //                         aS * x[i][j - 1];
        //             }
        //             else
        //             {
        //                 sigma = aW * x[i - 1][j] +
        //                         // aE * x[i + 1][j] +
        //                         aN * x[i][j + 1] +
        //                         aS * x[i][j - 1];
        //             }
        //         } else
        //         {
        //                 sigma = aW * x[i - 1][j] +
        //                         aE * x[i + 1][j] +
        //                         aN * x[i][j + 1] +
        //                         aS * x[i][j - 1];
        //         }

        //         const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
        //         maxError = std::max(maxError, std::abs(x[i][j] - x_new));
        //         x[i][j] = x_new;
        //     }
        // }

        solve_left_boundary_points(A, b, x, omega, maxError);
        solve_right_boundary_points(A, b, x, omega, maxError);
        solve_top_boundary_points(A, b, x, omega, maxError);
        solve_bottom_boundary_points(A, b, x, omega, maxError);

        solve_left_bottom_corner_boundary_points(A, b, x, omega, maxError);
        solve_right_bottom_corner_boundary_points(A, b, x, omega, maxError);
        solve_left_top_corner_boundary_points(A, b, x, omega, maxError);
        solve_right_top_corner_boundary_points(A, b, x, omega, maxError);

        solve_internal_points(A, b, x, omega, maxError);

        if (maxError < tolerance)
        {
            std::cout << "Gauss-Seidel solver returning after " << iter + 1 << " iterations." << std::endl;
            return;
        }
    }

    std::cout << "Gauss-Seidel did not converge within " << maxIterations << " iterations." << std::endl;
}

void solve_internal_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            const double aE = A[i][j][0];
            const double aW = A[i][j][1];
            const double aP = A[i][j][2];
            const double aS = A[i][j][3];
            const double aN = A[i][j][4];

            double sigma = aE * x[i + 1][j] +
                           aW * x[i - 1][j] +
                           aN * x[i][j + 1] +
                           aS * x[i][j - 1];

            const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
            maxError = std::max(maxError, std::abs(x[i][j] - x_new));
            x[i][j] = x_new;
        }
    }
}

void solve_left_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = 0;

    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        const double aE = A[i][j][0];
        const double aW = A[i][j][1];
        const double aP = A[i][j][2];
        const double aS = A[i][j][3];
        const double aN = A[i][j][4];

        double sigma = aE * x[i + 1][j] +
                       //    aW * x[i - 1][j] +
                       aN * x[i][j + 1] +
                       aS * x[i][j - 1];

        const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
        maxError = std::max(maxError, std::abs(x[i][j] - x_new));
        x[i][j] = x_new;
    }
}

void solve_right_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = numGridPoints_x_ - 1;

    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        const double aE = A[i][j][0];
        const double aW = A[i][j][1];
        const double aP = A[i][j][2];
        const double aS = A[i][j][3];
        const double aN = A[i][j][4];

        double sigma = aW * x[i - 1][j] +
                       // aE * x[i + 1][j] +
                       aN * x[i][j + 1] +
                       aS * x[i][j - 1];

        const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
        maxError = std::max(maxError, std::abs(x[i][j] - x_new));
        x[i][j] = x_new;
    }
}

void solve_top_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int j = numGridPoints_y_ - 1;

    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        const double aE = A[i][j][0];
        const double aW = A[i][j][1];
        const double aP = A[i][j][2];
        const double aS = A[i][j][3];
        const double aN = A[i][j][4];

        double sigma = aW * x[i - 1][j] +
                       aE * x[i + 1][j] +
                       // aN * x[i][j + 1] +
                       aS * x[i][j - 1];

        const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
        maxError = std::max(maxError, std::abs(x[i][j] - x_new));
        x[i][j] = x_new;
    }
}

void solve_bottom_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int j = 0;

    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {

        const double aE = A[i][j][0];
        const double aW = A[i][j][1];
        const double aP = A[i][j][2];
        const double aS = A[i][j][3];
        const double aN = A[i][j][4];

        double sigma = aW * x[i - 1][j] +
                       aE * x[i + 1][j] +
                       aN * x[i][j + 1];
        // aS * x[i][j - 1];

        const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
        maxError = std::max(maxError, std::abs(x[i][j] - x_new));
        x[i][j] = x_new;
    }
}

void solve_left_bottom_corner_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = 0;
    static const int j = 0;

    const double aE = A[i][j][0];
    const double aW = A[i][j][1];
    const double aP = A[i][j][2];
    const double aS = A[i][j][3];
    const double aN = A[i][j][4];

    double sigma = aE * x[i + 1][j] +
                   // aW * x[i - 1][j] +
                   aN * x[i][j + 1];
    // aS * x[i][j - 1];

    const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
    maxError = std::max(maxError, std::abs(x[i][j] - x_new));
    x[i][j] = x_new;
}

void solve_right_bottom_corner_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = numGridPoints_x_ - 1;
    static const int j = 0;

    const double aE = A[i][j][0];
    const double aW = A[i][j][1];
    const double aP = A[i][j][2];
    const double aS = A[i][j][3];
    const double aN = A[i][j][4];

    double sigma = aW * x[i - 1][j] +
                   // aE * x[i + 1][j] +
                   aN * x[i][j + 1];
    // aS * x[i][j - 1];

    const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
    maxError = std::max(maxError, std::abs(x[i][j] - x_new));
    x[i][j] = x_new;
}

void solve_left_top_corner_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = 0;
    static const int j = numGridPoints_y_ - 1;

    const double aE = A[i][j][0];
    const double aW = A[i][j][1];
    const double aP = A[i][j][2];
    const double aS = A[i][j][3];
    const double aN = A[i][j][4];

    double sigma = aE * x[i + 1][j] +
                   // aW * x[i - 1][j] +
                   //  aN * x[i][j + 1];
                   aS * x[i][j - 1];

    const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
    maxError = std::max(maxError, std::abs(x[i][j] - x_new));
    x[i][j] = x_new;
}

void solve_right_top_corner_boundary_points(const std::vector<vec2d> &A, const vec2d &b, vec2d &x, double &omega, double &maxError)
{
    size_t numGridPoints_x_ = A.size();
    size_t numGridPoints_y_ = A[0].size();

    static const int i = numGridPoints_x_ - 1;
    static const int j = numGridPoints_y_ - 1;

    const double aE = A[i][j][0];
    const double aW = A[i][j][1];
    const double aP = A[i][j][2];
    const double aS = A[i][j][3];
    const double aN = A[i][j][4];

    double sigma = aW * x[i - 1][j] +
                   // aE * x[i + 1][j] +
                   //  aN * x[i][j + 1];
                   aS * x[i][j - 1];

    const double x_new = (1.0 - omega) * x[i][j] + (omega / aP) * (b[i][j] - sigma);
    maxError = std::max(maxError, std::abs(x[i][j] - x_new));
    x[i][j] = x_new;
}