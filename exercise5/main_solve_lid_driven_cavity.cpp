#include "navier_stoks_solver.h"
#include <iostream>
#include <vector>

int main() {
    const std::string& configurationFile = "heat_driven_cavity_config.yaml";

    // Create an instance of the NavierStocksSolver
    NavierStocksSolver NSSolver(configurationFile);

    // Solve the heat equation
    NSSolver.solve();

    return 0;
}
