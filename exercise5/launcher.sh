#!/bin/bash

# Check if the user provided two arguments
if [ $# -ne 2 ]; then
    echo "No input value for <Rayleigh_number>"
    echo "No input value for <Number_of_grids>"    
    exit 1
fi

RAYLEIGH=$1
MESH=$2

# Apply the given Rayleigh and mesh to the config file
awk -v repl="$RAYLEIGH" '{gsub("RAYLEIGH_NUMBER", repl)}1' utilities/original_configuration_file.yaml > tmp.yaml
awk -v repl="$MESH" '{gsub("NUMBER_OF_GRIDS", repl)}1' tmp.yaml > heat_driven_cavity_config.yaml
rm tmp.yaml

# run the main solver
./runner.sh main_solve_lid_driven_cavity.cpp;

# plot the results
gnuplot -e "my_data='results/csvs/grid_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/grid_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_grid.gp;
gnuplot -e "my_data='results/csvs/plotU_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/plotU_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_u_y.gp;
gnuplot -e "my_data='results/csvs/plotV_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/plotV_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_v_x.gp;

gnuplot -e "my_data='results/csvs/finalResultsEntireDomain_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/contour_vel_mag_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_contour_Velocity.gp;
gnuplot -e "my_data='results/csvs/finalResultsEntireDomain_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/contour_u_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_contour_Velocity_u.gp;
gnuplot -e "my_data='results/csvs/finalResultsEntireDomain_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/contour_v_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_contour_Velocity_v.gp;
gnuplot -e "my_data='results/csvs/finalResultsEntireDomain_Ra_${RAYLEIGH}_mesh_${MESH}.csv';output_file='results/pngs/contour_T_Ra_${RAYLEIGH}_mesh_${MESH}.png'" utilities/plot_contour_Temperature.gp;
