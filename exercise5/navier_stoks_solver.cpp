#include "navier_stoks_solver.h"
#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"
#include "src/gauss_seidel.h"
#include "src/conjugate_gradient.h"
#include <algorithm> // for std::max_element

// #include <boost/filesystem.hpp>
// namespace fs = boost::filesystem;

NavierStocksSolver::NavierStocksSolver(const std::string &filename)
{
    readConfigFromYAML(filename);

    mesh = Mesh(
        numGridPoints_x_,
        numGridPoints_y_,
        GeometryWidth_,
        GeometryHeight_,
        refinedGridStretchingFactor_x_,
        refinedGridStretchingFactor_y_,
        meshType_);

    cp_ = Pr_ * lambda_ / mu_;
    gy_ = Ra_ * mu_ * lambda_ / (cp_ * beta_ * pow(rho_, 2.0) * (T_hot_ - T_cold_) * pow(GeometryWidth_, 3.));
    gx_ = 0.0;

    std::string case_name = "_Ra_" + std::to_string(int(Ra_)) + "_mesh_" + std::to_string(numGridPoints_x_);
    finalResultOutputFileName_ = finalResultOutputFileName_ + case_name + ".csv";
    plotUFileName_ = plotUFileName_ + case_name + ".csv";
    plotVFileName_ = plotVFileName_ + case_name + ".csv";
    gridOutputFileName_ = gridOutputFileName_ + case_name + ".csv";

    writeGridToCSV(gridOutputFileName_);

    if (timeIntegrationScheme_ == "Euler")
    {
        coef_R = 1.0;
        coef_R_old = 0.0;
    }
    else
    {
        coef_R = 1.5;
        coef_R_old = -0.5;
    }
}

// Method to read configuration parameters from a YAML file
bool NavierStocksSolver::readConfigFromYAML(const std::string &filename)
{
    try
    {
        YAML::Node config = YAML::LoadFile(filename);

        // Read configuration parameters from the YAML file
        GeometryWidth_ = config["GeometryWidth"].as<double>();
        GeometryHeight_ = config["GeometryHeight"].as<double>();
        initialValue_ = config["initialValue"].as<double>();

        rho_ = config["rho"].as<double>();
        scheme_ = config["scheme"].as<std::string>();

        timeStep_ = config["timeStep"].as<double>();
        adaptivetimestepCoeficient_ = config["adaptivetimestepCoeficient"].as<double>();
        timestepApproach_ = config["timestepApproach"].as<std::string>();

        numGridPoints_x_ = config["numGridPoints_x"].as<int>();
        numGridPoints_y_ = config["numGridPoints_y"].as<int>();
        refinedGridStretchingFactor_x_ = config["refinedGridStretchingFactor_x"].as<double>();
        refinedGridStretchingFactor_y_ = config["refinedGridStretchingFactor_y"].as<double>();

        convergence_criteria_ = config["convergence_criteria"].as<double>();

        Ra_ = config["Rayleigh_number"].as<double>();
        Pr_ = config["Prandtl_number"].as<double>();
        beta_ = config["beta"].as<double>();
        T_inf_ = config["T_inf"].as<double>();
        T_cold_ = config["T_cold"].as<double>();
        T_hot_ = config["T_hot"].as<double>();
        mu_ = config["mu"].as<double>();
        lambda_ = config["lambda"].as<double>();

        solverMaxIteration_ = config["solverMaxIteration"].as<int>();
        solverTolerance_ = config["solverTolerance"].as<double>();
        solverRelaxation_ = config["solverRelaxation"].as<double>();
        pressureSolver_ = config["pressureSolver"].as<std::string>();

        finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();
        plotUFileName_ = config["plotUFileName"].as<std::string>();
        plotVFileName_ = config["plotVFileName"].as<std::string>();

        gridOutputFileName_ = config["gridOutputFileName"].as<std::string>();
        timeIntegrationScheme_ = config["timeIntegrationScheme"].as<std::string>();

        meshType_ = config["meshType"].as<std::string>();

        outputInterval_ = config["outputInterval"].as<double>();

        return true; // Configuration read successfully
    }
    catch (const YAML::Exception &e)
    {
        std::cerr << "Error reading configuration: " << e.what() << std::endl;
        return false; // Configuration read failed
    }
}

void NavierStocksSolver::writeGridToCSV(std::string &filename)
{
    std::ofstream file(filename);
    if (file.is_open())
    {
        // Write column headers
        file << "variables=X, Y" << std::endl;

        // Write data for each point
        for (int i = 0; i < numGridPoints_x_; ++i)
        {
            for (int j = 0; j < numGridPoints_y_; ++j)
            {
                file << mesh.x_p_c(i, j) << "," << mesh.y_p_c(i, j) << std::endl;
            }
            file << std::endl;
        }

        file.close();
        std::cout << "CSV file '" << filename << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file " << filename << " for writing." << std::endl;
    }
};

void NavierStocksSolver::writeAveragePropertyDistributionToCSV(std::string &filename)
{
    std::ofstream file(filename);
    if (file.is_open())
    {
        // Write column headers
        file << "x, y, u, v, U_tot, p, T" << std::endl;

        // Write data for each point
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                const double u_cell = average(mesh.u[i][j], mesh.u[i + 1][j]);
                const double v_cell = average(mesh.v[i][j], mesh.v[i][j + 1]);
                const double U_tot = sqrt(u_cell * u_cell + v_cell * v_cell);
                const double P_cell = mesh.P[i][j];
                const double T_cell = mesh.T[i][j];

                file << mesh.x_p_c(i, j) << ", " << mesh.y_p_c(i, j) << ", " << u_cell << ", " << v_cell << ", " << U_tot << ", " << P_cell << ", " << T_cell << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << filename << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::writePropertyToCSV(std::string &filename, const vec2d &property)
{
    std::ofstream file(filename);
    if (file.is_open())
    {
        // Write column headers
        file << "i, j, x, y, " << filename << std::endl;

        // Write data for each point
        for (int i = 0; i < property.size(); i++)
        {
            for (int j = 0; j < property[i].size(); j++)
            {
                file << i << ", " << j << ", " << mesh.x_p_c(i, j) << ", " << mesh.y_p_c(i, j) << ", " << property[i][j] << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << filename << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::saveTheSimulationResults(double currentTime)
{
    static double lastOutputTime = 0.0;

    // Check if it's time to write output
    if (currentTime - lastOutputTime >= outputInterval_)
    {
        // Format the current time to include in the filename
        std::ostringstream oss_u, oss_v, oss_P, oss_T;
        oss_u << "results/u_" << std::fixed << std::setprecision(2) << currentTime << ".csv";
        oss_v << "results/v_" << std::fixed << std::setprecision(2) << currentTime << ".csv";
        oss_P << "results/P_" << std::fixed << std::setprecision(2) << currentTime << ".csv";
        oss_T << "results/T_" << std::fixed << std::setprecision(2) << currentTime << ".csv";

        std::string filename_u = oss_u.str();
        std::string filename_v = oss_v.str();
        std::string filename_P = oss_P.str();
        std::string filename_T = oss_T.str();

        writePropertyToCSV(filename_u, mesh.u);
        writePropertyToCSV(filename_v, mesh.v);
        writePropertyToCSV(filename_P, mesh.P);
        writePropertyToCSV(filename_T, mesh.T);

        // Update the last output time
        lastOutputTime = currentTime;
    }
}

double NavierStocksSolver::extractCurrentTime(const std::string &filename)
{
    // Find the position of the underscore before the timestamp
    size_t underscorePos = filename.find('_');
    if (underscorePos == std::string::npos)
    {
        // Handle the case where the filename format is not as expected
        return 0.0; // or any suitable default value
    }

    // Extract the substring after the underscore (timestamp)
    std::string timestampStr = filename.substr(underscorePos + 1);

    // Convert the timestamp string to a double
    try
    {
        return std::stod(timestampStr);
    }
    catch (const std::exception &e)
    {
        // Handle the case where the conversion fails
        std::cerr << "Error extracting current time from filename: " << e.what() << std::endl;
        return 0.0; // or any suitable default value
    }
}
void NavierStocksSolver::writeUAtCenterVerticalLineToCSV(std::string &filename)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(filename);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,y,U" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(filename, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // Find the grid cell index corresponding to x=0.5
        const double targetX = 0.5;
        int i_centerline = -1;

        for (int i = 0; i < numGridPoints_x_; ++i)
        {
            if (mesh.x_u_c(i, 0) >= targetX)
            {
                i_centerline = i;
                break;
            }
        }

        if (i_centerline == -1)
        {
            std::cerr << "Error: Unable to find grid cell for x=0.5." << std::endl;
            file.close();
            return;
        }

        // Initialize variables to track maximum value and its location
        double max_u_cell = std::numeric_limits<double>::lowest(); // Initialize with lowest possible value
        double max_u_x = 0.0;
        double max_u_y = 0.0;

        // Calculate the average value of u_cell along the vertical centerline
        double average_u_cell = 0.0;

        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double u_cell = linearInterpolation(mesh.x[i_centerline], mesh.u[i_centerline][j], mesh.x[i_centerline - 1], mesh.u[i_centerline - 1][j], 0.5);
            average_u_cell += u_cell;

            // Print the values
            file << mesh.x[i_centerline] << "," << mesh.y_u_c(i_centerline, j) << "," << u_cell << std::endl;

            // Update maximum value and its location if necessary
            if (u_cell > max_u_cell)
            {
                max_u_cell = u_cell;
                max_u_x = mesh.x[i_centerline];
                max_u_y = mesh.y_u_c(i_centerline, j);
            }
        }

        // Calculate and print the average value
        average_u_cell /= numGridPoints_y_;
        std::cout << "Average value of u_cell along the vertical centerline at x=0.5: " << average_u_cell << std::endl;

        // Print maximum value and its location
        const double max_u_cell_non_dim = max_u_cell / (lambda_ / (rho_ * cp_)); //- non dimentionalised
        std::cout << "Maximum value of u_cell: " << max_u_cell_non_dim << " at (x, y) = (" << max_u_x << ", " << max_u_y << ")" << std::endl;

        file.close();
        std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void NavierStocksSolver::writeVAtCenterHorizontalLineToCSV(std::string &filename)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(filename);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,y,V" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(filename, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // Find the grid cell index corresponding to y=0.5
        const double targetY = 0.5;
        int j_centerline = -1;

        for (int j = 0; j < numGridPoints_y_; ++j)
        {
            if (mesh.y_v_c(0, j) >= targetY)
            {
                j_centerline = j;
                break;
            }
        }

        if (j_centerline == -1)
        {
            std::cerr << "Error: Unable to find grid cell for y=0.5." << std::endl;
            file.close();
            return;
        }

        // Initialize variables to track maximum value and its location
        double max_v_cell = std::numeric_limits<double>::lowest(); // Initialize with lowest possible value
        double max_v_x = 0.0;
        double max_v_y = 0.0;

        // Calculate the average value of v_cell along the horizontal centerline
        double average_v_cell = 0.0;

        for (int i = 0; i < numGridPoints_x_; i++)
        {
            const double v_cell = linearInterpolation(mesh.y[j_centerline], mesh.v[i][j_centerline], mesh.y[j_centerline - 1], mesh.v[i][j_centerline - 1], 0.5);
            average_v_cell += v_cell;

            // Print the values
            file << mesh.x[i] << "," << mesh.y_v_c(i, j_centerline) << "," << v_cell << std::endl;

            // Update maximum value and its location if necessary
            if (v_cell > max_v_cell)
            {
                max_v_cell = v_cell;
                max_v_x = mesh.x[i];
                max_v_y = mesh.y_v_c(i, j_centerline);
            }
        }

        // Calculate and print the average value
        average_v_cell /= numGridPoints_x_;
        std::cout << "Average value of v_cell along the horizontal centerline at y=0.5: " << average_v_cell << std::endl;

        // Print maximum value and its location
        const double max_v_cell_non_dim = max_v_cell / (lambda_ / (rho_ * cp_)); //- non dimentionalised
        std::cout << "Maximum value of v_cell: " << max_v_cell_non_dim << " at (x, y) = (" << max_v_x << ", " << max_v_y << ")" << std::endl;

        file.close();
        std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

const double NavierStocksSolver::get_average_of_changes(
    vec2d &old_field,
    vec2d &new_field)
{
    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    double sumDifferences = 0.0;

    // Copy values from propertyDistribution_ to propertyDistribution_new
    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            double difference = std::abs(old_field[i][j] - new_field[i][j]);
            sumDifferences += difference;
        }
    }
    // Calculate the average
    double averageDifference = sumDifferences / (x_size * y_size);

    return averageDifference;
}

const double NavierStocksSolver::get_maximum_of_changes(
    const vec2d &old_field,
    const vec2d &new_field)
{

    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    // Calculate the change in the scalar field
    double maxChange = 0.0;

    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            double delta = std::abs(old_field[i][j] - new_field[i][j]);
            maxChange = std::max(maxChange, delta);
        }
    }

    return maxChange;
}

void NavierStocksSolver::store_property_in_temp_array(
    vec2d &old_field,
    const vec2d &new_field)
{
    // Determine the size of propertyDistribution_
    int x_size = old_field.size();
    int y_size = old_field[0].size();

    // Copy values from new_field to old_field
    for (int i = 0; i < x_size; ++i)
    {
        for (int j = 0; j < y_size; ++j)
        {
            old_field[i][j] = new_field[i][j];
        }
    }
}

double NavierStocksSolver::linearInterpolation(double x0, double y0, double x1, double y1, double x)
{
    // Ensure x0 is not equal to x1 to avoid division by zero
    if (x0 == x1)
    {
        return y0;
    }

    // Calculate the slope (m) and y-intercept (b) of the line between (x0, y0) and (x1, y1)
    double m = (y1 - y0) / (x1 - x0);
    double b = y0 - m * x0;

    // Use the equation of the line to interpolate the value at x
    return m * x + b;
}

double NavierStocksSolver::evaluate_convergence_rates()
{
    const double convergence_rate_u = get_maximum_of_changes(mesh.uold, mesh.u);
    const double convergence_rate_v = get_maximum_of_changes(mesh.vold, mesh.v);
    const double convergence_rate_P = get_maximum_of_changes(mesh.Pold, mesh.P);
    const double convergence_rate_T = get_maximum_of_changes(mesh.Told, mesh.T);

    std::cout << "Convergence Rate u is: " << convergence_rate_u << std::endl;
    std::cout << "Convergence Rate v is: " << convergence_rate_v << std::endl;
    std::cout << "Convergence Rate p is: " << convergence_rate_P << std::endl;
    std::cout << "Convergence Rate T is: " << convergence_rate_T << std::endl;

    // return std::max(convergence_rate_u, std::max(convergence_rate_v, convergence_rate_P));
    // return std::max(convergence_rate_u, convergence_rate_v);
    return std::max(convergence_rate_u, std::max(convergence_rate_v, convergence_rate_T));
}

double NavierStocksSolver::calculateTimeStep_conv()
{
    double max_dt = std::numeric_limits<double>::infinity();

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double u_cell = average(mesh.u[i][j], mesh.u[i + 1][j]);
            const double dt_u = adaptivetimestepCoeficient_ * mesh.dx_p(i, j) / std::fabs(u_cell);

            const double v_cell = average(mesh.v[i][j], mesh.v[i][j + 1]);
            const double dt_v = adaptivetimestepCoeficient_ * mesh.dy_p(i, j) / std::fabs(v_cell);

            max_dt = std::min(std::min(dt_u, dt_v), max_dt);
        }
    }
    return max_dt;
}

double NavierStocksSolver::calculateTimeStep_diff()
{
    double max_dt = std::numeric_limits<double>::infinity();

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double dt_u = adaptivetimestepCoeficient_ * std::pow(mesh.dx_p(i, j), 2.0) * rho_ / mu_;
            const double dt_v = adaptivetimestepCoeficient_ * std::pow(mesh.dy_p(i, j), 2.0) * rho_ / mu_;

            max_dt = std::min(std::min(dt_u, dt_v), max_dt);
        }
    }

    return max_dt;
}

double NavierStocksSolver::average(double a, double b)
{
    return 0.5 * (a + b);
}

double NavierStocksSolver::calculateTimeStep()
{
    if (timestepApproach_ == "constant")
    {
        return timeStep_;
    }
    else
    {
        const double dt_conv = calculateTimeStep_conv();
        static const double dt_diff = calculateTimeStep_diff();
        return std::min(dt_conv, dt_diff);
    }
}

void NavierStocksSolver::solve()
{
    double convergence_rate = 1.e10;

    // currentTime_ = loadLastSimulationResults();

    int itr = 0;
    while (convergence_rate > convergence_criteria_)
    // while(currentTime_<30)
    {
        timeStep_ = calculateTimeStep();
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "Iteration: " << itr << ", dt: " << timeStep_ << ", solving for time: " << currentTime_ << std::endl;

        // Update the property distribution based on the solution
        solveExplicit();

        // Advance the current time
        currentTime_ += timeStep_;

        convergence_rate = evaluate_convergence_rates();

        store_property_in_temp_array(mesh.uold, mesh.u);
        store_property_in_temp_array(mesh.vold, mesh.v);
        store_property_in_temp_array(mesh.Pold, mesh.P);
        store_property_in_temp_array(mesh.Told, mesh.T);

        saveTheSimulationResults(currentTime_);
        itr++;
    }

    writeAveragePropertyDistributionToCSV(finalResultOutputFileName_);
    writeVAtCenterHorizontalLineToCSV(plotVFileName_);
    writeUAtCenterVerticalLineToCSV(plotUFileName_);
    evaluate_avg_Nu();
    std::cout << "solution finished" << std::endl;
}

void NavierStocksSolver::solveExplicit()
{
    applyBoundaryValues_u();
    applyBoundaryValues_v();
    applyBoundaryValues_T();

    evaluate_u_p();
    evaluate_v_p();

    evaluate_P();

    evaluate_u_new();
    evaluate_v_new();
    evaluate_T_new();
}

void NavierStocksSolver::evaluate_avg_Nu()
{
    double avg_Nu = 0.0;
    double total_area = 0.0;

    // iterate over all points including boundary cells:
    const int i = 0;
    for (int j = 0; j < numGridPoints_y_; j++)
    {
        const double theta_p = (mesh.T[i][j] - (T_hot_ + T_cold_) / 2.0) / (T_hot_ - T_cold_);
        const double theta_wall = (T_hot_ - (T_hot_ + T_cold_) / 2.0) / (T_hot_ - T_cold_);
        const double Nu_y = -(theta_p - theta_wall) / (mesh.x_p_c(i, j) - mesh.x_p_w(i, j));
        avg_Nu += Nu_y * mesh.dy_p(i, j);
    }

    std::cout << "Average value of Nusselt number (Nu) in the domain: " << avg_Nu << std::endl;
}

void NavierStocksSolver::applyBoundaryValues_u()
{
    for (int i = 0; i < numGridPoints_x_ + 1; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            if (i == 0 | i == numGridPoints_x_) // left, or right boundaries
            {
                mesh.u[i][j] = 0.0;
                mesh.up[i][j] = 0.0;
            }

            else if (j == 0) // bottom boundary
            {
                mesh.u[i][j] = linearInterpolation(0.0, 0.0, mesh.y_u_c(i, j + 1), mesh.u[i][j + 1], mesh.y_u_c(i, j));
                mesh.up[i][j] = mesh.u[i][j];
            }

            else if (j == (numGridPoints_y_ - 1)) // top boundary
            {
                mesh.u[i][j] = linearInterpolation(GeometryHeight_, 0.0, mesh.y_u_c(i, j - 1), mesh.u[i][j - 1], mesh.y_u_c(i, j));
                mesh.up[i][j] = mesh.u[i][j];
            }
        }
    }
}

void NavierStocksSolver::applyBoundaryValues_v()
{
    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_ + 1; j++)
        {
            if (j == 0 | j == numGridPoints_y_) // bottom, or top boundaries
            {
                mesh.v[i][j] = 0.0;
                mesh.vp[i][j] = mesh.v[i][j];
            }
            else if (i == 0) // left boundary
            {
                mesh.v[i][j] = linearInterpolation(0.0, 0.0, mesh.x_v_c(i + 1, j), mesh.v[i + 1][j], mesh.x_v_c(i, j));
                mesh.vp[i][j] = mesh.v[i][j];
            }

            else if (i == (numGridPoints_x_ - 1)) // right boundary
            {
                mesh.v[i][j] = linearInterpolation(GeometryWidth_, 0.0, mesh.x_v_c(i - 1, j), mesh.v[i - 1][j], mesh.x_v_c(i, j));
                mesh.vp[i][j] = mesh.v[i][j];
            }
        }
    }
}

void NavierStocksSolver::applyBoundaryValues_T()
{
    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            if (i == 0) // left boundary
            {
                mesh.T[i][j] = linearInterpolation(0.0, T_hot_, mesh.x_p_c(i + 1, j), mesh.T[i + 1][j], mesh.x_p_c(i, j));
            }

            else if (i == numGridPoints_x_ - 1) // right boundary
            {
                mesh.T[i][j] = linearInterpolation(GeometryWidth_, T_cold_, mesh.x_p_c(i - 1, j), mesh.T[i - 1][j], mesh.x_p_c(i, j));
            }

            else if (j == 0) // bottom boundary
            {
                mesh.T[i][j] = mesh.T[i][j + 1]; // maybe improvement needed
            }

            else if (j == (numGridPoints_y_ - 1)) // top boundary
            {
                mesh.T[i][j] = mesh.T[i][j - 1]; // maybe improvement needed
            }
        }
    }
}

double NavierStocksSolver::computeRu(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T)
{
    const double u_E = u[i + 1][j];
    const double u_W = u[i - 1][j];
    const double u_N = u[i][j + 1];
    const double u_S = u[i][j - 1];
    const double u_P = u[i][j];

    const double u_e = linearInterpolation(mesh.x_u_c(i + 1, j), u[i + 1][j], mesh.x_u_c(i, j), u[i][j], mesh.x_u_e(i, j)); //- average(u[i + 1][j], u[i][j]);
    const double u_w = linearInterpolation(mesh.x_u_c(i - 1, j), u[i - 1][j], mesh.x_u_c(i, j), u[i][j], mesh.x_u_w(i, j)); //- average(u[i - 1][j], u[i][j]);
    const double v_n = average(v[i - 1][j + 1], v[i][j + 1]);                                                               //- linearInterpolation(mesh.x_p_c(i-1,j), v[i - 1][j + 1], mesh.x_p_c(i,j), v[i][j + 1], mesh.x_u_c(i, j)); //-
    const double v_s = average(v[i - 1][j], v[i][j]);                                                                       //- linearInterpolation(mesh.x_p_c(i-1,j), v[i - 1][j], mesh.x_p_c(i,j), v[i][j], mesh.x_u_c(i, j)); //-

    const double phi_e = u_e;
    const double phi_w = u_w;
    const double phi_n = average(u[i][j + 1], u[i][j]);
    const double phi_s = average(u[i][j - 1], u[i][j]);

    const double DeltaS_N_S = mesh.dx_u(i, j);
    const double DeltaS_E_W = mesh.dy_u(i, j);

    const double dpN_ = mesh.y_p_c(i, j + 1) - mesh.y_p_c(i, j);
    const double dpS_ = mesh.y_p_c(i, j) - mesh.y_p_c(i, j - 1);
    const double dpW_ = mesh.dx_p(i - 1, j);
    const double dpE_ = mesh.dx_p(i, j);

    return mu_ * ((u_N - u_P) * (DeltaS_N_S / dpN_) +
                  (u_S - u_P) * (DeltaS_N_S / dpS_) +
                  (u_E - u_P) * (DeltaS_E_W / dpE_) +
                  (u_W - u_P) * (DeltaS_E_W / dpW_)) -
           rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                   (u_e * phi_e - u_w * phi_w) * DeltaS_E_W);
    (rho_ * gx_ * beta_) * (average(T[i][j], T[i - 1][j]) - T_cold_) * mesh.dv_u(i, j);
}

void NavierStocksSolver::evaluate_u_p()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_; i++)
    {
        for (int j = 1; j < (numGridPoints_y_ - 1); j++)
        {
            const double Ru = computeRu(i, j, mesh.u, mesh.v, mesh.T);
            const double Ru_old = computeRu(i, j, mesh.uold, mesh.vold, mesh.Told);

            mesh.up[i][j] = mesh.u[i][j] + timeStep_ / (rho_ * mesh.dv_u(i, j)) * (coef_R * Ru + coef_R_old * Ru_old);
        }
    }
}

double NavierStocksSolver::compute_Rv(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T)
{
    const double v_E = v[i + 1][j];
    const double v_W = v[i - 1][j];
    const double v_N = v[i][j + 1];
    const double v_S = v[i][j - 1];
    const double v_P = v[i][j];

    const double v_n = linearInterpolation(mesh.y_v_c(i, j + 1), v[i][j + 1], mesh.y_v_c(i, j), v[i][j], mesh.y_v_n(i, j)); //- average(v[i][j + 1], v[i][j]);
    const double v_s = linearInterpolation(mesh.y_v_c(i, j - 1), v[i][j - 1], mesh.y_v_c(i, j), v[i][j], mesh.y_v_s(i, j)); //- average(v[i][j - 1], v[i][j]);
    const double u_w = average(u[i][j], u[i][j - 1]);                                                                       //- linearInterpolation(mesh.y_u_c(i, j - 1), u[i][j - 1], mesh.y_u_c(i, j), u[i][j], mesh.y_v_c(i, j));
    const double u_e = average(u[i + 1][j], u[i + 1][j - 1]);                                                               //- linearInterpolation(mesh.y_u_c(i + 1, j - 1), u[i + 1][j - 1], mesh.y_u_c(i + 1, j), u[i + 1][j], mesh.y_v_c(i, j));

    const double DeltaS_N_S = mesh.dx_v(i, j);
    const double DeltaS_E_W = mesh.dy_v(i, j);

    const double phi_e = average(v[i + 1][j], v[i][j]);
    const double phi_w = average(v[i - 1][j], v[i][j]);
    const double phi_n = v_n;
    const double phi_s = v_s;

    const double dpN_ = mesh.dy_p(i, j);
    const double dpS_ = mesh.dy_p(i, j - 1);
    const double dpW_ = mesh.x_p_c(i, j) - mesh.x_p_c(i - 1, j);
    const double dpE_ = mesh.x_p_c(i + 1, j) - mesh.x_p_c(i, j);

    return mu_ * ((v_N - v_P) * (DeltaS_N_S / dpN_) +
                  (v_S - v_P) * (DeltaS_N_S / dpS_) +
                  (v_E - v_P) * (DeltaS_E_W / dpE_) +
                  (v_W - v_P) * (DeltaS_E_W / dpW_)) -
           rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                   (u_e * phi_e - u_w * phi_w) * DeltaS_E_W) +
           (rho_ * gy_ * beta_) * (average(T[i][j], T[i][j - 1]) - T_cold_) * mesh.dv_v(i, j);
}

void NavierStocksSolver::evaluate_v_p()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_; j++)
        {
            const double Rv_ = compute_Rv(i, j, mesh.u, mesh.v, mesh.T);
            const double Rv_old = compute_Rv(i, j, mesh.uold, mesh.vold, mesh.Told);

            mesh.vp[i][j] = mesh.v[i][j] + timeStep_ / (rho_ * mesh.dv_v(i, j)) * (coef_R * Rv_ + coef_R_old * Rv_old);
        }
    }
}

double NavierStocksSolver::compute_RT(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T)
{
    const double T_E = T[i + 1][j];
    const double T_W = T[i - 1][j];
    const double T_N = T[i][j + 1];
    const double T_S = T[i][j - 1];
    const double T_P = T[i][j];

    const double v_n = v[i][j + 1];
    const double v_s = v[i][j];
    const double u_w = u[i][j];
    const double u_e = u[i + 1][j];

    const double DeltaS_N_S = mesh.dx_p(i, j);
    const double DeltaS_E_W = mesh.dy_p(i, j);

    const double phi_e = average(T[i + 1][j], T[i][j]);
    const double phi_w = average(T[i - 1][j], T[i][j]);
    const double phi_n = average(T[i][j + 1], T[i][j]);
    const double phi_s = average(T[i][j - 1], T[i][j]);

    const double dpN_ = mesh.y_p_c(i, j + 1) - mesh.y_p_c(i, j);
    const double dpS_ = mesh.y_p_c(i, j) - mesh.y_p_c(i, j - 1);
    const double dpW_ = mesh.x_p_c(i, j) - mesh.x_p_c(i - 1, j);
    const double dpE_ = mesh.x_p_c(i + 1, j) - mesh.x_p_c(i, j);

    return (lambda_ / cp_) * ((T_N - T_P) * (DeltaS_N_S / dpN_) + (T_S - T_P) * (DeltaS_N_S / dpS_) + (T_E - T_P) * (DeltaS_E_W / dpE_) + (T_W - T_P) * (DeltaS_E_W / dpW_)) -
           rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                   (u_e * phi_e - u_w * phi_w) * DeltaS_E_W);
}

void NavierStocksSolver::evaluate_T_new()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            const double RT_ = compute_RT(i, j, mesh.u, mesh.v, mesh.T);
            const double RT_old = compute_RT(i, j, mesh.uold, mesh.vold, mesh.Told);

            mesh.T[i][j] = mesh.Told[i][j] + timeStep_ / (rho_ * mesh.dv_p(i, j)) * (coef_R * RT_ + coef_R_old * RT_old);
        }
    }
}

void NavierStocksSolver::evaluate_u_new()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            mesh.u[i][j] = mesh.up[i][j] - (timeStep_ / rho_) * (mesh.P[i][j] - mesh.P[i - 1][j]) / mesh.dx_u(i, j);
        }
    }
}

void NavierStocksSolver::evaluate_v_new()
{
    // iterate over internal points only:
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_; j++)
        {
            mesh.v[i][j] = mesh.vp[i][j] - (timeStep_ / rho_) * (mesh.P[i][j] - mesh.P[i][j - 1]) / mesh.dy_v(i, j);
        }
    }
}

void NavierStocksSolver::evaluate_P()
{
    // Fill the matrix A and vector b for the current time step
    auto [A, b] = fillMatrixAAndVectorB();

    // Solve the system of equations using Gauss-Seidel
    if (pressureSolver_ == "Gauss_Sidel")
    {
        gaussSeidel(A, b, solverMaxIteration_, solverTolerance_, mesh.P, solverRelaxation_);
    }
    else
    {
        conjugateGradient(A, b, solverMaxIteration_, solverTolerance_, mesh.P);
    }
}

std::pair<std::vector<vec2d>, vec2d> NavierStocksSolver::fillMatrixAAndVectorB()
{
    static const int diagonals = 5;

    static std::vector<vec2d> A(numGridPoints_x_, vec2d(numGridPoints_y_, vec(diagonals, 0.0)));
    static vec2d b(numGridPoints_x_, vec(numGridPoints_y_, 0.0));

    for (int i = 0; i < numGridPoints_x_; i++)
    {
        for (int j = 0; j < numGridPoints_y_; j++)
        {
            const double aW = get_aW_pressure_eq(i, j);
            const double aE = get_aE_pressure_eq(i, j);
            const double aN = get_aN_pressure_eq(i, j);
            const double aS = get_aS_pressure_eq(i, j);

            const double aP = aE + aW + aN + aS;

            A[i][j][0] = -1. * aE;
            A[i][j][1] = -1. * aW;
            A[i][j][2] = aP;
            A[i][j][3] = -1. * aS;
            A[i][j][4] = -1. * aN;

            const double vp_n = mesh.vp[i][j + 1];
            const double vp_s = mesh.vp[i][j];
            const double up_e = mesh.up[i + 1][j];
            const double up_w = mesh.up[i][j];

            b[i][j] = -1. * (rho_ / timeStep_) * ((vp_n - vp_s) * mesh.dx_p(i, j) + (up_e - up_w) * mesh.dy_p(i, j));
        }
    }

    return std::make_pair(A, b);
}

inline const double NavierStocksSolver::get_aW_pressure_eq(const int i, const int j)
{
    if (i == 0)
    {
        return 0;
    }
    return mesh.dy_p(i, j) / (mesh.x[i] - mesh.x[i - 1]);
}

inline const double NavierStocksSolver::get_aE_pressure_eq(const int i, const int j)
{
    if (i == (numGridPoints_x_ - 1))
    {
        return 0;
    }
    return mesh.dy_p(i, j) / (mesh.x[i + 1] - mesh.x[i]);
}

inline const double NavierStocksSolver::get_aS_pressure_eq(const int i, const int j)
{
    if (j == 0)
    {
        return 0;
    }
    return mesh.dx_p(i, j) / (mesh.y[j] - mesh.y[j - 1]);
}

inline const double NavierStocksSolver::get_aN_pressure_eq(const int i, const int j)
{
    if (j == (numGridPoints_y_ - 1))
    {
        return 0;
    }
    return mesh.dx_p(i, j) / (mesh.y[j + 1] - mesh.y[j]);
}