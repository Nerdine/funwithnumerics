#include "heat_equation_solver.h"
#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"
#include "gauss_seidel.h"

HeatEquationSolver::HeatEquationSolver(const std::string &filename)
{
    readConfigFromYAML(filename);

    temperatureDistribution_ = std::vector<double>(numGridPoints_); // initialise the size of the temperature vector
    thermalDiffusivity_ = thermalConductivity_ / (rodDensity_ * rodCp_);
    dx_ = rodLength_ / numGridPoints_;
    gamma_ = thermalDiffusivity_ * timeStep_ / (std::pow(dx_, 2));
    r_ = 1.0 / (1.0 + 2. * gamma_ * beta_);

    if (beta_ == 0)
    {
        std::cout << "Selected temporal method is explicit." << std::endl;
        if (gamma_ < 0.5)
        {
            std::cout << "The dt convergence condition is met." << std::endl;
        }
        else
        {
            std::cout << "The dt convergence condition is not met. The solution is susceptible to numerical divergence" << std::endl;
            std::cout << "Do you want to continue (yes/no)? ";

            std::string userResponse;
            std::cin >> userResponse;

            if (userResponse == "no")
            {
                std::cout << "Stopping the code." << std::endl;
                exit(EXIT_FAILURE);  // Exit the program with a failure status
            }
            else if (userResponse != "yes")
            {
                std::cout << "Invalid response. Assuming 'no'." << std::endl;
                exit(EXIT_FAILURE);
            }
        }
    }
}

// Method to read configuration parameters from a YAML file
bool HeatEquationSolver::readConfigFromYAML(const std::string &filename)
{
    try
    {
        YAML::Node config = YAML::LoadFile(filename);

        // Read configuration parameters from the YAML file
        rodLength_ = config["rodLength"].as<double>();
        rodDensity_ = config["rodDensity"].as<double>();
        rodCp_ = config["rodCp"].as<double>();
        timeStep_ = config["timeStep"].as<double>();
        numGridPoints_ = config["numGridPoints"].as<int>();
        thermalConductivity_ = config["thermalConductivity"].as<double>();
        leftBoundaryTemp_ = config["leftBoundaryTemp"].as<double>();
        rightBoundaryTemp_ = config["rightBoundaryTemp"].as<double>();
        endTime_ = config["endTime"].as<double>();
        beta_ = config["beta"].as<double>();
        initialTemperature_ = config["initialTemperature"].as<double>();
        solverMaxIteration_ = config["solverMaxIteration"].as<int>();
        solverTolerance_ = config["solverTolerance"].as<double>();
        solverRelaxation_ = config["solverRelaxation"].as<double>();
        finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();
        evolutionaryResultOutputFileName_ = config["evolutionaryResultOutputFileName"].as<std::string>();
        probLocation_ = config["probLocation"].as<double>();

        return true; // Configuration read successfully
    }
    catch (const YAML::Exception &e)
    {
        std::cerr << "Error reading configuration: " << e.what() << std::endl;
        return false; // Configuration read failed
    }
}

void HeatEquationSolver::initializeTemperatureDistribution()
{
    // Initialize the temperatureDistribution_ vector based on the initialTemperature value
    for (int i = 0; i < temperatureDistribution_.size(); i++)
    {
        temperatureDistribution_[i] = initialTemperature_;
    }
}

void HeatEquationSolver::setBoundaryConditions()
{
    // Set the boundary values
    temperatureDistribution_[0] = leftBoundaryTemp_;                   // Set left boundary value
    temperatureDistribution_[numGridPoints_ - 1] = rightBoundaryTemp_; // Set right boundary value
}

void HeatEquationSolver::writeTemperatureDistributionToCSV()
{
    std::ofstream file(finalResultOutputFileName_);
    if (file.is_open())
    {
        // Write column headers
        file << "dx,temperatureDistribution" << std::endl;

        // Write data for each point
        for (size_t i = 0; i < temperatureDistribution_.size(); i++)
        {
            file << dx_ * i << "," << temperatureDistribution_[i] << std::endl;
        }

        file.close();
        std::cout << "CSV file '" << finalResultOutputFileName_ << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

double HeatEquationSolver::getTemperatureValueAtLocation(double x)
{
    // Calculate the indices and weights for linear interpolation
    int index_left_cell = int(x * double(numGridPoints_) - 0.5);
    int index_right_cell = index_left_cell + 1;

    // Retrieve temperatures from adjacent cells
    double temp_left_cell = temperatureDistribution_[index_left_cell];
    double temp_right_cell = temperatureDistribution_[index_right_cell];

    // Calculate the slope and interpolate temperature at the given location
    double slope = (temp_right_cell - temp_left_cell) / dx_;
    double temp_at_x = temp_left_cell + slope * (x - (index_left_cell * dx_ + dx_ / 2.0));

    return temp_at_x;
}


void HeatEquationSolver::writeTemperatureAtLocationToCSV(double t)
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(evolutionaryResultOutputFileName_);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,t,temperature" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(evolutionaryResultOutputFileName_, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {
        // Write the data for the specified x and t
        file << probLocation_ << "," << t << "," << getTemperatureValueAtLocation(probLocation_) << std::endl;

        file.close();
        // std::cout << "CSV data written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void HeatEquationSolver::solve() {
    initializeTemperatureDistribution();
    
    // Determine the solving method based on the beta value
    std::function<void()> solvingMethod;
    if (beta_ == 0) {
        solvingMethod = [this]() { solveExplicit(); };
    } else {
        solvingMethod = [this]() { solveNonExplicit(); };
    }

    double currentTime = timeStep_;

    while (currentTime <= endTime_) {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "solving for the time: " << currentTime << std::endl;

        // Apply boundary conditions
        setBoundaryConditions();

        // Update the temperature distribution based on the solution
        solvingMethod();

        // Advance the current time
        currentTime += timeStep_;

        writeTemperatureAtLocationToCSV(currentTime);
        std::cout<<"temperature at x = 0.75 is: "<<getTemperatureValueAtLocation(0.75)<<std::endl;
    }

    writeTemperatureDistributionToCSV();
    std::cout << "solution finished" << std::endl;
}

const std::vector<double> &HeatEquationSolver::getCurrentTemperatureDistribution() const
{
    return temperatureDistribution_;
}

void HeatEquationSolver::solveExplicit()
{
    int size = numGridPoints_; // Use numGridPoints_ as the size of the system
    
    std::vector<double> temperatureDistribution_temp_ = temperatureDistribution_;

    for (int i = 1; i < size - 1; i++)
    {
        temperatureDistribution_[i] = r_ * (
            temperatureDistribution_temp_[i] + 
            gamma_ * 
            (
                temperatureDistribution_temp_[i+1] + 
                -2. * temperatureDistribution_temp_[i] + 
                temperatureDistribution_temp_[i-1]
            )
        ) ;
    }

}

void HeatEquationSolver::solveNonExplicit()
{
        // Fill the matrix A and vector b for the current time step
        auto [A, b] = fillMatrixAAndVectorB();

        // Solve the system of equations using Gauss-Seidel
        temperatureDistribution_ = gaussSeidel(A, b, solverMaxIteration_, solverTolerance_, temperatureDistribution_, solverRelaxation_);

}


std::pair<std::vector<std::vector<double>>, std::vector<double>> HeatEquationSolver::fillMatrixAAndVectorB()
{
    int size = numGridPoints_; // Use numGridPoints_ as the size of the system
    static const int diagonals = 3;

    std::vector<std::vector<double>> A(size, std::vector<double>(diagonals, 0.0));
    std::vector<double> b(size, 0.0);

    A[0][1] = 1;
    A[size - 1][1] = 1.;

    b[0] = temperatureDistribution_[0];
    b[size - 1] = temperatureDistribution_[size - 1];

    for (int i = 1; i < size - 1; i++)
    {
        A[i][0] = -1. * r_ * gamma_ * beta_;
        A[i][1] = 1;
        A[i][2] = -1. * r_ * gamma_ * beta_;

        b[i] = r_ * (
            temperatureDistribution_[i]  + 
            gamma_ * (1. - beta_) * (
                temperatureDistribution_[i + 1] +
                -2. * temperatureDistribution_[i] +
                temperatureDistribution_[i - 1]
                )
            );
    }

    return std::make_pair(A, b);
}
