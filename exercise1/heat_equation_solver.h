#ifndef HEAT_EQUATION_SOLVER_H
#define HEAT_EQUATION_SOLVER_H

#include <vector>
#include <fstream>

class HeatEquationSolver
{
public:
    // Constructor to initialize the solver with parameters
    HeatEquationSolver(const std::string& filename);

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string& filename);

    // Initialize the temperature distribution on the rod
    void initializeTemperatureDistribution();

    // Set the temperature on boundaries of the rod
    void setBoundaryConditions();

    double getTemperatureValueAtLocation(double x);

    // Function to write the temperature values of the entire rod at the end of solution
    void writeTemperatureDistributionToCSV();

    // Function to write the temperature value at a location in each timestep
    void writeTemperatureAtLocationToCSV(double t);

    // Function to fill and return the coefficient matrix A and vector b for Ax = b
    std::pair<std::vector<std::vector<double>>, std::vector<double>> fillMatrixAAndVectorB();

    void solveExplicit();

    void solveNonExplicit();

    // Solve the 1D heat equation over a specified time period
    void solve();

    // Get the temperature distribution at the current time if needed from outside of the class
    const std::vector<double> &getCurrentTemperatureDistribution() const;

private:
    // private member functions:

    double rodLength_;
    double timeStep_;
    int numGridPoints_;
    double rodDensity_;
    double rodCp_;
    double thermalConductivity_;
    double thermalDiffusivity_;
    double initialTemperature_;
    double leftBoundaryTemp_;
    double rightBoundaryTemp_;
    double endTime_;
    double dx_;
    double beta_;  // Additional parameter for temporal approach
    int solverMaxIteration_;
    double solverTolerance_;
    double solverRelaxation_;
    double gamma_; // internal parameter used to simplify the coefficients
    double r_;     // internal parameter used to simplify the coefficients
    double probLocation_;    
    std::vector<double> temperatureDistribution_;
    std::string finalResultOutputFileName_;
    std::string evolutionaryResultOutputFileName_;

};

#endif // HEAT_EQUATION_SOLVER_H
