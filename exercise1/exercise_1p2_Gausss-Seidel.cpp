#include <iostream>
#include <vector>
#include <cmath>
#include "gauss_seidel.h"

int main() {
    // Define the coefficient matrix A and the right-hand side vector b
    std::vector<std::vector<double> > A = {{4, -1, -1}, {-2, 6, 1}, {-1, 1, 7}};
    std::vector<double> b = {3, 9, -6};

    // Maximum number of iterations and tolerance for convergence
    int maxIterations = 1000;
    double tolerance = 1e-6;

    // Solve the system of equations using Gauss-Seidel
    std::vector<double> solution = gaussSeidel(A, b, maxIterations, tolerance);

    // Print the solution
    std::cout << "Solution: ";
    for (double xi : solution) {
        std::cout << xi << " ";
    }
    std::cout << std::endl;


    // Define the coefficient matrix A and the right-hand side vector b
    std::vector<std::vector<double> > A_non_diagonal_dominant = {{-1, 1, 7}, {4, -1, -1}, {-2, 6, 1}};
    std::vector<double> b_non_diagonal_dominant = {9, -6, 3};

    // Solve the system of equations using Gauss-Seidel
    std::vector<double> solution_non_diagonal_dominant = gaussSeidel(A_non_diagonal_dominant, b_non_diagonal_dominant, maxIterations, tolerance);

    // Print the solution
    std::cout << "Solution for non diagonally dominant: ";
    for (double xi : solution_non_diagonal_dominant) {
        std::cout << xi << " ";
    }
    std::cout << std::endl;

    return 0;
}