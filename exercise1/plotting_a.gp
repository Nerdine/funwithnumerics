fn_output = 'results/plots/temperature_a.pdf'
fn_data = 'evolutionResult.csv'

# Set plot title and labels
set title 'Temperature at x=0.5m for t=[0;2000]s'
set xlabel 't [s]'
set ylabel 'Temperature [K]'

# Enable grid
set grid
# Set the starting points of the x and y axes to zero
set xrange [0:2100]
set yrange [300:]

# Specify the output file format and filename
set term pdf # Change 'pdf' to the desired vector graphic format (e.g., 'svg', 'eps', etc.)
set output fn_output # Specify the desired output filename

set datafile separator ','
# Plot data from the CSV file
plot fn_data every ::1 using 2:3 with lines title 'Temperature'

# Close the output file
set output
