#!/bin/bash

# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <cpp_file>"
    exit 1
fi

cpp_file="$1"
output_file="${cpp_file%.cpp}"

# Check if the input C++ file exists
if [ ! -f "$cpp_file" ]; then
    echo "Error: The input file '$cpp_file' does not exist."
    exit 1
fi

# Compile the C++ program using g++ with the C++11 standard
g++ -std=c++17 -o "$output_file" "$cpp_file" gauss_seidel.cpp heat_equation_solver.cpp -lyaml-cpp

# Check if compilation was successful
if [ $? -eq 0 ]; then
    echo "Compilation successful. Running the program..."
    "./$output_file"
else
    echo "Compilation failed."
fi
