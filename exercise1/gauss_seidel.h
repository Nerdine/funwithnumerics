#ifndef GAUSS_SEIDEL_H
#define GAUSS_SEIDEL_H

#include <vector>

std::vector<double> gaussSeidel(
    const std::vector<std::vector<double>>& A, 
    const std::vector<double>& b, 
    int maxIterations, 
    double tolerance, 
    std::vector<double>& x,
    double omega = 1.0
    );


#endif // GAUSS_SEIDEL_H
