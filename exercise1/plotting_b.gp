fn_output_pdf = 'results/plots/temperature_b.pdf'
fn_data = 'finalResults.csv'

# Set plot title and labels
set title 'Temperature profile at t=800s'
set xlabel 'x [m]'
set ylabel 'Temperature [K]'

# Enable grid
set grid
# Set the starting points of the x and y axes to zero
set xrange [0:]
set yrange [293:]

set datafile separator ','

# Change the terminal to PDF for the same plot
set term pdf
set output fn_output_pdf

# Plot the same data from the CSV file and save as PDF
plot fn_data every ::1 using 1:2 with lines title 'Temperature'

# Close the PDF output
set output

