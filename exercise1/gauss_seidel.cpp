#include "gauss_seidel.h"
#include <iostream>
#include <cmath>

std::vector<double> gaussSeidel(
    const std::vector<std::vector<double>>& A, 
    const std::vector<double>& b, 
    int maxIterations, 
    double tolerance, 
    std::vector<double>& x,
    double omega
    )
{
    int n = A.size();

    for (int iter = 0; iter < maxIterations; iter++)
    {
        std::vector<double> newX(n, 0.0);

        newX[0] = (1.0 - omega) * x[0] + (omega / A[0][1]) * (b[0] - (A[0][2]* x[1]));
        newX[n-1] = (1.0 - omega) * x[n-1] + (omega / A[n-1][1]) * (b[n-1] - (A[0][0]* x[n-2]));

        for (int i = 1; i < n-1; i++)
        {
            double aE = A[i][0];
            double aP = A[i][1];
            double aW = A[i][2];

            double sigma = aE * x[i-1] + aW * x[i+1];

            newX[i] = (1.0 - omega) * x[i] + (omega / aP) * (b[i] - sigma);
        }

        double maxError = 0.0;
        for (int i = 0; i < n; i++)
        {
            maxError = std::max(maxError, std::abs(newX[i] - x[i]));
            x[i] = newX[i];
        }

        if (maxError < tolerance)
        {
            std::cout << "Gauss-Seidel solver returning after " << iter+1 << " iterations." << std::endl;
            return x;
        }
    }

    std::cout << "Gauss-Seidel did not converge within " << maxIterations << " iterations." << std::endl;
    return x;
}
