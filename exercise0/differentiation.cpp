#include <iostream>
#include <cmath>

using namespace std;

// Define the function f(x) = x^2 * cos(x)
double f(double x) {
    return x * x * cos(x);
}

// Numerical derivative using central differences
double centralDifference(double x, double h) {
    double f_plus_h = f(x + h);
    double f_minus_h = f(x - h);
    return (f_plus_h - f_minus_h) / (2.0 * h);
}

// Numerical derivative using backward differences
double backwardDifference(double x, double h) {
    double f_minus_h = f(x - h);
    double f_x = f(x);
    return (f_x - f_minus_h) / h;
}

// Numerical derivative using forward differences
double forwardDifference(double x, double h) {
    double f_plus_h = f(x + h);
    double f_x = f(x);
    return (f_plus_h - f_x) / h;
}

double analyticalDerivative(double x) {
    return x*x*(-sin(x)) + 2*x*cos(x);
}

void differentiate(double x, double h) {

    double central_derivative = centralDifference(x, h);
    double backward_derivative = backwardDifference(x, h);
    double forward_derivative = forwardDifference(x, h);
    double analytical_derivative = analyticalDerivative(x);

    cout << "h value:"  << h << endl;

    cout << "Analytical first derivative of f(x) = x^2 * cos(x) at x = " << x << " using forward differencing: " << analytical_derivative << endl;
    cout << "Numerical derivative of f(x) = x^2 * cos(x) at x = " << x << " using central differencing: " << central_derivative << endl;
    cout << "Analytical - numerical derivative using central differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_derivative-central_derivative << endl;

    cout << "Numerical derivative of f(x) = x^2 * cos(x) at x = " << x << " using backward differencing: " << backward_derivative << endl;
    cout << "Analytical - numerical derivative using backward differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_derivative-backward_derivative << endl;

    cout << "Numerical derivative of f(x) = x^2 * cos(x) at x = " << x << " using forward differencing: " << forward_derivative << endl;
    cout << "Analytical - numerical derivative using forward differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_derivative-forward_derivative << endl;
}

int main() {
    

    differentiate(2.0, 1e-1);
    differentiate(2.0, 1e-2);
    differentiate(2.0, 1e-3);
    differentiate(2.0, 1e-4);
    differentiate(2.0, 1e-5);
    
    differentiate(2.0, 1e-6);
    
    return 0;
}
