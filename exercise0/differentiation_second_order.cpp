#include <iostream>
#include <cmath>

using namespace std;

// Define the function f(x) = x^2 * cos(x)
double f(double x) {
    return x * x * cos(x);
}

// Numerical second derivative using central differences
double centralSecondDerivative(double x, double h) {
    double f_x_minus_h = f(x - h);
    double f_x = f(x);
    double f_x_plus_h = f(x + h);
    return (f_x_minus_h - 2.0 * f_x + f_x_plus_h) / (h * h);
}

// Numerical second derivative using backward differences
double backwardSecondDerivative(double x, double h) {
    double f_x_minus_2h = f(x - 2*h);
    double f_x = f(x);
    double f_x_minus_h = f(x - h);
    return (f_x - 2.0 * f_x_minus_h + f_x_minus_2h) / (h * h);
}

// Numerical second derivative using forward differences
double forwardSecondDerivative(double x, double h) {
    double f_x_plus_2h = f(x + 2*h);
    double f_x = f(x);
    double f_x_plus_h = f(x + h);
    return (f_x_plus_2h - 2.0 * f_x_plus_h + f_x) / (h * h);
}

double analyticalSecondOrderDerivative(double x, double h) {
    return  2*cos(x) - 2*x*sin(x) - 2*x*sin(x) - x*x*cos(x);
}



void differentiate_second_order(double x, double h) {
    double central_second_derivative = centralSecondDerivative(x, h);
    double backward_second_derivative = backwardSecondDerivative(x, h);
    double forward_second_derivative = forwardSecondDerivative(x, h);
    double analytical_second_derivative = analyticalSecondOrderDerivative(x, h);
    
    cout << h << endl;

    cout << "Analytical second derivative of f(x) = x^2 * cos(x) at x = " << x << " using forward differencing: " << analytical_second_derivative << endl;
    cout << "Numerical second derivative of f(x) = x^2 * cos(x) at x = " << x << " using central differencing: " << central_second_derivative << endl;
    cout << "Analytical - numerical second derivative using central differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_second_derivative - central_second_derivative << endl;

    cout << "Numerical second derivative of f(x) = x^2 * cos(x) at x = " << x << " using backward differencing: " << backward_second_derivative << endl;
    cout << "Analytical - numerical second derivative using backward differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_second_derivative - backward_second_derivative << endl;

    cout << "Numerical second derivative of f(x) = x^2 * cos(x) at x = " << x << " using forward differencing: " << forward_second_derivative << endl;
    cout << "Analytical - numerical second derivative using forward differencing of f(x) = x^2 * cos(x) at x = " << x << " : " << analytical_second_derivative - forward_second_derivative << endl;

}


int main() {
    differentiate_second_order(2.0, 1e-1);
    differentiate_second_order(2.0, 1e-2);
    differentiate_second_order(2.0, 1e-3);
    differentiate_second_order(2.0, 1e-4);
    differentiate_second_order(2.0, 1e-5);
    differentiate_second_order(2.0, 1e-6);
    return 0;
}
