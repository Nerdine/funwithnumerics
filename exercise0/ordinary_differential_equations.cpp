
#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>

using namespace std;

// Define ODE parameters
const double m = 3.0;  // Mass
const double g = 9.81; // Gravitational acceleration
const double b = 1.0;  // Damping coefficient
const double initial_velocity = 2.0;  // Initial velocity
const double epsilon = 1e-4;         // Tolerance for stopping
const int max_iterations = 1000;      // Maximum number of iterations
const double delta_t = 0.1;         // Define time step value
std::string approach = "Implicit";


// Function to write t and v vectors to a CSV file
void writeVectorsToCSV(const std::vector<double>& time, const std::vector<double>& velocity, const std::vector<bool>& converged, const std::string& filename) {
    // Open the file for writing
    std::ofstream outputFile(filename);

    // Check if the file is open
    if (!outputFile.is_open()) {
        std::cerr << "Error opening the file." << std::endl;
        return; // Exit the function if there's an error
    }

    // Write headers (optional)
    outputFile << "Time,Velocity,Converged" << std::endl;

    // Write data from the vectors to the file
    for (size_t i = 0; i < time.size(); ++i) {
        outputFile << time[i] << "," << velocity[i] << "," << converged[i] << std::endl;
    }

    // Close the file
    outputFile.close();

    std::cout << "Data has been written to " << filename << "." << std::endl;
}

void solveODE_Explicit(double delta_t, std::vector<double>& time, std::vector<double>& velocity, std::vector<bool>& converged) {
    double velocity_current = initial_velocity;
    double velocity_previous = 0.0;
    double current_time = 0.0;
    int iteration_count = 0;

    while (iteration_count < max_iterations && std::fabs(velocity_current - velocity_previous) >= epsilon) {
        velocity_previous = velocity_current;
        // Update time
        current_time += delta_t;
        time.push_back(current_time);

        // Calculate velocity change
        double dv = delta_t * (m * g - b * velocity_current);
        velocity_current += dv;

        // Store current velocity
        velocity.push_back(velocity_current);
        converged.push_back(std::fabs(velocity_current - velocity_previous) < epsilon);
        // Update previous velocity and iteration count
        iteration_count++;
    }

    converged.push_back(std::fabs(velocity_current - velocity_previous) < epsilon);

}

void solveODE_Implicit(double delta_t, std::vector<double>& time, std::vector<double>& velocity, std::vector<bool>& converged) {
    double velocity_current = initial_velocity;
    double velocity_previous = 0.0;
    double current_time = 0.0;
    int iteration_count = 0;

    while (iteration_count < max_iterations && std::fabs(velocity_current - velocity_previous) >= epsilon) {
        velocity_previous = velocity_current;
        // Update time
        current_time += delta_t;
        time.push_back(current_time);

        // Calculate velocity change
        velocity_current = (delta_t * g+velocity_previous)/(1+b*delta_t/m);

        // Store current velocity
        velocity.push_back(velocity_current);
        converged.push_back(std::fabs(velocity_current - velocity_previous) < epsilon);

        // Update previous velocity and iteration count
        iteration_count = iteration_count +1;

    }
    converged.push_back(std::fabs(velocity_current - velocity_previous) < epsilon);
}
/**
 * Analytical solution mit last value of velocity vergleichen
*/

int main() {

    std::vector<double> time(1,0);
    std::vector<double> velocity(1,2);
    std::vector<bool> converged(1,false);

    // Construct the filename using delta_t
    std::ostringstream filenameStream;
    filenameStream << "results/csv_data/" << approach << "_delta_t_" << delta_t << ".csv";
    std::string filename = filenameStream.str();
    
    // Solve the ODE for the current time step
    if (approach == "Implicit") {
        solveODE_Implicit(delta_t, time, velocity, converged);
    } else {
        solveODE_Explicit(delta_t, time, velocity, converged);
    }

    // Write the vectors to the CSV file
    writeVectorsToCSV(time, velocity, converged, filename);

    return 0;
}