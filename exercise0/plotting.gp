fn_output = 'results/plots/Implicit_delta_t_12.png'
fn_data = 'results/csv_data/Implicit_delta_t_12.csv'


# Set plot title and labels
set title 'Velocity vs. Iteration'
set xlabel 'Iteration'
set ylabel 'Velocity'

# Enable grid
set grid
# Set the starting points of the x and y axes to zero
set xrange [0:]
set yrange [0:]

# Specify the output file format and filename
set term png # Change 'png' to the desired format (e.g., 'pdf', 'svg', etc.)
set output fn_output # Specify the desired output filename


set datafile separator ','
# Plot data from the CSV file
plot fn_data every ::1 using 1:2 with lines title 'Velocity'

# Close the output file
set output

