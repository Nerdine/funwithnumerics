#include <iostream>
#include <cmath>

using namespace std;

// Define the Gaussian distribution function
double gaussian(double x, double mean, double variance) {
    double exponent = -(pow(x - mean, 2) / (2.0 * variance));
    return (1.0 / (sqrt(2.0 * M_PI) * variance)) * exp(exponent);
}

// Numerical integration using Riemann sum (rectangles)
double integrate(double a, double b, double mean, double variance, int num_intervals, bool useTrapezoidal) {
    double dx = (b - a) / num_intervals;
    double integral = 0.0;

    for (int i = 0; i < num_intervals; i++) {
        double x0 = a + i * dx;
        double x1 = a + (i + 1) * dx;
        double mid = (x0 + x1) / 2.0; // Midpoint for trapezoidal rule

        double f0 = gaussian(x0, mean, variance);
        double f1 = gaussian(x1, mean, variance);

        if (useTrapezoidal) {
            integral += 0.5 * dx * (f0 + f1); // Trapezoidal rule
        } else {
            integral += dx * f0; // Riemann sum
        }
    }

    return integral;
}

int main() {
    double a = -1.0;     // Lower limit of integration
    double b = 1.0;      // Upper limit of integration
    double mean = 0.0;   // Mean of the Gaussian distribution
    double variance = 1.0;  // Variance of the Gaussian distribution
    int num_intervals = 10000; // Number of intervals for Riemann sum

    for (int numIntervals : {10, 100, 1000, 10000, 100000}) {
        double resultRiemann = integrate(a, b, mean, variance, numIntervals, false);
        double resultTrapezoidal = integrate(a, b, mean, variance, numIntervals, true);

        cout << "Number of intervals: " << numIntervals << endl;
        cout << "Riemann sum result: " << resultRiemann << endl;
        cout << "Trapezoidal rule result: " << resultTrapezoidal << endl;
        cout << "-----------------------------------------" << endl;
    }

    return 0;
}
