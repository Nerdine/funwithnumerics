#include <iostream>
#include <cmath>

using namespace std;

double newtonRaphson(double x0, double tolerance, int maxIterations) {
    double x = x0;
    int iteration = 0;

    while (iteration < maxIterations) {
        double f_x = cos(x) - x;
        double f_prime_x = -sin(x) - 1;

        if (fabs(f_prime_x) < 1e-6) {
            cerr << "Error: Division by zero or near-zero derivative. Newton-Raphson method cannot continue." << endl;
            return NAN; // Not-a-Number to represent an error
        }

        double delta_x = f_x / f_prime_x;
        x -= delta_x;

        if (fabs(delta_x) < tolerance) {
            cout << "Converged to a root after " << iteration << " iterations." << endl;
            return x;
        }

        iteration++;
    }

    cerr << "Newton-Raphson method did not converge within the specified number of iterations." << endl;
    return NAN; // Not-a-Number to represent an error
}

int main() {
    double initialGuess = 0.0;
    double tolerance = 1e-6;
    int maxIterations = 100;

    double root = newtonRaphson(initialGuess, tolerance, maxIterations);

    if (!isnan(root)) {
        cout << "Approximate root: " << root << endl;
    } else {
        cerr << "Failed to find a root." << endl;
    }

    return 0;
}
