#!/bin/bash

# Check for the correct number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <cpp_file>"
    exit 1
fi

cpp_file="$1"
output_file="${cpp_file%.cpp}"
log_file="${output_file}_log.txt"

# Check if the input C++ file exists
if [ ! -f "$cpp_file" ]; then
    echo "Error: The input file '$cpp_file' does not exist."
    exit 1
fi

# Compile the C++ program using g++ with the C++17 standard
g++ -std=c++17 -o "$output_file" "$cpp_file" src/conjugate_gradient.cpp src/gauss_seidel.cpp src/Matrix2x2.cpp src/vector2D.cpp src/mesh.cpp navier_stoks_solver.cpp -lyaml-cpp -lboost_serialization -lspdlog -lfmt 2>&1 | tee "$log_file"


# Check if compilation was successful
if [ ${PIPESTATUS[0]} -eq 0 ]; then
    echo "Compilation successful. Running the program..."
    "./$output_file"
else
    echo "Compilation failed."
fi


# to detach a screen:
# - start the terminal and type "screen"
# - to detach: ctrl+A followed by ctrl+D
# - to retach: screen -r
# - to list all the screens: screen -ls
# - to attach to a specific screen: screen -r session_name
# - to terminat the screen session: exit
# - to terminate the screen from outside of screen: screen -X -S session_name quit
# - to create a new screen: "screen -S session_name"
# - to remove all the screens: "killall screen"



# to assing a task to a specific CPU core:
# - taskset -c 0 ./your_code