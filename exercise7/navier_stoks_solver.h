#ifndef navier_stoks_solver_H
#define navier_stoks_solver_H

#include <vector>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

#include "src/mesh.h"
#include "src/Matrix2x2.h"

typedef std::vector<double> vec;
typedef std::vector<std::vector<double>> vec2d;

class NavierStocksSolver
{
public:
    // Constructor to initialize the solver with parameters
    NavierStocksSolver(const std::string &filename);

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string &filename);

    // Functions to write and read the property values of the entire geometry
    void writePropertyToCSV(std::string &filename, const vec2d &property);
    void writeAveragePropertyDistributionToCSV(std::string &filename);
    // double extractCurrentTime(const std::string &filename);

    // Function to write the property value at a location in each timestep
    void writeUAtCenterVerticalLineToCSV(std::string &filename);
    void writeVAtCenterHorizontalLineToCSV(std::string &filename);
    void writeGridToCSV(std::string &filename);

    void applyBoundaryValues_u();
    void applyBoundaryValues_v();
    void applyBoundaryValues_T();
    double computeRu(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T);
    double compute_Rv(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T);
    double compute_RT(int i, int j, const vec2d &u, const vec2d &v, const vec2d &T);
    void evaluate_u_p();
    void evaluate_v_p();
    void evaluate_P();
    void evaluate_u_new();
    void evaluate_v_new();
    void evaluate_T_new();
    void evaluate_avg_Nu();

    // Function to fill and return the coefficient matrix A and vector b for Ax = b
    std::pair<std::vector<vec2d>, vec2d> fillMatrixAAndVectorB();
    const double get_aW_pressure_eq(const int i, const int j);
    const double get_aE_pressure_eq(const int i, const int j);
    const double get_aS_pressure_eq(const int i, const int j);
    const double get_aN_pressure_eq(const int i, const int j);

    void solveExplicit();

    // Solve the convection-diffusion equation over a specified time period
    void solve();

    // Get the property distribution at the current time if needed from outside of the class
    const vec2d &getCurrentPropertyDistribution() const;

    const double get_average_of_changes(vec2d &old_field, vec2d &new_field);
    const double get_maximum_of_changes(const vec2d &old_field, const vec2d &new_field);

    void store_property_in_temp_array(vec2d &old_field, const vec2d &new_field);
    void update_property_averages(vec2d &old_field, const vec2d &new_field);

    void saveTheSimulationResults();
    double evaluate_convergence_rates();
    double average(double a, double b);
    double linearInterpolation(double x0, double y0, double x1, double y1, double x);
    double calculateTimeStep_conv();
    double calculateTimeStep_diff();
    double calculateTimeStep();
    std::string getCurrentDateTimeString();

    void serializeMesh(const std::string &filename);
    void deserializeMesh(const std::string& filename);
    void initialiseTheLogger();
    void controlLoggerBehaviour(const int &itr);

    double energy_budget_pressure(const int i, const int j);
    double energy_budget_convective(const int i, const int j);
    double energy_budget_body_force(const int i, const int j);
    double energy_budget_viscous(const int i, const int j);
    double energy_budget_convective_diffusive(const int i, const int j);
    double return_energy_at(const int i, const int j);
    vector2D helperFunctionTemp(const int &i, const int &j);
    void energy_analysis();

    Matrix2x2 return_velocity_gradient_at(const int i, const int j);

    inline const double get_un_in_p_cell(const int i, const int j);
    inline const double get_us_in_p_cell(const int i, const int j);
    inline const double get_uw_in_p_cell(const int i, const int j);
    inline const double get_ue_in_p_cell(const int i, const int j);        

    inline const double get_ve_in_p_cell(const int i, const int j);
    inline const double get_vw_in_p_cell(const int i, const int j);
    inline const double get_vn_in_p_cell(const int i, const int j);
    inline const double get_vs_in_p_cell(const int i, const int j);


private:
    // private member functions:
    double timeStep_;
    double adaptivetimestepCoeficient_;
    std::string timestepApproach_;
    double finishTime_;

    int numGridPoints_x_;
    int numGridPoints_y_;
    double refinedGridStretchingFactor_x_;
    double refinedGridStretchingFactor_y_;

    int solverMaxIteration_;
    double solverTolerance_;
    double solverRelaxation_;
    std::string pressureSolver_;

    double initialValue_;

    double convergence_criteria_;

    double GeometryWidth_;
    double GeometryHeight_;

    double rho_;
    double mu_;
    double lambda_;
    double gy_;
    double gx_;
    double Re_;
    double Ra_;
    double Pr_;
    double beta_;
    double cp_;
    double T_inf_;
    double T_cold_;
    double T_hot_;
    double coef_R;
    double coef_R_old;

    std::string scheme_;
    std::string timeIntegrationScheme_;
    std::string finalResultOutputFileName_;
    std::string plotUFileName_;
    std::string plotVFileName_;
    std::string gridOutputFileName_;
    std::string energyAnalysisFileName_;
    std::string meshType_;
    std::string serializationOutputFileName_;
    std::string transiendResultsFileName_;
    std::string loggerLocation_;
    
    bool loadLastRun_;
    double outputInterval_;
    double startAveragingTime_;

    Mesh mesh;

    // Set up logging to file
    std::shared_ptr<spdlog::logger> file_logger;
    bool logger_enabled;
    int loggingInterval_;
};

#endif // navier_stoks_solver_H
