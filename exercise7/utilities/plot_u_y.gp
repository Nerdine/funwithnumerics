# my_data = "results/grid.csv"  # Default value
# output_file = "results/grid.png"  # Default value

# Check if parameters are provided from the command line
if (ARGC > 1) { my_data = ARGV[1] }
if (ARGC > 2) { output_file = ARGV[2] }

# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output output_file

# Set the plot title and labels for x:property
set title "y vs U"
set xlabel "U"
set ylabel "y"

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"
set yrange [0:1]

# Plot x against property with different line types and no markers

plot my_data using 3:2 with lines linecolor "blue" notitle

# plot "results/u_y_Re_3200_mesh_32.csv" using 3:2 with linespoints title "h=L/32" linetype 2 pi 5, \
#      "results/u_y_Re_3200_mesh_64.csv" using 3:2 with linespoints title "h=L/64" linetype 3 pi 10, \
#      "results/u_y_Re_3200_mesh_128.csv" using 3:2 with linespoints title "h=L/128" linetype 4 pi 20, \
#      "results/u_y_Re_3200_mesh_320.csv" using 3:2 with linespoints title "h=L/320" linetype 5 pi 60, \
#      "results/u_y_Re_3200_mesh_480.csv" using 3:2 with linespoints title "h=L/480" linetype 6 pi 80, \
#      "Guia-u-y.csv" using 5:1 with points pointtype 7 pointsize 1 linecolor "red" title "Ref"

# Save the plot to a file and close Gnuplot
set output
quit
