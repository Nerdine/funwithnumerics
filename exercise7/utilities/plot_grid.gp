# Define variables that can be set from the command line
num_isolines = 5
min_value = 0
max_value = 1
# my_data = "results/grid.csv"  # Default value
# output_file = "results/grid.png"  # Default value

# Check if parameters are provided from the command line
if (ARGC > 1) { my_data = ARGV[1] }
if (ARGC > 2) { output_file = ARGV[2] }

# Set the output format
set terminal pngcairo enhanced font "Helvetica,12"
set output output_file

# Set the plot title and labels
# set title "2D Grid"
set xlabel "X"
set ylabel "Y"

# Specify the color range based on your data
# set cbrange [min_value:max_value]

# Customize the color map
set pm3d map
set palette defined (0 "blue", 0.4 "green", 0.6 "yellow", 1 "red")

set xrange [0:1]
set yrange [0:1]

# Set the size ratio to ensure x-axis length is exactly twice the y-axis length
set size ratio -1

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"

# Create the 2D grid plot from the CSV data file
set view map
splot my_data u 1:2:1 w l notitle

# Save the plot to a file and close Gnuplot
set output
quit
