#include "navier_stoks_solver.h"
#include "src/conjugate_gradient.h"
#include "src/gauss_seidel.h"
#include "src/vector2D.h"
#include "yaml-cpp/yaml.h"
#include <algorithm> // for std::max_element
#include <chrono>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <sstream>

// #include <boost/filesystem.hpp>
// namespace fs = boost::filesystem;

NavierStocksSolver::NavierStocksSolver(const std::string &filename) {
  readConfigFromYAML(filename);

  mesh = Mesh(numGridPoints_x_, numGridPoints_y_, GeometryWidth_,
              GeometryHeight_, refinedGridStretchingFactor_x_,
              refinedGridStretchingFactor_y_, meshType_);


  std::ostringstream oss;
  oss << std::scientific << std::setprecision(2) << Ra_;
  std::string Ra_scientific = oss.str();

  std::string case_name = "_Ra_" + Ra_scientific + "_mesh_" +
                          std::to_string(numGridPoints_x_);
  finalResultOutputFileName_ = finalResultOutputFileName_ + case_name + ".csv";
  plotUFileName_ = plotUFileName_ + case_name + ".csv";
  plotVFileName_ = plotVFileName_ + case_name + ".csv";
  gridOutputFileName_ = gridOutputFileName_ + case_name + ".csv";
  energyAnalysisFileName_ = energyAnalysisFileName_ + case_name + ".csv";
  serializationOutputFileName_ =
      serializationOutputFileName_ + case_name + ".ser";
  transiendResultsFileName_ = transiendResultsFileName_ + case_name + "_";

  if (loadLastRun_) {
    deserializeMesh(serializationOutputFileName_);
  }

  //- initialising the logger:
  initialiseTheLogger();

  cp_ = Pr_ * lambda_ / mu_;
  gy_ = Ra_ * mu_ * lambda_ /
        (cp_ * beta_ * pow(rho_, 2.0) * (T_hot_ - T_cold_) *
         pow(GeometryHeight_, 3.));
  gx_ = 0.0;

  writeGridToCSV(gridOutputFileName_);

  if (timeIntegrationScheme_ == "Euler") {
    coef_R = 1.0;
    coef_R_old = 0.0;
  } else {
    coef_R = 1.5;
    coef_R_old = -0.5;
  }
}

void NavierStocksSolver::initialiseTheLogger() {
  std::string dateTimeString = getCurrentDateTimeString();
  std::ostringstream oss_logger;
  oss_logger << loggerLocation_ << "run_" << std::fixed << dateTimeString
             << ".log";

  std::string filename_logger = oss_logger.str();

  file_logger = spdlog::basic_logger_mt("file_logger", filename_logger);

  file_logger->info("-------------------------------------");
  file_logger->info("Parameters of the sumulation: ");
  file_logger->info("continuation of previous run: {}", loadLastRun_);
  file_logger->info("GeometryWidth: {}", GeometryWidth_);
  file_logger->info("GeometryHeight: {}", GeometryHeight_);
  file_logger->info("numGridPoints_x: {}", numGridPoints_x_);
  file_logger->info("numGridPoints_y: {}", numGridPoints_y_);
  file_logger->info("Rayleigh_number: {}", Ra_);
  file_logger->info("Prandtl_number: {}", Pr_);
  file_logger->info("scheme: {}", scheme_);
  file_logger->info("timeIntegrationScheme: {}", timeIntegrationScheme_);
  file_logger->info("timestepApproach: {}", timestepApproach_);
  file_logger->info("CFL number: {}", adaptivetimestepCoeficient_);
  file_logger->info("finishTime: {}", finishTime_);
  file_logger->info("startAveragingTime: {}", startAveragingTime_);
  file_logger->info("meshType: {}", meshType_);
  file_logger->info("pressureSolver: {}", pressureSolver_);
  file_logger->info("solverMaxIteration: {}", solverMaxIteration_);
  file_logger->info("solverTolerance: {}", solverTolerance_);
  file_logger->info("solverRelaxation: {}", solverRelaxation_);
  file_logger->info("outputInterval: {}", outputInterval_);
  file_logger->info("-------------------------------------");
}

void NavierStocksSolver::controlLoggerBehaviour(const int &itr) {
  if (itr % loggingInterval_ == 0) {
    logger_enabled = true;
  } else {
    logger_enabled = false;
  }

  if (logger_enabled) {
    file_logger->info("-------------------------------------");
    file_logger->info("Iteration: {}, dt: {}, solving for time: {}", itr,
                      timeStep_, mesh.meshTime);
  }
}

std::string NavierStocksSolver::getCurrentDateTimeString() {
  // Get the current time
  auto now = std::chrono::system_clock::now();
  std::time_t now_c = std::chrono::system_clock::to_time_t(now);

  // Convert the current time to a tm struct for local time
  std::tm *local_time = std::localtime(&now_c);

  // Format the date and time into a string
  std::ostringstream oss;
  oss << std::setfill('0') << std::setw(4) << local_time->tm_year + 1900 << "_"
      << std::setw(2) << local_time->tm_mon + 1 << "_" << std::setw(2)
      << local_time->tm_mday << "_" << std::setw(2) << local_time->tm_hour
      << "_" << std::setw(2) << local_time->tm_min << "_" << std::setw(2)
      << local_time->tm_sec;
  return oss.str();
}

// Method to serialize mesh object to save simulation
void NavierStocksSolver::serializeMesh(const std::string &filename) {
  std::ofstream ofs(filename);
  boost::archive::text_oarchive oa(ofs);
  oa << mesh;
}

// Method to deserialize mesh object to load simulation
void NavierStocksSolver::deserializeMesh(const std::string &filename) {
  std::ifstream ifs(filename);
  boost::archive::text_iarchive ia(ifs);
  ia >> mesh;
}

// Method to read configuration parameters from a YAML file
bool NavierStocksSolver::readConfigFromYAML(const std::string &filename) {
  try {
    YAML::Node config = YAML::LoadFile(filename);

    // Read configuration parameters from the YAML file
    GeometryWidth_ = config["GeometryWidth"].as<double>();
    GeometryHeight_ = config["GeometryHeight"].as<double>();
    initialValue_ = config["initialValue"].as<double>();

    rho_ = config["rho"].as<double>();
    scheme_ = config["scheme"].as<std::string>();

    timeStep_ = config["timeStep"].as<double>();
    adaptivetimestepCoeficient_ =
        config["adaptivetimestepCoeficient"].as<double>();
    timestepApproach_ = config["timestepApproach"].as<std::string>();
    finishTime_ = config["finishTime"].as<double>();

    numGridPoints_x_ = config["numGridPoints_x"].as<int>();
    numGridPoints_y_ = config["numGridPoints_y"].as<int>();
    refinedGridStretchingFactor_x_ =
        config["refinedGridStretchingFactor_x"].as<double>();
    refinedGridStretchingFactor_y_ =
        config["refinedGridStretchingFactor_y"].as<double>();

    convergence_criteria_ = config["convergence_criteria"].as<double>();

    Ra_ = config["Rayleigh_number"].as<double>();
    Pr_ = config["Prandtl_number"].as<double>();
    beta_ = config["beta"].as<double>();
    T_inf_ = config["T_inf"].as<double>();
    T_cold_ = config["T_cold"].as<double>();
    T_hot_ = config["T_hot"].as<double>();
    mu_ = config["mu"].as<double>();
    lambda_ = config["lambda"].as<double>();
    startAveragingTime_ = config["startAveragingTime"].as<double>();
    loadLastRun_ = config["loadLastRun"].as<bool>();

    solverMaxIteration_ = config["solverMaxIteration"].as<int>();
    solverTolerance_ = config["solverTolerance"].as<double>();
    solverRelaxation_ = config["solverRelaxation"].as<double>();
    pressureSolver_ = config["pressureSolver"].as<std::string>();

    finalResultOutputFileName_ =
        config["finalResultOutputFileName"].as<std::string>();
    plotUFileName_ = config["plotUFileName"].as<std::string>();
    plotVFileName_ = config["plotVFileName"].as<std::string>();

    gridOutputFileName_ = config["gridOutputFileName"].as<std::string>();
    energyAnalysisFileName_ =
        config["energyAnalysisFileName"].as<std::string>();
    timeIntegrationScheme_ = config["timeIntegrationScheme"].as<std::string>();
    serializationOutputFileName_ =
        config["serializationOutputFileName"].as<std::string>();
    transiendResultsFileName_ =
        config["transiendResultsFileName"].as<std::string>();
    loggerLocation_ = config["loggerLocation"].as<std::string>();
    loggingInterval_ = config["loggingInterval"].as<int>();

    meshType_ = config["meshType"].as<std::string>();

    outputInterval_ = config["outputInterval"].as<double>();

    return true; // Configuration read successfully
  } catch (const YAML::Exception &e) {
    file_logger->error("Error reading configuration: {}", e.what());
    return false; // Configuration read failed
  }
}

void NavierStocksSolver::writeGridToCSV(std::string &filename) {
  std::ofstream file(filename);
  if (file.is_open()) {
    // Write column headers
    file << "variables=X, Y" << std::endl;

    // Write data for each point
    for (int i = 0; i < numGridPoints_x_; ++i) {
      for (int j = 0; j < numGridPoints_y_; ++j) {
        file << mesh.x_p_c(i, j) << "," << mesh.y_p_c(i, j) << std::endl;
      }
      file << std::endl;
    }

    file.close();
    file_logger->info("CSV file '{}' written successfully.", filename);
  } else {
    file_logger->error("Error: Unable to open file {} for writing.", filename);
  }
};

void NavierStocksSolver::writeAveragePropertyDistributionToCSV(
    std::string &filename) {
  std::ofstream file(filename);
  if (file.is_open()) {
    // Write column headers
    file << "x, y, u, v, U_tot, p, T" << std::endl;

    // Write data for each point
    for (int i = 0; i < numGridPoints_x_; i++) {
      for (int j = 0; j < numGridPoints_y_; j++) {
        const double u_cell = average(mesh.u[i][j], mesh.u[i + 1][j]);
        const double v_cell = average(mesh.v[i][j], mesh.v[i][j + 1]);
        const double U_tot = sqrt(u_cell * u_cell + v_cell * v_cell);
        const double P_cell = mesh.P[i][j];
        const double T_cell = mesh.T[i][j];

        file << mesh.x_p_c(i, j) << ", " << mesh.y_p_c(i, j) << ", " << u_cell
             << ", " << v_cell << ", " << U_tot << ", " << P_cell << ", "
             << T_cell << std::endl;
      }
    }

    file.close();
    file_logger->info("CSV file '{}' written successfully.", filename);
  } else {
    file_logger->error("Error: Unable to open file {} for writing.", filename);
  }
}

void NavierStocksSolver::writePropertyToCSV(std::string &filename,
                                            const vec2d &property) {
  std::ofstream file(filename);
  if (file.is_open()) {
    // Write column headers
    file << "i, j, x, y, " << filename << std::endl;

    // Write data for each point
    for (int i = 0; i < property.size(); i++) {
      for (int j = 0; j < property[i].size(); j++) {
        file << i << ", " << j << ", " << mesh.x_p_c(i, j) << ", "
             << mesh.y_p_c(i, j) << ", " << property[i][j] << std::endl;
      }
    }

    file.close();
    file_logger->info("CSV file '{}' written successfully.", filename);
  } else {
    file_logger->error("Error: Unable to open file {} for writing.", filename);
  }
}

void NavierStocksSolver::saveTheSimulationResults() {
  static double lastOutputTime = 0.0;

  // Check if it's time to write output
  if (mesh.meshTime - lastOutputTime >= outputInterval_) {
    // Format the current time to include in the filename
    std::ostringstream oss_u, oss_v, oss_P, oss_T;
    oss_u << transiendResultsFileName_ << "u_" << std::fixed
          << std::setprecision(3) << mesh.meshTime << ".csv";
    oss_v << transiendResultsFileName_ << "v_" << std::fixed
          << std::setprecision(3) << mesh.meshTime << ".csv";
    oss_P << transiendResultsFileName_ << "P_" << std::fixed
          << std::setprecision(3) << mesh.meshTime << ".csv";
    oss_T << transiendResultsFileName_ << "T_" << std::fixed
          << std::setprecision(3) << mesh.meshTime << ".csv";

    std::string filename_u = oss_u.str();
    std::string filename_v = oss_v.str();
    std::string filename_P = oss_P.str();
    std::string filename_T = oss_T.str();

    writePropertyToCSV(filename_u, mesh.u);
    writePropertyToCSV(filename_v, mesh.v);
    writePropertyToCSV(filename_P, mesh.P);
    writePropertyToCSV(filename_T, mesh.T);

    file_logger->info("u simulation results stored in {}.", filename_u);
    file_logger->info("v simulation results stored in {}.", filename_v);
    file_logger->info("P simulation results stored in {}.", filename_P);
    file_logger->info("T simulation results stored in {}.", filename_T);

    serializeMesh(serializationOutputFileName_);

    if (mesh.meshTime > startAveragingTime_) {
      std::ostringstream oss_u, oss_v, oss_P, oss_T;

      oss_u << transiendResultsFileName_ << "Avg_u_" << std::fixed
            << std::setprecision(3) << mesh.meshTime << ".csv";
      oss_v << transiendResultsFileName_ << "Avg_v_" << std::fixed
            << std::setprecision(3) << mesh.meshTime << ".csv";
      oss_P << transiendResultsFileName_ << "Avg_P_" << std::fixed
            << std::setprecision(3) << mesh.meshTime << ".csv";
      oss_T << transiendResultsFileName_ << "Avg_T_" << std::fixed
            << std::setprecision(3) << mesh.meshTime << ".csv";

      filename_u = oss_u.str();
      filename_v = oss_v.str();
      filename_P = oss_P.str();
      filename_T = oss_T.str();

      writePropertyToCSV(filename_u, mesh.avgU);
      writePropertyToCSV(filename_v, mesh.avgV);
      writePropertyToCSV(filename_P, mesh.avgP);
      writePropertyToCSV(filename_T, mesh.avgT);

      file_logger->info("Avg_u simulation results stored in {}.", filename_u);
      file_logger->info("Avg_v simulation results stored in {}.", filename_v);
      file_logger->info("Avg_P simulation results stored in {}.", filename_P);
      file_logger->info("Avg_T simulation results stored in {}.", filename_T);
    }

    // Update the last output time
    lastOutputTime = mesh.meshTime;
  }
}

// double NavierStocksSolver::extractCurrentTime(const std::string &filename)
// {
//     // Find the position of the underscore before the timestamp
//     size_t underscorePos = filename.find('_');
//     if (underscorePos == std::string::npos)
//     {
//         // Handle the case where the filename format is not as expected
//         return 0.0; // or any suitable default value
//     }

//     // Extract the substring after the underscore (timestamp)
//     std::string timestampStr = filename.substr(underscorePos + 1);

//     // Convert the timestamp string to a double
//     try
//     {
//         return std::stod(timestampStr);
//     }
//     catch (const std::exception &e)
//     {
//         // Handle the case where the conversion fails
//         std::cerr << "Error extracting current time from filename: " <<
//         e.what() << std::endl; return 0.0; // or any suitable default value
//     }
// }

void NavierStocksSolver::writeUAtCenterVerticalLineToCSV(
    std::string &filename) {
  static bool headersWritten =
      false; // Keep track if headers are already written
  std::ofstream file;

  if (!headersWritten) {
    file.open(filename);
    if (file.is_open()) {
      // Write column headers (only once)
      file << "x,y,Avg_U" << std::endl;
      headersWritten = true;
    } else {
      file_logger->error("Error: Unable to open file {} for writing.",
                         filename);
      return;
    }
  } else {
    file.open(filename, std::ios::app); // Open the file in append mode
  }

  if (file.is_open()) {
    // Find the grid cell index corresponding to x=0.5
    const double targetX = 0.5;
    int i_centerline = -1;

    for (int i = 0; i < numGridPoints_x_; ++i) {
      if (mesh.x_u_c(i, 0) >= targetX) {
        i_centerline = i;
        break;
      }
    }

    if (i_centerline == -1) {
      file_logger->error("Error: Unable to find grid cell for x=0.5.");
      file.close();
      return;
    }

    // Initialize variables to track maximum value and its location
    double max_u_cell =
        std::numeric_limits<double>::lowest(); // Initialize with lowest
                                               // possible value
    double max_u_x = 0.0;
    double max_u_y = 0.0;

    // Calculate the average value of u_cell along the vertical centerline
    double average_u_cell = 0.0;

    for (int j = 0; j < numGridPoints_y_; j++) {
      const double u_cell = linearInterpolation(
          mesh.x[i_centerline], mesh.avgU[i_centerline][j],
          mesh.x[i_centerline - 1], mesh.avgU[i_centerline - 1][j], 0.5);
      average_u_cell += u_cell;

      // Print the values
      file << mesh.x[i_centerline] << "," << mesh.y_u_c(i_centerline, j) << ","
           << u_cell << std::endl;

      // Update maximum value and its location if necessary
      if (u_cell > max_u_cell) {
        max_u_cell = u_cell;
        max_u_x = mesh.x[i_centerline];
        max_u_y = mesh.y_u_c(i_centerline, j);
      }
    }

    // Calculate and print the average value
    average_u_cell /= numGridPoints_y_;
    file_logger->info(
        "Average value of u_cell along the vertical centerline at x=0.5: {}",
        average_u_cell);

    // Print maximum value and its location
    const double max_u_cell_non_dim =
        max_u_cell / (lambda_ / (rho_ * cp_)); //- non dimentionalised
    file_logger->info("Maximum value of u_cell: {} at (x, y) = ({}, {})",
                      max_u_cell_non_dim, max_u_x, max_u_y);

    file.close();
    file_logger->info("CSV data written successfully.");
  } else {
    file_logger->error("Error: Unable to open file for writing.");
  }
}

void NavierStocksSolver::writeVAtCenterHorizontalLineToCSV(
    std::string &filename) {
  static bool headersWritten =
      false; // Keep track if headers are already written
  std::ofstream file;

  if (!headersWritten) {
    file.open(filename);
    if (file.is_open()) {
      // Write column headers (only once)
      file << "x,y,V" << std::endl;
      headersWritten = true;
    } else {
      file_logger->error("Error: Unable to open file for writing.");
      return;
    }
  } else {
    file.open(filename, std::ios::app); // Open the file in append mode
  }

  if (file.is_open()) {
    // Find the grid cell index corresponding to y=0.5
    const double targetY = 0.5;
    int j_centerline = -1;

    for (int j = 0; j < numGridPoints_y_; ++j) {
      if (mesh.y_v_c(0, j) >= targetY) {
        j_centerline = j;
        break;
      }
    }

    if (j_centerline == -1) {
      file_logger->error("Error: Unable to find grid cell for y=0.5.");
      file.close();
      return;
    }

    // Initialize variables to track maximum value and its location
    double max_v_cell =
        std::numeric_limits<double>::lowest(); // Initialize with lowest
                                               // possible value
    double max_v_x = 0.0;
    double max_v_y = 0.0;

    // Calculate the average value of v_cell along the horizontal centerline
    double average_v_cell = 0.0;

    for (int i = 0; i < numGridPoints_x_; i++) {
      const double v_cell = linearInterpolation(
          mesh.y[j_centerline], mesh.avgV[i][j_centerline],
          mesh.y[j_centerline - 1], mesh.avgV[i][j_centerline - 1], 0.5);
      average_v_cell += v_cell;

      // Print the values
      file << mesh.x[i] << "," << mesh.y_v_c(i, j_centerline) << "," << v_cell
           << std::endl;

      // Update maximum value and its location if necessary
      if (v_cell > max_v_cell) {
        max_v_cell = v_cell;
        max_v_x = mesh.x[i];
        max_v_y = mesh.y_v_c(i, j_centerline);
      }
    }

    // Calculate and print the average value
    average_v_cell /= numGridPoints_x_;
    file_logger->info(
        "Average value of v_cell along the horizontal centerline at y=0.5: {}",
        average_v_cell);

    // Print maximum value and its location
    const double max_v_cell_non_dim =
        max_v_cell / (lambda_ / (rho_ * cp_)); //- non dimentionalised
    file_logger->info("Maximum value of v_cell: {} at (x, y) = ({},{})",
                      max_v_cell_non_dim, max_v_x, max_v_y);

    file.close();
    file_logger->info("CSV data written successfully.");
  } else {
    file_logger->error("Error: Unable to open file for writing.");
  }
}

const double NavierStocksSolver::get_average_of_changes(vec2d &old_field,
                                                        vec2d &new_field) {
  // Determine the size of propertyDistribution_
  int x_size = old_field.size();
  int y_size = old_field[0].size();

  double sumDifferences = 0.0;

  // Copy values from propertyDistribution_ to propertyDistribution_new
  for (int i = 0; i < x_size; ++i) {
    for (int j = 0; j < y_size; ++j) {
      double difference = std::abs(old_field[i][j] - new_field[i][j]);
      sumDifferences += difference;
    }
  }
  // Calculate the average
  double averageDifference = sumDifferences / (x_size * y_size);

  return averageDifference;
}

const double
NavierStocksSolver::get_maximum_of_changes(const vec2d &old_field,
                                           const vec2d &new_field) {

  // Determine the size of propertyDistribution_
  int x_size = old_field.size();
  int y_size = old_field[0].size();

  // Calculate the change in the scalar field
  double maxChange = 0.0;

  for (int i = 0; i < x_size; ++i) {
    for (int j = 0; j < y_size; ++j) {
      double delta = std::abs(old_field[i][j] - new_field[i][j]);
      maxChange = std::max(maxChange, delta);
    }
  }

  return maxChange;
}

void NavierStocksSolver::store_property_in_temp_array(vec2d &old_field,
                                                      const vec2d &new_field) {
  // Determine the size of propertyDistribution_
  int x_size = old_field.size();
  int y_size = old_field[0].size();

  // Copy values from new_field to old_field
  for (int i = 0; i < x_size; ++i) {
    for (int j = 0; j < y_size; ++j) {
      old_field[i][j] = new_field[i][j];
    }
  }
}

void NavierStocksSolver::update_property_averages(vec2d &avg_field,
                                                  const vec2d &field) {
  if (mesh.meshTime >= startAveragingTime_) {
    // Determine the size of propertyDistribution_
    int x_size = field.size();
    int y_size = field[0].size();

    mesh.meshAveragedTime += timeStep_;

    // Copy values from new_field to old_field
    for (int i = 0; i < x_size; ++i) {
      for (int j = 0; j < y_size; ++j) {
        avg_field[i][j] += (field[i][j] * timeStep_) / mesh.meshAveragedTime;
      }
    }
    if (logger_enabled) {
      file_logger->info("averaging done for duration of {} seconds.",
                        mesh.meshAveragedTime);
    }
  }
}

double NavierStocksSolver::linearInterpolation(double x0, double y0, double x1,
                                               double y1, double x) {
  // Ensure x0 is not equal to x1 to avoid division by zero
  if (x0 == x1) {
    return y0;
  }

  // Calculate the slope (m) and y-intercept (b) of the line between (x0, y0)
  // and (x1, y1)
  double m = (y1 - y0) / (x1 - x0);
  double b = y0 - m * x0;

  // Use the equation of the line to interpolate the value at x
  return m * x + b;
}

double NavierStocksSolver::evaluate_convergence_rates() {
  const double convergence_rate_u = get_maximum_of_changes(mesh.uold, mesh.u);
  const double convergence_rate_v = get_maximum_of_changes(mesh.vold, mesh.v);
  const double convergence_rate_P = get_maximum_of_changes(mesh.Pold, mesh.P);
  const double convergence_rate_T = get_maximum_of_changes(mesh.Told, mesh.T);

  if (logger_enabled) {

    file_logger->info("Convergence Rate u is: {}", convergence_rate_u);
    file_logger->info("Convergence Rate v is: {}", convergence_rate_v);
    file_logger->info("Convergence Rate p is: {}", convergence_rate_P);
    file_logger->info("Convergence Rate T is: {}", convergence_rate_T);
  }

  // return std::max(convergence_rate_u, std::max(convergence_rate_v,
  // convergence_rate_P)); return std::max(convergence_rate_u,
  // convergence_rate_v);
  return std::max(convergence_rate_u,
                  std::max(convergence_rate_v, convergence_rate_T));
}

double NavierStocksSolver::calculateTimeStep_conv() {
  double max_dt = std::numeric_limits<double>::infinity();

  for (int i = 0; i < numGridPoints_x_; i++) {
    for (int j = 0; j < numGridPoints_y_; j++) {
      const double u_cell = average(mesh.u[i][j], mesh.u[i + 1][j]);
      const double dt_u =
          adaptivetimestepCoeficient_ * mesh.dx_p(i, j) / std::fabs(u_cell);

      const double v_cell = average(mesh.v[i][j], mesh.v[i][j + 1]);
      const double dt_v =
          adaptivetimestepCoeficient_ * mesh.dy_p(i, j) / std::fabs(v_cell);

      max_dt = std::min(std::min(dt_u, dt_v), max_dt);
    }
  }
  return max_dt;
}

double NavierStocksSolver::calculateTimeStep_diff() {
  double max_dt = std::numeric_limits<double>::infinity();

  for (int i = 0; i < numGridPoints_x_; i++) {
    for (int j = 0; j < numGridPoints_y_; j++) {
      const double dt_u = adaptivetimestepCoeficient_ *
                          std::pow(mesh.dx_p(i, j), 2.0) * rho_ / mu_;
      const double dt_v = adaptivetimestepCoeficient_ *
                          std::pow(mesh.dy_p(i, j), 2.0) * rho_ / mu_;

      max_dt = std::min(std::min(dt_u, dt_v), max_dt);
    }
  }

  return max_dt;
}

double NavierStocksSolver::average(double a, double b) { return 0.5 * (a + b); }

double NavierStocksSolver::calculateTimeStep() {
  if (timestepApproach_ == "constant") {
    return timeStep_;
  } else {
    const double dt_conv = calculateTimeStep_conv();
    static const double dt_diff = calculateTimeStep_diff();
    return std::min(dt_conv, dt_diff);
  }
}

void NavierStocksSolver::solve() {
  double convergence_rate = 1.e10;

  int itr = 0;
  // while (convergence_rate > convergence_criteria_)
  while (mesh.meshTime < finishTime_) {
    controlLoggerBehaviour(itr);
    timeStep_ = calculateTimeStep();

    // Update the property distribution based on the solution
    solveExplicit();

    // Advance the current time
    mesh.meshTime += timeStep_;

    convergence_rate = evaluate_convergence_rates();

    store_property_in_temp_array(mesh.uold, mesh.u);
    store_property_in_temp_array(mesh.vold, mesh.v);
    store_property_in_temp_array(mesh.Pold, mesh.P);
    store_property_in_temp_array(mesh.Told, mesh.T);

    update_property_averages(mesh.avgU, mesh.u);
    update_property_averages(mesh.avgV, mesh.v);
    update_property_averages(mesh.avgP, mesh.P);
    update_property_averages(mesh.avgT, mesh.T);

    saveTheSimulationResults();
    energy_analysis();
    file_logger->flush();

    itr++;
  }

  writeAveragePropertyDistributionToCSV(finalResultOutputFileName_);
  writeVAtCenterHorizontalLineToCSV(plotVFileName_);
  writeUAtCenterVerticalLineToCSV(plotUFileName_);
  evaluate_avg_Nu();

  file_logger->info("solution finished");
}

void NavierStocksSolver::solveExplicit() {
  applyBoundaryValues_u();
  applyBoundaryValues_v();
  applyBoundaryValues_T();

  evaluate_u_p();
  evaluate_v_p();

  evaluate_P();

  evaluate_u_new();
  evaluate_v_new();
  evaluate_T_new();
}

void NavierStocksSolver::evaluate_avg_Nu() {
  double avg_Nu = 0.0;
  double total_area = 0.0;

  // iterate over all points including boundary cells:
  const int i = 0;
  for (int j = 0; j < numGridPoints_y_; j++) {
    const double theta_p =
        (mesh.T[i][j] - (T_hot_ + T_cold_) / 2.0) / (T_hot_ - T_cold_);
    const double theta_wall =
        (T_hot_ - (T_hot_ + T_cold_) / 2.0) / (T_hot_ - T_cold_);
    const double Nu_y =
        -(theta_p - theta_wall) / (mesh.x_p_c(i, j) - mesh.x_p_w(i, j));
    avg_Nu += Nu_y * mesh.dy_p(i, j);
  }

  file_logger->info("Average value of Nusselt number (Nu) in the domain: {}",
                    avg_Nu);
}

void NavierStocksSolver::applyBoundaryValues_u() {
  for (int i = 0; i < numGridPoints_x_ + 1; i++) {
    for (int j = 0; j < numGridPoints_y_; j++) {
      if (i == 0 | i == numGridPoints_x_) // left, or right boundaries
      {
        mesh.u[i][j] = 0.0;
        mesh.up[i][j] = 0.0;
      }

      else if (j == 0) // bottom boundary
      {
        mesh.u[i][j] = linearInterpolation(0.0, 0.0, mesh.y_u_c(i, j + 1),
                                           mesh.u[i][j + 1], mesh.y_u_c(i, j));
        mesh.up[i][j] = mesh.u[i][j];
      }

      else if (j == (numGridPoints_y_ - 1)) // top boundary
      {
        mesh.u[i][j] =
            linearInterpolation(GeometryHeight_, 0.0, mesh.y_u_c(i, j - 1),
                                mesh.u[i][j - 1], mesh.y_u_c(i, j));
        mesh.up[i][j] = mesh.u[i][j];
      }
    }
  }
}

void NavierStocksSolver::applyBoundaryValues_v() {
  for (int i = 0; i < numGridPoints_x_; i++) {
    for (int j = 0; j < numGridPoints_y_ + 1; j++) {
      if (j == 0 | j == numGridPoints_y_) // bottom, or top boundaries
      {
        mesh.v[i][j] = 0.0;
        mesh.vp[i][j] = mesh.v[i][j];
      } else if (i == 0) // left boundary
      {
        mesh.v[i][j] = linearInterpolation(0.0, 0.0, mesh.x_v_c(i + 1, j),
                                           mesh.v[i + 1][j], mesh.x_v_c(i, j));
        mesh.vp[i][j] = mesh.v[i][j];
      }

      else if (i == (numGridPoints_x_ - 1)) // right boundary
      {
        mesh.v[i][j] =
            linearInterpolation(GeometryWidth_, 0.0, mesh.x_v_c(i - 1, j),
                                mesh.v[i - 1][j], mesh.x_v_c(i, j));
        mesh.vp[i][j] = mesh.v[i][j];
      }
    }
  }
}

void NavierStocksSolver::applyBoundaryValues_T() {
  for (int i = 0; i < numGridPoints_x_; i++) {
    for (int j = 0; j < numGridPoints_y_; j++) {
      if (i == 0) // left boundary
      {
        mesh.T[i][j] = linearInterpolation(0.0, T_hot_, mesh.x_p_c(i + 1, j),
                                           mesh.T[i + 1][j], mesh.x_p_c(i, j));
      }

      else if (i == numGridPoints_x_ - 1) // right boundary
      {
        mesh.T[i][j] =
            linearInterpolation(GeometryWidth_, T_cold_, mesh.x_p_c(i - 1, j),
                                mesh.T[i - 1][j], mesh.x_p_c(i, j));
      }

      else if (j == 0) // bottom boundary
      {
        mesh.T[i][j] = mesh.T[i][j + 1]; // maybe improvement needed
      }

      else if (j == (numGridPoints_y_ - 1)) // top boundary
      {
        mesh.T[i][j] = mesh.T[i][j - 1]; // maybe improvement needed
      }
    }
  }
}

double NavierStocksSolver::computeRu(int i, int j, const vec2d &u,
                                     const vec2d &v, const vec2d &T) {
  const double u_E = u[i + 1][j];
  const double u_W = u[i - 1][j];
  const double u_N = u[i][j + 1];
  const double u_S = u[i][j - 1];
  const double u_P = u[i][j];

  const double u_e = linearInterpolation(
      mesh.x_u_c(i + 1, j), u[i + 1][j], mesh.x_u_c(i, j), u[i][j],
      mesh.x_u_e(i, j)); //- average(u[i + 1][j], u[i][j]);
  const double u_w = linearInterpolation(
      mesh.x_u_c(i - 1, j), u[i - 1][j], mesh.x_u_c(i, j), u[i][j],
      mesh.x_u_w(i, j)); //- average(u[i - 1][j], u[i][j]);
  const double v_n = average(
      v[i - 1][j + 1],
      v[i][j + 1]); //- linearInterpolation(mesh.x_p_c(i-1,j), v[i - 1][j + 1],
                    // mesh.x_p_c(i,j), v[i][j + 1], mesh.x_u_c(i, j)); //-
  const double v_s =
      average(v[i - 1][j],
              v[i][j]); //- linearInterpolation(mesh.x_p_c(i-1,j), v[i - 1][j],
                        // mesh.x_p_c(i,j), v[i][j], mesh.x_u_c(i, j)); //-

  const double phi_e = u_e;
  const double phi_w = u_w;
  const double phi_n = average(u[i][j + 1], u[i][j]);
  const double phi_s = average(u[i][j - 1], u[i][j]);

  const double DeltaS_N_S = mesh.dx_u(i, j);
  const double DeltaS_E_W = mesh.dy_u(i, j);

  const double dpN_ = mesh.y_p_c(i, j + 1) - mesh.y_p_c(i, j);
  const double dpS_ = mesh.y_p_c(i, j) - mesh.y_p_c(i, j - 1);
  const double dpW_ = mesh.dx_p(i - 1, j);
  const double dpE_ = mesh.dx_p(i, j);

  return mu_ * ((u_N - u_P) * (DeltaS_N_S / dpN_) +
                (u_S - u_P) * (DeltaS_N_S / dpS_) +
                (u_E - u_P) * (DeltaS_E_W / dpE_) +
                (u_W - u_P) * (DeltaS_E_W / dpW_)) -
         rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                 (u_e * phi_e - u_w * phi_w) * DeltaS_E_W) +
         (rho_ * gx_ * beta_) * (average(T[i][j], T[i - 1][j]) - T_cold_) *
             mesh.dv_u(i, j);
}

void NavierStocksSolver::evaluate_u_p() {
  // iterate over internal points only:
  for (int i = 1; i < numGridPoints_x_; i++) {
    for (int j = 1; j < (numGridPoints_y_ - 1); j++) {
      const double Ru = computeRu(i, j, mesh.u, mesh.v, mesh.T);
      const double Ru_old = computeRu(i, j, mesh.uold, mesh.vold, mesh.Told);

      mesh.up[i][j] = mesh.u[i][j] + timeStep_ / (rho_ * mesh.dv_u(i, j)) *
                                         (coef_R * Ru + coef_R_old * Ru_old);
    }
  }
}

double NavierStocksSolver::compute_Rv(int i, int j, const vec2d &u,
                                      const vec2d &v, const vec2d &T) {
  const double v_E = v[i + 1][j];
  const double v_W = v[i - 1][j];
  const double v_N = v[i][j + 1];
  const double v_S = v[i][j - 1];
  const double v_P = v[i][j];

  const double v_n = linearInterpolation(
      mesh.y_v_c(i, j + 1), v[i][j + 1], mesh.y_v_c(i, j), v[i][j],
      mesh.y_v_n(i, j)); //- average(v[i][j + 1], v[i][j]);
  const double v_s = linearInterpolation(
      mesh.y_v_c(i, j - 1), v[i][j - 1], mesh.y_v_c(i, j), v[i][j],
      mesh.y_v_s(i, j)); //- average(v[i][j - 1], v[i][j]);
  const double u_w = average(
      u[i][j],
      u[i][j - 1]); //- linearInterpolation(mesh.y_u_c(i, j - 1), u[i][j - 1],
                    // mesh.y_u_c(i, j), u[i][j], mesh.y_v_c(i, j));
  const double u_e = average(
      u[i + 1][j],
      u[i + 1]
       [j - 1]); //- linearInterpolation(mesh.y_u_c(i + 1, j - 1), u[i + 1][j -
                 // 1], mesh.y_u_c(i + 1, j), u[i + 1][j], mesh.y_v_c(i, j));

  const double DeltaS_N_S = mesh.dx_v(i, j);
  const double DeltaS_E_W = mesh.dy_v(i, j);

  const double phi_e = average(v[i + 1][j], v[i][j]);
  const double phi_w = average(v[i - 1][j], v[i][j]);
  const double phi_n = v_n;
  const double phi_s = v_s;

  const double dpN_ = mesh.dy_p(i, j);
  const double dpS_ = mesh.dy_p(i, j - 1);
  const double dpW_ = mesh.x_p_c(i, j) - mesh.x_p_c(i - 1, j);
  const double dpE_ = mesh.x_p_c(i + 1, j) - mesh.x_p_c(i, j);

  return mu_ * ((v_N - v_P) * (DeltaS_N_S / dpN_) +
                (v_S - v_P) * (DeltaS_N_S / dpS_) +
                (v_E - v_P) * (DeltaS_E_W / dpE_) +
                (v_W - v_P) * (DeltaS_E_W / dpW_)) -
         rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                 (u_e * phi_e - u_w * phi_w) * DeltaS_E_W) +
         (rho_ * gy_ * beta_) * (average(T[i][j], T[i][j - 1]) - T_cold_) *
             mesh.dv_v(i, j);
}

void NavierStocksSolver::evaluate_v_p() {
  // iterate over internal points only:
  for (int i = 1; i < numGridPoints_x_ - 1; i++) {
    for (int j = 1; j < numGridPoints_y_; j++) {
      const double Rv_ = compute_Rv(i, j, mesh.u, mesh.v, mesh.T);
      const double Rv_old = compute_Rv(i, j, mesh.uold, mesh.vold, mesh.Told);

      mesh.vp[i][j] = mesh.v[i][j] + timeStep_ / (rho_ * mesh.dv_v(i, j)) *
                                         (coef_R * Rv_ + coef_R_old * Rv_old);
    }
  }
}

double NavierStocksSolver::compute_RT(int i, int j, const vec2d &u,
                                      const vec2d &v, const vec2d &T) {
  const double T_E = T[i + 1][j];
  const double T_W = T[i - 1][j];
  const double T_N = T[i][j + 1];
  const double T_S = T[i][j - 1];
  const double T_P = T[i][j];

  const double v_n = v[i][j + 1];
  const double v_s = v[i][j];
  const double u_w = u[i][j];
  const double u_e = u[i + 1][j];

  const double DeltaS_N_S = mesh.dx_p(i, j);
  const double DeltaS_E_W = mesh.dy_p(i, j);

  const double phi_e = average(T[i + 1][j], T[i][j]);
  const double phi_w = average(T[i - 1][j], T[i][j]);
  const double phi_n = average(T[i][j + 1], T[i][j]);
  const double phi_s = average(T[i][j - 1], T[i][j]);

  const double dpN_ = mesh.y_p_c(i, j + 1) - mesh.y_p_c(i, j);
  const double dpS_ = mesh.y_p_c(i, j) - mesh.y_p_c(i, j - 1);
  const double dpW_ = mesh.x_p_c(i, j) - mesh.x_p_c(i - 1, j);
  const double dpE_ = mesh.x_p_c(i + 1, j) - mesh.x_p_c(i, j);

  return (lambda_ / cp_) * ((T_N - T_P) * (DeltaS_N_S / dpN_) +
                            (T_S - T_P) * (DeltaS_N_S / dpS_) +
                            (T_E - T_P) * (DeltaS_E_W / dpE_) +
                            (T_W - T_P) * (DeltaS_E_W / dpW_)) -
         rho_ * ((v_n * phi_n - v_s * phi_s) * DeltaS_N_S +
                 (u_e * phi_e - u_w * phi_w) * DeltaS_E_W);
}

void NavierStocksSolver::evaluate_T_new() {
  // iterate over internal points only:
  for (int i = 1; i < numGridPoints_x_ - 1; i++) {
    for (int j = 1; j < numGridPoints_y_ - 1; j++) {
      const double RT_ = compute_RT(i, j, mesh.u, mesh.v, mesh.T);
      const double RT_old = compute_RT(i, j, mesh.uold, mesh.vold, mesh.Told);

      mesh.T[i][j] = mesh.Told[i][j] + timeStep_ / (rho_ * mesh.dv_p(i, j)) *
                                           (coef_R * RT_ + coef_R_old * RT_old);
    }
  }
}

void NavierStocksSolver::evaluate_u_new() {
  // iterate over internal points only:
  for (int i = 1; i < numGridPoints_x_; i++) {
    for (int j = 1; j < numGridPoints_y_ - 1; j++) {
      mesh.u[i][j] = mesh.up[i][j] - (timeStep_ / rho_) *
                                         (mesh.P[i][j] - mesh.P[i - 1][j]) /
                                         mesh.dx_u(i, j);
    }
  }
}

void NavierStocksSolver::evaluate_v_new() {
  // iterate over internal points only:
  for (int i = 1; i < numGridPoints_x_ - 1; i++) {
    for (int j = 1; j < numGridPoints_y_; j++) {
      mesh.v[i][j] = mesh.vp[i][j] - (timeStep_ / rho_) *
                                         (mesh.P[i][j] - mesh.P[i][j - 1]) /
                                         mesh.dy_v(i, j);
    }
  }
}

void NavierStocksSolver::evaluate_P() {
  // Fill the matrix A and vector b for the current time step
  auto [A, b] = fillMatrixAAndVectorB();

  // Solve the system of equations using Gauss-Seidel
  if (pressureSolver_ == "Gauss_Sidel") {
    gaussSeidel(A, b, solverMaxIteration_, solverTolerance_, mesh.P,
                solverRelaxation_);
  } else {
    conjugateGradient(A, b, solverMaxIteration_, solverTolerance_, mesh.P,
                      file_logger, logger_enabled);
  }
}

std::pair<std::vector<vec2d>, vec2d>
NavierStocksSolver::fillMatrixAAndVectorB() {
  static const int diagonals = 5;

  static std::vector<vec2d> A(numGridPoints_x_,
                              vec2d(numGridPoints_y_, vec(diagonals, 0.0)));
  static vec2d b(numGridPoints_x_, vec(numGridPoints_y_, 0.0));

  for (int i = 0; i < numGridPoints_x_; i++) {
    for (int j = 0; j < numGridPoints_y_; j++) {
      const double aW = get_aW_pressure_eq(i, j);
      const double aE = get_aE_pressure_eq(i, j);
      const double aN = get_aN_pressure_eq(i, j);
      const double aS = get_aS_pressure_eq(i, j);

      const double aP = aE + aW + aN + aS;

      A[i][j][0] = -1. * aE;
      A[i][j][1] = -1. * aW;
      A[i][j][2] = aP;
      A[i][j][3] = -1. * aS;
      A[i][j][4] = -1. * aN;

      const double vp_n = mesh.vp[i][j + 1];
      const double vp_s = mesh.vp[i][j];
      const double up_e = mesh.up[i + 1][j];
      const double up_w = mesh.up[i][j];

      b[i][j] =
          -1. * (rho_ / timeStep_) *
          ((vp_n - vp_s) * mesh.dx_p(i, j) + (up_e - up_w) * mesh.dy_p(i, j));
    }
  }

  return std::make_pair(A, b);
}

inline const double NavierStocksSolver::get_aW_pressure_eq(const int i,
                                                           const int j) {
  if (i == 0) {
    return 0;
  }
  return mesh.dy_p(i, j) / (mesh.x[i] - mesh.x[i - 1]);
}

inline const double NavierStocksSolver::get_aE_pressure_eq(const int i,
                                                           const int j) {
  if (i == (numGridPoints_x_ - 1)) {
    return 0;
  }
  return mesh.dy_p(i, j) / (mesh.x[i + 1] - mesh.x[i]);
}

inline const double NavierStocksSolver::get_aS_pressure_eq(const int i,
                                                           const int j) {
  if (j == 0) {
    return 0;
  }
  return mesh.dx_p(i, j) / (mesh.y[j] - mesh.y[j - 1]);
}

inline const double NavierStocksSolver::get_aN_pressure_eq(const int i,
                                                           const int j) {
  if (j == (numGridPoints_y_ - 1)) {
    return 0;
  }
  return mesh.dx_p(i, j) / (mesh.y[j + 1] - mesh.y[j]);
}

void NavierStocksSolver::energy_analysis() {

  static double viscous = 0;
  static double body_force = 0;
  static double pressure = 0;
  static double convective = 0;
  static double convective_diffusive = 0;

  for (int i = 2; i < numGridPoints_x_ - 2; i++) {
    for (int j = 2; j < numGridPoints_y_ - 2; j++) {
      body_force += energy_budget_body_force(i, j) * timeStep_;
      viscous += energy_budget_viscous(i, j) * timeStep_;
      pressure += energy_budget_pressure(i, j) * timeStep_;
      convective += energy_budget_convective(i, j) * timeStep_;
      convective_diffusive +=
          energy_budget_convective_diffusive(i, j) * timeStep_;
    }
  }

  static double initial_energy = body_force + viscous;

  static bool headersWritten = false;
  std::ofstream file;

  if (!headersWritten) {
    file.open(energyAnalysisFileName_);
    if (file.is_open()) {
      // Write column headers (only once)
      file << "time, body_force, viscous, pressure, convective, "
              "convective_diffusive, "
              "initial_e, current_e, diff_e"
           << std::endl;
      headersWritten = true;
    } else {
      file_logger->error("Error: Unable to open file {} for writing.",
                         energyAnalysisFileName_);
      return;
    }
  } else {
    file.open(energyAnalysisFileName_,
              std::ios::app); // Open the file in append mode
  }

  // Print the values
  file << mesh.meshTime << "," << body_force << "," << viscous << ","
       << pressure << "," << convective << "," << convective_diffusive << ","
       << initial_energy << "," << body_force + viscous << ","
       << body_force + viscous - initial_energy << std::endl;

  file.close();
}

double NavierStocksSolver::energy_budget_pressure(const int i, const int j) {

  const double Pe = (mesh.P[i + 1][j] + mesh.P[i][j]) / 2.;
  const double Pw = (mesh.P[i - 1][j] + mesh.P[i][j]) / 2.;
  const double Pn = (mesh.P[i][j + 1] + mesh.P[i][j]) / 2.;
  const double Ps = (mesh.P[i][j - 1] + mesh.P[i][j]) / 2.;

  const double ue = mesh.u[i + 1][j];
  const double uw = mesh.u[i][j];
  const double vn = mesh.v[i][j + 1];
  const double vs = mesh.v[i][j];

  const double pressure_budget = (Pe * ue - Pw * uw) * mesh.dy_p(i, j) +
                                 (Pn * vn - Ps * vs) * mesh.dx_p(i, j);
  return pressure_budget;
}

double NavierStocksSolver::energy_budget_convective(const int i, const int j) {

  const double ee = (return_energy_at(i + 1, j) + return_energy_at(i, j)) / 2.;
  const double ew = (return_energy_at(i - 1, j) + return_energy_at(i, j)) / 2.;
  const double en = (return_energy_at(i, j + 1) + return_energy_at(i, j)) / 2.;
  const double es = (return_energy_at(i, j - 1) + return_energy_at(i, j)) / 2.;

  const double ue = mesh.u[i + 1][j];
  const double uw = mesh.u[i][j];
  const double vn = mesh.v[i][j + 1];
  const double vs = mesh.v[i][j];

  const double convective_budget = -((ee * ue - ew * uw) * mesh.dy_p(i, j) +
                                     (en * vn - es * vs) * mesh.dx_p(i, j));
  return convective_budget;
}

double NavierStocksSolver::energy_budget_body_force(const int i, const int j) {

  return (Pr_ * mesh.T[i][j] * (mesh.v[i][j] + mesh.v[i][j + 1]) / 2.) *
         mesh.dv_p(i, j);
}

double NavierStocksSolver::energy_budget_viscous(const int i, const int j) {

  Matrix2x2 grad_u = return_velocity_gradient_at(i, j);
  Matrix2x2 grad_u_plus_transpose = grad_u.add(grad_u.transpose());

  const double phi = grad_u_plus_transpose.doubleDotProduct(grad_u);
  return -Pr_ / sqrt(Ra_) * phi * mesh.dv_p(i, j);
}

double NavierStocksSolver::energy_budget_convective_diffusive(const int i,
                                                              const int j) {

  vector2D term_at_p_cell = helperFunctionTemp(i, j);
  vector2D term_at_p_right_cell = helperFunctionTemp(i + 1, j);
  vector2D term_at_p_left_cell = helperFunctionTemp(i - 1, j);
  vector2D term_at_p_top_cell = helperFunctionTemp(i, j + 1);
  vector2D term_at_p_bottom_cell = helperFunctionTemp(i, j - 1);

  const double term_at_e = (term_at_p_cell.x + term_at_p_right_cell.x) / 2.;
  const double term_at_w = (term_at_p_cell.x + term_at_p_left_cell.x) / 2.;
  const double term_at_n = (term_at_p_cell.y + term_at_p_top_cell.y) / 2.;
  const double term_at_s = (term_at_p_cell.y + term_at_p_bottom_cell.y) / 2.;

  return Pr_ / sqrt(Ra_) *
         ((term_at_e - term_at_w) * mesh.dy_p(i, j) +
          (term_at_n - term_at_s) * mesh.dx_p(i, j));
}

vector2D NavierStocksSolver::helperFunctionTemp(const int &i, const int &j) {
  Matrix2x2 grad_u = return_velocity_gradient_at(i, j);
  Matrix2x2 grad_u_plus_transpose = grad_u.add(grad_u.transpose());

  return grad_u_plus_transpose.multiplyVectorLeft(mesh.u[i][j], mesh.v[i][j]);
}

double NavierStocksSolver::return_energy_at(const int i, const int j) {

  const double ue = mesh.u[i + 1][j];
  const double uw = mesh.u[i][j];
  const double vn = mesh.v[i][j + 1];
  const double vs = mesh.v[i][j];

  return 0.5 * (pow((ue + uw) / 2.0, 2.0) + pow((vn + vs) / 2.0, 2.0));
}

Matrix2x2 NavierStocksSolver::return_velocity_gradient_at(const int i,
                                                          const int j) {

  const double ue = get_ue_in_p_cell(i, j);
  const double uw = get_uw_in_p_cell(i, j);
  const double un = get_un_in_p_cell(i, j);
  const double us = get_us_in_p_cell(i, j);

  const double vn = get_vn_in_p_cell(i, j);
  const double vs = get_vs_in_p_cell(i, j);
  const double ve = get_ve_in_p_cell(i, j);
  const double vw = get_vw_in_p_cell(i, j);

  return Matrix2x2((ue - uw) / mesh.dx_p(i, j), (ve - vw) / mesh.dx_p(i, j),
                   (un - us) / mesh.dy_p(i, j), (vn - vs) / mesh.dy_p(i, j));
}

inline const double NavierStocksSolver::get_un_in_p_cell(const int i,
                                                         const int j) {
  return ((mesh.u[i][j] + mesh.u[i + 1][j]) / 2. +
          (mesh.u[i][j + 1] + mesh.u[i + 1][j + 1]) / 2.) /
         2.;
}

inline const double NavierStocksSolver::get_us_in_p_cell(const int i,
                                                         const int j) {
  return ((mesh.u[i][j] + mesh.u[i + 1][j]) / 2. +
          (mesh.u[i][j - 1] + mesh.u[i + 1][j - 1]) / 2.) /
         2.;
}

inline const double NavierStocksSolver::get_uw_in_p_cell(const int i,
                                                         const int j) {
  return mesh.u[i][j];
}

inline const double NavierStocksSolver::get_ue_in_p_cell(const int i,
                                                         const int j) {
  return mesh.u[i + 1][j];
}

inline const double NavierStocksSolver::get_ve_in_p_cell(const int i,
                                                         const int j) {
  return ((mesh.v[i][j] + mesh.v[i][j + 1]) / 2. +
          (mesh.v[i + 1][j] + mesh.v[i + 1][j + 1]) / 2.) /
         2.;
}

inline const double NavierStocksSolver::get_vw_in_p_cell(const int i,
                                                         const int j) {
  return ((mesh.v[i][j] + mesh.v[i][j + 1]) / 2. +
          (mesh.v[i - 1][j] + mesh.v[i - 1][j + 1]) / 2.) /
         2.;
}

inline const double NavierStocksSolver::get_vn_in_p_cell(const int i,
                                                         const int j) {
  return mesh.v[i][j + 1];
}

inline const double NavierStocksSolver::get_vs_in_p_cell(const int i,
                                                         const int j) {
  return mesh.v[i][j];
}
