#ifndef MATRIX2X2_H
#define MATRIX2X2_H

#include "vector2D.h"

class Matrix2x2 {
private:
    double data[2][2];

public:
    // Constructors
    Matrix2x2();
    Matrix2x2(double a, double b, double c, double d);

    // Accessor methods
    double get(int i, int j) const;

    // Setter methods
    void set(int i, int j, double value);

    // Matrix operations
    Matrix2x2 add(const Matrix2x2& other) const;
    Matrix2x2 multiply(const Matrix2x2& other) const;
    Matrix2x2 transpose() const; // Function to calculate the transpose
    vector2D multiplyVectorLeft(double x, double y) const; // Function to multiply a 1x2 vector from the left
    double doubleDotProduct(const Matrix2x2& other) const;

    // Print method
    void print() const;
};

#endif // MATRIX2X2_H
