#include "Matrix2x2.h"
#include "vector2D.h"
#include <iostream>

// Constructors
Matrix2x2::Matrix2x2() {
  // Initialize matrix to zeros
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      data[i][j] = 0.0;
    }
  }
}

Matrix2x2::Matrix2x2(double a, double b, double c, double d) {
  data[0][0] = a;
  data[0][1] = b;
  data[1][0] = c;
  data[1][1] = d;
}

// Accessor methods
double Matrix2x2::get(int i, int j) const { return data[i][j]; }

// Setter methods
void Matrix2x2::set(int i, int j, double value) { data[i][j] = value; }

// Matrix operations
Matrix2x2 Matrix2x2::add(const Matrix2x2 &other) const {
  Matrix2x2 result;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      result.data[i][j] = data[i][j] + other.data[i][j];
    }
  }
  return result;
}

Matrix2x2 Matrix2x2::multiply(const Matrix2x2 &other) const {
  Matrix2x2 result;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      result.data[i][j] =
          data[i][0] * other.data[0][j] + data[i][1] * other.data[1][j];
    }
  }
  return result;
}

Matrix2x2 Matrix2x2::transpose() const {
  Matrix2x2 result;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      result.data[j][i] = data[i][j];
    }
  }
  return result;
}

vector2D Matrix2x2::multiplyVectorLeft(double x, double y) const {
  double resultX = data[0][0] * x + data[1][0] * y;
  double resultY = data[1][0] * x + data[1][1] * y;
  return vector2D(resultX, resultY);
}

double Matrix2x2::doubleDotProduct(const Matrix2x2 &other) const {
  double result = 0.0;
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      result += data[i][j] * other.data[j][i];
    }
  }
  return result;
}

// Print method
void Matrix2x2::print() const {
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      std::cout << data[i][j] << " ";
    }
    std::cout << std::endl;
  }
}
