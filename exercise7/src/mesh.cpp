#include "mesh.h"
#include <cmath>
#include <iostream>

Mesh::Mesh(
	const int &numGridPoints_x_,
	const int &numGridPoints_y_,
	const double &GeometryWidth_,
	const double &GeometryHeight_,
	const double &refinedGridStretchingFactor_x_,
	const double &refinedGridStretchingFactor_y_,
	const std::string mesh_type) : L_(GeometryWidth_),
								   H_(GeometryHeight_),
								   refinedGridStretchingFactor_x_(refinedGridStretchingFactor_x_),
								   refinedGridStretchingFactor_y_(refinedGridStretchingFactor_y_),
								   size_x_(numGridPoints_x_),
								   size_y_(numGridPoints_y_),
								   x(numGridPoints_x_, 0),
								   y(numGridPoints_y_, 0),
								   xfe(numGridPoints_x_, 0),
								   xfw(numGridPoints_x_, 0),
								   yfn(numGridPoints_y_, 0),
								   yfs(numGridPoints_y_, 0),
								   P(numGridPoints_x_, vec(numGridPoints_y_, 1)),
								   Pold(numGridPoints_x_, vec(numGridPoints_y_, 0.0)),
								   avgP(numGridPoints_x_, vec(numGridPoints_y_, 0.0)),								   
								   T(numGridPoints_x_, vec(numGridPoints_y_, 1)),
								   Told(numGridPoints_x_, vec(numGridPoints_y_, 0.0)),
								   avgT(numGridPoints_x_, vec(numGridPoints_y_, 0.0)),								   
								   u(numGridPoints_x_ + 1, vec(numGridPoints_y_, 0.0)),
								   uold(numGridPoints_x_ + 1, vec(numGridPoints_y_, 0.0)),
								   up(numGridPoints_x_ + 1, vec(numGridPoints_y_, 0.0)),
								   avgU(numGridPoints_x_ + 1, vec(numGridPoints_y_, 0.0)),								   
								   v(numGridPoints_x_, vec(numGridPoints_y_ + 1, 0.0)),
								   vold(numGridPoints_x_, vec(numGridPoints_y_ + 1, 0.0)),
								   vp(numGridPoints_x_, vec(numGridPoints_y_ + 1, 0.0)),
								   avgV(numGridPoints_x_, vec(numGridPoints_y_ + 1, 0.0)),								   
								   meshTime(0.0), meshAveragedTime(0.0)
{

	if (mesh_type == "refined")
	{
		create_refined_mesh();
	}
	else
	{
		create_uniform_mesh();
	}
};

void Mesh::create_uniform_mesh()
{
	double dx = L_ / (size_x_ - 1);
	double dy = H_ / (size_y_ - 1);

	x[0] = dx / 2.0;
	y[0] = dy / 2.0;

	xfw[0] = 0.0;
	xfe[0] = dx;

	yfs[0] = 0.0;
	yfn[0] = dy;

	for (int i = 1; i < size_x_; ++i)
	{
		x[i] = x[i - 1] + dx;
		xfw[i] = xfw[i - 1] + dx;
		xfe[i] = xfe[i - 1] + dx;
	}

	for (int j = 1; j < size_y_; ++j)
	{
		y[j] = y[j - 1] + dy;
		yfs[j] = yfs[j - 1] + dy;
		yfn[j] = yfn[j - 1] + dy;
	}
}

inline double Mesh::non_uniform_mesh_alpha(double i, int numGridPoints, double &refinedGridStretchingFactor_)
{
	return std::tanh(refinedGridStretchingFactor_ * (2.0 * i / static_cast<double>(numGridPoints) - 1.0)) / std::tanh(refinedGridStretchingFactor_) / 2.;
	// return (1.0 + (tanh(refinedGridStretchingFactor_x_ * (2.0 * double(i) / (numGridPoints - 1) - 1.0)) / tanh(refinedGridStretchingFactor_x_)))/2.;
}

double Mesh::evaluate_dx_non_uniform_mesh(double i, int numGridPoints, double &refinedGridStretchingFactor_)
{
	double x_i = non_uniform_mesh_alpha(i, numGridPoints, refinedGridStretchingFactor_);
	double x_ip1 = non_uniform_mesh_alpha(i + 1, numGridPoints, refinedGridStretchingFactor_);
	double x_im1 = non_uniform_mesh_alpha(i - 1, numGridPoints, refinedGridStretchingFactor_);

	return (x_i - x_im1) / 2. + (x_ip1 - x_i) / 2.;
}

void Mesh::create_refined_mesh()
{

	const double dx_0 = evaluate_dx_non_uniform_mesh(0, size_x_, refinedGridStretchingFactor_x_);
	const double dy_0 = evaluate_dx_non_uniform_mesh(0, size_y_, refinedGridStretchingFactor_y_);

	x[0] = dx_0 / 2.0;
	y[0] = dy_0 / 2.0;

	xfw[0] = 0.0;
	xfe[0] = dx_0;

	yfs[0] = 0.0;
	yfn[0] = dy_0;

	for (int i = 1; i < size_x_; ++i)
	{
		const double dx = evaluate_dx_non_uniform_mesh(i, size_x_, refinedGridStretchingFactor_x_);
		x[i] = x[i - 1] + dx;
		xfw[i] = xfw[i - 1] + dx; //- x[i] - dx / 2.0; //-
		xfe[i] = xfe[i - 1] + dx; //- x[i] + dx / 2.0; //-
	}

	for (int j = 1; j < size_y_; ++j)
	{
		const double dy = evaluate_dx_non_uniform_mesh(j, size_y_, refinedGridStretchingFactor_y_);
		y[j] = y[j - 1] + dy;
		yfs[j] = yfs[j - 1] + dy; //- y[j] - dy/2.0; //-
		yfn[j] = yfn[j - 1] + dy; //- y[j] + dy/2.0; //-
	}
}

Mesh::Mesh(){};
