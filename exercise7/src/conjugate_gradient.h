#ifndef CONJUGATE_GRADIENT_H
#define CONJUGATE_GRADIENT_H

#include <vector>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>

typedef std::vector<double> vec;
typedef std::vector<std::vector<double>> vec2d;

void conjugateGradient(
    const std::vector<vec2d> &A,
    const vec2d &b,
    int maxIterations,
    double tolerance,
    vec2d &x, 
    std::shared_ptr<spdlog::logger> &file_logger, 
    bool &logger_enbled);

#endif // CONJUGATE_GRADIENT_H
