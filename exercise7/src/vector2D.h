#ifndef vector2D_H
#define vector2D_H

#include <cmath>

class vector2D {
public:
    double x, y;

    // Constructors
    vector2D() : x(0.0), y(0.0) {}
    vector2D(double x_, double y_) : x(x_), y(y_) {}

    // Copy constructor
    vector2D(const vector2D& other) : x(other.x), y(other.y) {}

    // Destructor (not necessary in this case, but good practice)
    ~vector2D() {}

    // Assignment operator
    vector2D& operator=(const vector2D& other) {
        if (this != &other) {
            x = other.x;
            y = other.y;
        }
        return *this;
    }

    // Vector addition
    vector2D operator+(const vector2D& other) const {
        return vector2D(x + other.x, y + other.y);
    }

    // Vector subtraction
    vector2D operator-(const vector2D& other) const {
        return vector2D(x - other.x, y - other.y);
    }

    // Scalar multiplication
    vector2D operator*(double scalar) const {
        return vector2D(x * scalar, y * scalar);
    }

    // Scalar division
    vector2D operator/(double scalar) const {
        if (scalar == 0.0) return *this; // Avoid division by zero
        return vector2D(x / scalar, y / scalar);
    }

    // Dot product
    double dot(const vector2D& other) const {
        return x * other.x + y * other.y;
    }

    // Cross product
    double cross(const vector2D& other) const {
        return x * other.y - y * other.x;
    }

    // Magnitude (length) of the vector
    double magnitude() const {
        return std::sqrt(x * x + y * y);
    }

    // Normalize the vector
    vector2D normalize() const {
        double mag = magnitude();
        if (mag == 0.0) return *this; // Avoid division by zero
        return *this / mag;
    }
};

#endif // vector2D_H
