#ifndef CONVECTION_DIFFUSION_SOLVER_H
#define CONVECTION_DIFFUSION_SOLVER_H

#include <vector>
#include <fstream>

class ConvectionDiffusionSolver
{
public:
    // Constructor to initialize the solver with parameters
    ConvectionDiffusionSolver(const std::string &filename);

    // Method to read configuration parameters from a YAML file
    bool readConfigFromYAML(const std::string &filename);

    // Method to initialize the location vectors of x and y
    void initialise_xy_vectors();

    // Initialize the property distribution on the geometry
    void initializePropertyDistribution();

    // Function to write the property values of the entire geometry at the end of solution
    void writePropertyDistributionToCSV();

    // Function to write the property value at a location in each timestep
    void writePropertyAtOutletToCSV();

    void solveExplicit();

    // Solve the convection-diffusion equation over a specified time period
    void solve();

    // Get the property distribution at the current time if needed from outside of the class
    const std::vector<std::vector<double>> &getCurrentPropertyDistribution() const;

    // Function to get the geometric average of two values
    inline double geometric_average_x(const double x1, const double x2);

    inline const double u(const double x, const double y);
    inline const double v(const double x, const double y);    

    const double evaluate_diffusion_term_internal(const int i, const int j);
    const double evaluate_diffusion_term_left_boundary(const int i, const int j);
    const double evaluate_diffusion_term_right_boundary(const int i, const int j);
    const double evaluate_diffusion_term_top_boundary(const int i, const int j);
    const double evaluate_diffusion_term_inlet_boundary(const int i, const int j);
    const double evaluate_diffusion_term_outlet_boundary(const int i, const int j);

    const double evaluate_convective_term_internal(const int i, const int j);
    const double evaluate_convective_term_left_boundary(const int i, const int j);
    const double evaluate_convective_term_right_boundary(const int i, const int j);
    const double evaluate_convective_term_top_boundary(const int i, const int j);
    const double evaluate_convective_term_inlet_boundary(const int i, const int j);
    const double evaluate_convective_term_outlet_boundary(const int i, const int j);

    const double get_property_at(const double value_p, const double value_i, const double vel);

    void solve_internal_points();
    void solve_left_boundary();
    void solve_right_boundary();
    void solve_top_boundary();
    void solve_bottom_boundary();

    const double get_average_of_changes();
    void store_property_in_temp_array();
    void solve_corner_points();
    
private:
    // private member functions:
    double timeStep_;
    int numGridPoints_x_;
    int numGridPoints_y_;

    double initialValue_;

    double convergence_criteria_;
    double dx_;
    double dy_;
    std::vector<std::vector<double>> propertyDistribution_;
    std::vector<std::vector<double>> propertyDistribution_new;    
    std::vector<double> x_;
    std::vector<double> y_;

    double GeometryWidth_;
    double GeometryHeight_;

    double alpha_;
    double rho_;
    double gamma_;

    std::string scheme_;
    std::string finalResultOutputFileName_;
    std::string finalResultsAtOutlet_;

    double currentTime_;
};

#endif // CONVECTION_DIFFUSION_SOLVER_H
