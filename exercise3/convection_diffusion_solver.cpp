#include "convection_diffusion_solver.h"
#include <cmath>
#include <iostream>
#include "yaml-cpp/yaml.h"

ConvectionDiffusionSolver::ConvectionDiffusionSolver(const std::string &filename)
{
    readConfigFromYAML(filename);
    initialise_xy_vectors();
    initializePropertyDistribution();
}

void ConvectionDiffusionSolver::initialise_xy_vectors()
{
    x_.resize(numGridPoints_x_, 0.0);
    y_.resize(numGridPoints_y_, 0.0);

    // Calculate the spacing between points in each dimension
    dx_ = GeometryWidth_ / numGridPoints_x_;
    dy_ = GeometryHeight_ / numGridPoints_y_;

    x_[0] = -1.0 + dx_ / 2.0;
    y_[0] = dy_ / 2.0;

    for (int i = 1; i < numGridPoints_x_; i++)
    {
        x_[i] = i * dx_ + x_[0];
    }

    for (int j = 1; j < numGridPoints_y_; j++)
    {
        y_[j] = j * dy_ + y_[0];
    }
}

// Method to read configuration parameters from a YAML file
bool ConvectionDiffusionSolver::readConfigFromYAML(const std::string &filename)
{
    try
    {
        YAML::Node config = YAML::LoadFile(filename);

        // Read configuration parameters from the YAML file
        GeometryWidth_ = config["GeometryWidth"].as<double>();
        GeometryHeight_ = config["GeometryHeight"].as<double>();
        initialValue_ = config["initialValue"].as<double>();

        alpha_ = config["alpha"].as<double>();
        rho_ = config["rho"].as<double>();
        gamma_ = config["gamma"].as<double>();
        scheme_ = config["scheme"].as<std::string>();

        timeStep_ = config["timeStep"].as<double>();
        numGridPoints_x_ = config["numGridPoints_x"].as<int>();
        numGridPoints_y_ = config["numGridPoints_y"].as<int>();
        finalResultOutputFileName_ = config["finalResultOutputFileName"].as<std::string>();
        finalResultsAtOutlet_ = config["finalResultsAtOutlet"].as<std::string>();
        convergence_criteria_ = config["convergence_criteria"].as<double>();

        return true; // Configuration read successfully
    }
    catch (const YAML::Exception &e)
    {
        std::cerr << "Error reading configuration: " << e.what() << std::endl;
        return false; // Configuration read failed
    }
}

void ConvectionDiffusionSolver::initializePropertyDistribution()
{
    propertyDistribution_.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, initialValue_));
    propertyDistribution_new.resize(numGridPoints_x_, std::vector<double>(numGridPoints_y_, initialValue_));
}

void ConvectionDiffusionSolver::writePropertyDistributionToCSV()
{
    std::ofstream file(finalResultOutputFileName_);
    if (file.is_open())
    {
        // Write column headers
        file << "x, y, propertyDistribution, u, v" << std::endl;

        // Write data for each point
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            for (int j = 0; j < numGridPoints_y_; j++)
            {
                file << x_[i] << ", " << y_[j] << ", " << propertyDistribution_[i][j] << ", " << u(x_[i], y_[j]) << ", " << v(x_[i], y_[j]) << std::endl;
            }
        }

        file.close();
        std::cout << "CSV file '" << finalResultOutputFileName_ << "' written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

void ConvectionDiffusionSolver::writePropertyAtOutletToCSV()
{
    static bool headersWritten = false; // Keep track if headers are already written
    std::ofstream file;

    if (!headersWritten)
    {
        file.open(finalResultsAtOutlet_);
        if (file.is_open())
        {
            // Write column headers (only once)
            file << "x,y,property" << std::endl;
            headersWritten = true;
        }
        else
        {
            std::cerr << "Error: Unable to open file for writing." << std::endl;
            return;
        }
    }
    else
    {
        file.open(finalResultsAtOutlet_, std::ios::app); // Open the file in append mode
    }

    if (file.is_open())
    {

        const int j = 0;
        for (int i = 0; i < numGridPoints_x_; i++)
        {
            file << x_[i] << "," << y_[j] << "," << propertyDistribution_[i][j] << std::endl;
        }
        file.close();
        std::cout << "CSV data at the outlet written successfully." << std::endl;
    }
    else
    {
        std::cerr << "Error: Unable to open file for writing." << std::endl;
    }
}

inline const double ConvectionDiffusionSolver::u(const double x, const double y)
{
    return 2. * y * (1. - x * x);
};

inline const double ConvectionDiffusionSolver::v(const double x, const double y)
{
    return -2. * x * (1. - y * y);
};

const double ConvectionDiffusionSolver::get_average_of_changes()
{

    double sumDifferences = 0.0;

    // Calculate differences and accumulate the sum
    for (int i = 0; i < numGridPoints_x_; ++i)
    {
        for (int j = 0; j < numGridPoints_y_; ++j)
        {
            double difference = std::abs(propertyDistribution_[i][j] - propertyDistribution_new[i][j]);
            sumDifferences += difference;
        }
    }

    // Calculate the average
    double averageDifference = sumDifferences / (numGridPoints_x_ * numGridPoints_y_);

    return averageDifference;
}

void ConvectionDiffusionSolver::store_property_in_temp_array()
{
    // Copy values from propertyDistribution_ to propertyDistribution_new
    for (int i = 0; i < numGridPoints_x_; ++i)
    {
        for (int j = 0; j < numGridPoints_y_; ++j)
        {
            propertyDistribution_[i][j] = propertyDistribution_new[i][j];
        }
    }
}
void ConvectionDiffusionSolver::solve()
{
    currentTime_ = timeStep_;
    double convergence_rate = 1.e10;

    while (convergence_rate > convergence_criteria_)
    {
        std::cout << "-------------------------------------" << std::endl;
        std::cout << "solving for the time: " << currentTime_ << std::endl;

        // Update the property distribution based on the solution
        solveExplicit();

        // Advance the current time
        currentTime_ += timeStep_;

        // writePropertyAtLocationToCSV(currentTime_);

        convergence_rate = get_average_of_changes();
        std::cout << "Convergence Rate is: " << convergence_rate << std::endl;

        store_property_in_temp_array();
    }

    writePropertyDistributionToCSV();
    writePropertyAtOutletToCSV();
    std::cout << "solution finished" << std::endl;
}

const double ConvectionDiffusionSolver::evaluate_diffusion_term_internal(const int i, const int j)
{
    const double diff = gamma_ * ((propertyDistribution_[i + 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i - 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i][j + 1] - propertyDistribution_[i][j]) * dx_ / dy_ +
                                  (propertyDistribution_[i][j - 1] - propertyDistribution_[i][j]) * dx_ / dy_);

    return diff;
};

const double ConvectionDiffusionSolver::evaluate_diffusion_term_left_boundary(const int i, const int j)
{
    const double diff = gamma_ * ((propertyDistribution_[i + 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  ((1. - tanh(alpha_)) - propertyDistribution_[i][j]) * dy_ / (dx_ / 2.0) +
                                  (propertyDistribution_[i][j + 1] - propertyDistribution_[i][j]) * dx_ / dy_ +
                                  (propertyDistribution_[i][j - 1] - propertyDistribution_[i][j]) * dx_ / dy_);

    return diff;
};

const double ConvectionDiffusionSolver::evaluate_diffusion_term_right_boundary(const int i, const int j)
{
    const double diff = gamma_ * (((1. - tanh(alpha_)) - propertyDistribution_[i][j]) * dy_ / (dx_ / 2.0) +
                                  (propertyDistribution_[i - 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i][j + 1] - propertyDistribution_[i][j]) * dx_ / dy_ +
                                  (propertyDistribution_[i][j - 1] - propertyDistribution_[i][j]) * dx_ / dy_);

    return diff;
};

const double ConvectionDiffusionSolver::evaluate_diffusion_term_top_boundary(const int i, const int j)
{
    const double diff = gamma_ * ((propertyDistribution_[i - 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i + 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  ((1. - tanh(alpha_)) - propertyDistribution_[i][j]) * dx_ / (dy_ / 2.0) +
                                  (propertyDistribution_[i][j - 1] - propertyDistribution_[i][j]) * dx_ / dy_);

    return diff;
};

const double ConvectionDiffusionSolver::evaluate_diffusion_term_inlet_boundary(const int i, const int j)
{
    const double diff = gamma_ * ((propertyDistribution_[i + 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i - 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i][j + 1] - propertyDistribution_[i][j]) * dx_ / dy_ +
                                  ((1. + tanh((2. * x_[i] + 1.) * alpha_)) - propertyDistribution_[i][j]) * dx_ / (dy_ / 2.));
    return diff;
};

const double ConvectionDiffusionSolver::evaluate_diffusion_term_outlet_boundary(const int i, const int j)
{
    const double diff = gamma_ * ((propertyDistribution_[i + 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i - 1][j] - propertyDistribution_[i][j]) * dy_ / dx_ +
                                  (propertyDistribution_[i][j + 1] - propertyDistribution_[i][j]) * dx_ / dy_ +
                                  0.0);

    return diff;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_internal(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i + 1][j], u_e);
    const double property_w = get_property_at(propertyDistribution_[i - 1][j], propertyDistribution_[i][j], u_w);
    const double property_n = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i][j + 1], v_n);
    const double property_s = get_property_at(propertyDistribution_[i][j - 1], propertyDistribution_[i][j], v_s);

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_left_boundary(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i + 1][j], u_e);
    const double property_w = 1. - tanh(alpha_);
    const double property_n = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i][j + 1], v_n);
    const double property_s = get_property_at(propertyDistribution_[i][j - 1], propertyDistribution_[i][j], v_s);

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_right_boundary(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = 1. - tanh(alpha_);
    const double property_w = get_property_at(propertyDistribution_[i - 1][j], propertyDistribution_[i][j], u_w);
    const double property_n = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i][j + 1], v_n);
    const double property_s = get_property_at(propertyDistribution_[i][j - 1], propertyDistribution_[i][j], v_s);

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_top_boundary(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i + 1][j], u_e);
    const double property_w = get_property_at(propertyDistribution_[i - 1][j], propertyDistribution_[i][j], u_w);
    const double property_n = 1. - tanh(alpha_);
    const double property_s = get_property_at(propertyDistribution_[i][j - 1], propertyDistribution_[i][j], v_s);

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_inlet_boundary(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i + 1][j], u_e);
    const double property_w = get_property_at(propertyDistribution_[i - 1][j], propertyDistribution_[i][j], u_w);
    const double property_n = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i][j + 1], v_n);
    const double property_s = 1. + tanh((2. * x_[i] + 1.) * alpha_);

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::evaluate_convective_term_outlet_boundary(const int i, const int j)
{
    const double u_e = u(x_[i] + dx_ / 2., y_[j]);
    const double u_w = u(x_[i] - dx_ / 2., y_[j]);
    const double v_n = v(x_[i], y_[j] + dy_ / 2.);
    const double v_s = v(x_[i], y_[j] - dy_ / 2.);

    const double property_e = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i + 1][j], u_e);
    const double property_w = get_property_at(propertyDistribution_[i - 1][j], propertyDistribution_[i][j], u_w);
    const double property_n = get_property_at(propertyDistribution_[i][j], propertyDistribution_[i][j + 1], v_n);
    const double property_s = propertyDistribution_[i][j];

    const double conv = rho_ * (u_e * property_e * dy_ -
                                u_w * property_w * dy_ +
                                v_n * property_n * dx_ -
                                v_s * property_s * dx_);

    return conv;
};

const double ConvectionDiffusionSolver::get_property_at(const double value_i, const double value_ip1, const double vel_f)
{
    if (scheme_ == "CDS")
    {
        return (value_ip1 + value_i) / 2.0;
    }
    else if (scheme_ == "UDS")
    {
        if (vel_f > 0)
        {
            return value_i;
        }
        // else
        return value_ip1;
    }
}

const std::vector<std::vector<double>> &ConvectionDiffusionSolver::getCurrentPropertyDistribution() const
{
    return propertyDistribution_;
}

void ConvectionDiffusionSolver::solve_internal_points()
{
    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        for (int j = 1; j < numGridPoints_y_ - 1; j++)
        {
            propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                             timeStep_ / (rho_ * (dx_ * dy_)) *
                                                 (-1. * evaluate_convective_term_internal(i, j) + evaluate_diffusion_term_internal(i, j));
        }
    }
}

void ConvectionDiffusionSolver::solve_left_boundary()
{
    const int i = 0;

    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                         timeStep_ / (rho_ * (dx_ * dy_)) *
                                             (-1. * evaluate_convective_term_left_boundary(i, j) + evaluate_diffusion_term_left_boundary(i, j));
    }
}

void ConvectionDiffusionSolver::solve_right_boundary()
{
    const int i = numGridPoints_x_ - 1;

    for (int j = 1; j < numGridPoints_y_ - 1; j++)
    {
        propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                         timeStep_ / (rho_ * (dx_ * dy_)) *
                                             (-1. * evaluate_convective_term_right_boundary(i, j) + evaluate_diffusion_term_right_boundary(i, j));
    }
}

void ConvectionDiffusionSolver::solve_top_boundary()
{
    const int j = numGridPoints_y_ - 1;

    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                         timeStep_ / (rho_ * (dx_ * dy_)) *
                                             (-1. * evaluate_convective_term_top_boundary(i, j) + evaluate_diffusion_term_top_boundary(i, j));
    }
}

void ConvectionDiffusionSolver::solve_bottom_boundary()
{
    const int j = 0;

    for (int i = 1; i < numGridPoints_x_ - 1; i++)
    {
        if (x_[i] <= 0.0) // inlet
        {
            propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                             timeStep_ / (rho_ * (dx_ * dy_)) *
                                                 (-1. * evaluate_convective_term_inlet_boundary(i, j) + evaluate_diffusion_term_inlet_boundary(i, j));
        }
        else // outlet
        {
            propertyDistribution_new[i][j] = propertyDistribution_[i][j] +
                                             timeStep_ / (rho_ * (dx_ * dy_)) *
                                                 (-1. * evaluate_convective_term_outlet_boundary(i, j) + evaluate_diffusion_term_outlet_boundary(i, j));
        }
    }
}

void ConvectionDiffusionSolver::solve_corner_points()
{
    int i = 0;
    int j = 0;
    propertyDistribution_new[i][j] = (propertyDistribution_[i + 1][j] + propertyDistribution_[i][j + 1]) / 2.;

    i = numGridPoints_x_ - 1;
    j = 0;
    propertyDistribution_new[i][j] = (propertyDistribution_[i - 1][j] + propertyDistribution_[i][j + 1]) / 2.;

    i = 0;
    j = numGridPoints_y_ - 1;
    propertyDistribution_new[i][j] = (propertyDistribution_[i + 1][j] + propertyDistribution_[i][j - 1]) / 2.;

    i = numGridPoints_x_ - 1;
    j = numGridPoints_y_ - 1;
    propertyDistribution_new[i][j] = (propertyDistribution_[i - 1][j] + propertyDistribution_[i][j - 1]) / 2.;
}

void ConvectionDiffusionSolver::solveExplicit()
{
    solve_internal_points();
    solve_bottom_boundary();
    solve_left_boundary();
    solve_right_boundary();
    solve_top_boundary();
    solve_corner_points();
}
