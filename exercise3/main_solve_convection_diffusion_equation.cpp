#include "convection_diffusion_solver.h"
#include <iostream>
#include <vector>

int main() {
    const std::string& configurationFile = "convection_diffusion_config.yaml";

    // Create an instance of the ConvectionDiffusionSolver
    ConvectionDiffusionSolver heatSolver(configurationFile);

    // Solve the heat equation
    heatSolver.solve();

    return 0;
}
