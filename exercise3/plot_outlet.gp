# Set the output format (e.g., PNG)
set terminal pngcairo enhanced font "Helvetica,12"
set output "property_at_outlet.png"

# Set the plot title and labels for x:property
set title "X vs Property"
set xlabel "X"
set ylabel "Property"

# Specify the input data file and format
set datafile separator ","
set datafile missing "NaN"
set yrange [0:2.1]

# Plot x against property with a blue line and no markers
plot "finalResultOutlet.csv" using 1:3 with lines linecolor "blue" notitle

# Save the plot to a file and close Gnuplot
set output
quit
